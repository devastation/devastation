 /**************************************************************
 * Speed Jackpot
 *
 * After a flag game is won, this will double the speed at which
 * the jackpot rises.
 *
 * By: Hallowed be thy name
 *
 **************************************************************/

#include "asss.h"
#include "config.h"
#include "objects.h"
#include "fg_wz.h"
#include "flagcore.h"

/* Interfaces */
local Imodman *mm;
local Imainloop *ml;
local Iobjects *obj;
local Ichat *chat;
local Iconfig *cfg;
local Iarenaman *aman;

/* Arena data */
typedef struct Adata
{
    int active;
} Adata;

local int arenaKey;

local void GoToNormalMode(Arena *arena)
{
   Target target;
   target.type = T_ARENA;
   target.u.arena = arena;
   
   Adata *adata = P_ARENA_DATA(arena, arenaKey);
   if (adata->active)
   {
      obj->Toggle(&target, 995, 0);

      adata->active = 0;
      cfg->SetInt(arena->cfg, "Kill", "JackpotBountyPercent", 120000, NULL, 0);
   }
}

/* FastMode-> Double the speed */
local int DoubleSpeed(void* param)
{
    Arena *a = (Arena*) param;
    
    Target target;
    target.type = T_ARENA;
    target.u.arena = a;

    obj->Toggle(&target, 995, 1);

    cfg->SetInt(a->cfg, "Kill", "JackpotBountyPercent", 200000, NULL, 0);

    Adata *adata = P_ARENA_DATA(a, arenaKey);
    
    adata->active = 1;
    chat->SendArenaMessage(a, "The jackpot will rise with 2x speed for the next 30 minutes!");

    return FALSE;
}

local void GoToFastMode(Arena *arena)
{
   Target target;
   target.type = T_ARENA;
   target.u.arena = arena;

   Adata *adata = P_ARENA_DATA(arena, arenaKey);
   if (!adata->active)
   {
       ml->SetTimer(DoubleSpeed, 1500, 1500, arena, arena);
   }
}

local int resetJP(void* param)
{
   Arena *a = (Arena*) param;

   GoToNormalMode(a);

   return FALSE;
}

/* FlagReset callback */
local void StartTimer(Arena *a, int freq, int *points)
{
   GoToFastMode(a);
   ml->SetTimer(resetJP, 180000, 180000, a, a);
}

/* Module information (?modinfo) */
EXPORT const char info_speedjp[] = "(Devastation) v1.2 Hakaku, Hallowed be thy name";

EXPORT int MM_speedjp(int action, Imodman *mm_, Arena *arena)
{
   if (action == MM_LOAD)
   {
        mm = mm_;

        ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        obj = mm->GetInterface(I_OBJECTS, ALLARENAS);
        cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
        aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
        
        // check interfaces
        if (!ml || !chat || !obj || !cfg || !aman)
        {
            // release interfaces
            mm->ReleaseInterface(aman);
            mm->ReleaseInterface(cfg);
            mm->ReleaseInterface(obj);
            mm->ReleaseInterface(chat);
            mm->ReleaseInterface(ml);
            
            return MM_FAIL;
        }
        
        return MM_OK;
   }
   else if (action == MM_UNLOAD)
   {
        // release interfaces
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(obj);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(ml);
      
        return MM_OK;
   }
   else if (action == MM_ATTACH)
   {
        // arena data
        arenaKey = aman->AllocateArenaData(sizeof(struct Adata));
        if (arenaKey == -1)
            return MM_FAIL;
        
        // register callbacks
        mm->RegCallback(CB_FLAGRESET, StartTimer, arena);
        
        return MM_OK;
   }
   else if (action == MM_DETACH)
   {
        // clear data
        aman->FreeArenaData(arenaKey);

        // clear timers
        ml->ClearTimer(resetJP, NULL);
        ml->ClearTimer(DoubleSpeed, NULL);

        // unregister callbacks
        mm->UnregCallback(CB_FLAGRESET, StartTimer, arena);
        
        return MM_OK;
   }

   return MM_FAIL;
}
