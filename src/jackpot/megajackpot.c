 /**************************************************************
 * MegaJackpot
 *
 * Handles everything to do with the MegaJackpot event
 *  for Devastation
 *
 * Originally created by SOS, modified by XDOOM, ported
 *  by Hallowed be thy name, and modified by Hakaku.
 *
 **************************************************************/

#include "asss.h"
#include "fg_wz.h"
#include "jackpot.h"
#include "credits.h"

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Icmdman *cmd;
local Iconfig *cfg;
local Iplayerdata *pd;
local Iarenaman *aman;
local Iobjects *obj;
local Iflagcore *flags;
local Ijackpot *jp;
local Icredits *credits;

/************************************************************************/
/*                          Interface Functions                         */
/************************************************************************/

/* Arena player count */
local int GetPlayerCount(Arena *arena)
{
    int i = 0;
    Player *p;
    Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {
            i++;
        }
    }
    pd->Unlock();
    return i;
}

/* Arena count */
local int getMJPMultiplier(int players, Arena *arena)
{
    int xten = cfg->GetInt(arena->cfg, "MegaJackpot", "xTen", 15);
    int xeight = cfg->GetInt(arena->cfg, "MegaJackpot", "xEight", 12);
    int xsix = cfg->GetInt(arena->cfg, "MegaJackpot", "xSix", 10);
    int xfour = cfg->GetInt(arena->cfg, "MegaJackpot", "xFour", 8);
    int xtwo = cfg->GetInt(arena->cfg, "MegaJackpot", "xTwo", 6);

    if (players >= xten)
        return 10;
    else if (players >= xeight)
        return 8;
    else if (players >= xsix)
        return 6;
    else if (players >= xfour)
        return 4;
    else if (players >= xtwo)
        return 2;
    else
        return 1;
}

/* Toggle lvz */
local void MJPHideAll(Arena *arena)
{
    Target target;
    target.type = T_ARENA;
    target.u.arena = arena;

    obj->Toggle(&target, 400, 0);
    obj->Toggle(&target, 401, 0);
    obj->Toggle(&target, 414, 0);
    obj->Toggle(&target, 424, 0);
    obj->Toggle(&target, 413, 0);
    obj->Toggle(&target, 423, 0);
    obj->Toggle(&target, 412, 0);
    obj->Toggle(&target, 422, 0);
    obj->Toggle(&target, 411, 0);
    obj->Toggle(&target, 421, 0);
    obj->Toggle(&target, 410, 0);
    obj->Toggle(&target, 420, 0);
}

/* Set the graphics (lvz) */
local void setMJPgfx(Arena *arena)
{
    int players = GetPlayerCount(arena);
    Target target;
    target.type = T_ARENA;
    target.u.arena = arena;

    int xten = cfg->GetInt(arena->cfg, "MegaJackpot", "xTen", 15);
    int xeight = cfg->GetInt(arena->cfg, "MegaJackpot", "xEight", 12);
    int xsix = cfg->GetInt(arena->cfg, "MegaJackpot", "xSix", 10);
    int xfour = cfg->GetInt(arena->cfg, "MegaJackpot", "xFour", 8);
    int xtwo = cfg->GetInt(arena->cfg, "MegaJackpot", "xTwo", 6);

    int minJPSize = cfg->GetInt(arena->cfg, "MegaJackpot", "minJPSize", 15000000);
    int jackpot = jp->GetJP(arena);

    /* Hide all the lvz so we can display new ones */
    MJPHideAll(arena);

    if (players >= xten && jackpot >= minJPSize)
    {
        obj->Toggle(&target, 414, 1);
        obj->Toggle(&target, 424, 1);
    }
    else if (players >= xeight && jackpot >= minJPSize)
    {
        obj->Toggle(&target, 413, 1);
        obj->Toggle(&target, 423, 1);
    }
    else if (players >= xsix && jackpot >= minJPSize)
    {
        obj->Toggle(&target, 412, 1);
        obj->Toggle(&target, 422, 1);
    }
    else if (players >= xfour && jackpot >= minJPSize)
    {
        obj->Toggle(&target, 411, 1);
        obj->Toggle(&target, 421, 1);
    }
    else if (players >= xtwo && jackpot >= minJPSize)
    {
        obj->Toggle(&target, 410, 1);
        obj->Toggle(&target, 420, 1);
    }
    else if (players < xtwo)
    {
        obj->Toggle(&target, 401, 1); //Not enough players
    }
    else if (jackpot < minJPSize)
    {
        obj->Toggle(&target, 400, 1); //Jackpot too low
    }
}

/************************************************************************/
/*                         Module Callbacks                             */
/************************************************************************/

/* Player ShipChange */
local void mjpShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    setMJPgfx(p->arena);
	
	/* Set Credits Multiplier*/
	int players = GetPlayerCount(p->arena);
	int count = getMJPMultiplier(players, p->arena);
	credits->Multiplier(count,1,p->arena);
}

/* Player Action (entering, leaving) */
local void mjpPlayerAction(Player *p, int action, Arena *arena)
{
    setMJPgfx(arena);
	
	/* Set Credits Multiplier*/
	int players = GetPlayerCount(arena);
	int count = getMJPMultiplier(players, arena);;
	credits->Multiplier(count,1,p->arena);
}

/* FlagReset */
/*local void mjpBonus(Arena *arena, int freq, int *points)
{
    int count = 0;
    int players = GetPlayerCount(arena);
    count = getMJPMultiplier(players, arena);
    cfg->SetInt(arena->cfg, "Kill", "JackpotBountyPercent", 12000 * count, NULL, 0);
}*/

/************************************************************************/
/*                          Module Commands                             */
/************************************************************************/

local helptext_t start_help =
"Targets: none\n"
"Args: none\n"
"Starts MegaJackpot!";

/* Check if this arena supports megajackpot */
local void CmegajpStart(const char *command, const char *params, Player *p, const Target *target)
{
    /* Announce that the event has started */
    chat->SendArenaSoundMessage(p->arena, 2, "Mega Jackpot has started!");

    /* Register Callbacks */
    //mm->RegCallback(CB_FLAGRESET, mjpBonus, p->arena);
    mm->RegCallback(CB_PLAYERACTION, mjpPlayerAction, p->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, mjpShipFreqChange, p->arena);
	
	/* Set Multiplier */
	credits->Multiplier(1,1,p->arena);

    /* Display the lvz */
    setMJPgfx(p->arena);
	
}

local helptext_t stop_help =
"Targets: none\n"
"Args: none\n"
"Stops MegaJackpot!";

/* Stopped */
local void CmegajpStop(const char *command, const char *params, Player *p, const Target *target)
{
    /* Announce that the event has stopped */
    chat->SendArenaSoundMessage(p->arena, 1, "Mega Jackpot Stopped!");

    /* Hide the lvz */
    MJPHideAll(p->arena);

    /* Unregister Callbacks*/
    //mm->UnregCallback(CB_FLAGRESET, mjpBonus, p->arena);
    mm->UnregCallback(CB_PLAYERACTION, mjpPlayerAction, p->arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, mjpShipFreqChange, p->arena);
	
	/* Reset Multiplier */
	credits->Multiplier(1,0,p->arena);
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_megajackpot[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_megajackpot(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;

        chat = mm->GetInterface(I_CHAT, arena);
        cmd = mm->GetInterface(I_CMDMAN, arena);
        cfg = mm->GetInterface(I_CONFIG, arena);
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        aman = mm->GetInterface(I_ARENAMAN, arena);
        obj = mm->GetInterface(I_OBJECTS, arena);
        flags = mm->GetInterface(I_FLAGCORE, arena);
        jp = mm->GetInterface(I_JACKPOT, arena);
        credits = mm->GetInterface(I_CREDITS, arena);

        if (!chat || !cmd || !cfg || !pd || !aman || !obj || !flags || !jp || !credits)
        {
            mm->ReleaseInterface(credits);
            mm->ReleaseInterface(jp);
            mm->ReleaseInterface(flags);
            mm->ReleaseInterface(obj);
            mm->ReleaseInterface(aman);
            mm->ReleaseInterface(pd);
            mm->ReleaseInterface(cfg);
            mm->ReleaseInterface(cmd);
            mm->ReleaseInterface(chat);

            return MM_FAIL;
        }
        else
        {
            return MM_OK;
        }
    }
    else if (action == MM_UNLOAD)
    {
        mm->ReleaseInterface(credits);
        mm->ReleaseInterface(jp);
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(obj);
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(chat);

        return MM_OK;
    }
    else if (action == MM_ATTACH)
    {
        cmd->AddCommand("start", CmegajpStart, arena, start_help);
        cmd->AddCommand("stop", CmegajpStop, arena, stop_help);
        
        return MM_OK;
    }
    else if (action == MM_DETACH)
    {
        cmd->RemoveCommand("stop", CmegajpStop, arena);
        cmd->RemoveCommand("start", CmegajpStart, arena);
        
        return MM_OK;
    }
    return MM_FAIL;
}
