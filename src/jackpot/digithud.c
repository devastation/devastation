/* digithud
 *
 * meant for an arena or per-player hud showing a number.
 *
 * [DigitHUD]
 * Digits = 10
 * ; max number of digits
 * PrefixZeros = 0
 * ; if the number is say 10 and Digits = 5, do you want it to show as 00010
 * BackgroundObjectID = 0
 * ; the background to the hud
 * UnitsDigitObjectID = 0
 * ; the rest are automatically calaculate:
 * ; TensDigitObjectID = UnitsDigitObjectID + 1
 * ; and so on...
 * DigitStartImageID = 0
 * ; this should match the IMAGE0 type stuff in the lvz ini
 * ; start image is letter '0', the next is '1' and so on...
 *
 * jul 1 2006 smong.
 *            note: requires my patched objects.c to all Target.type = T_PLAYER
 *             in objs->Image().
 *
 */

#include "asss.h"
#include <math.h>
#include "objects.h"
#include "digithud.h"
#include "jackpot.h"
#include "fg_wz.h"

struct adata
{
	byte cfg_digits;
	byte cfg_prefixzeros;
	short cfg_backgroundobjid;
	short cfg_unitsobjid;
	short cfg_startimgid;
};

local void ArenaAction(Arena *arena, int action);
local void Off(const Target *target);
local void Update(const Target *target, int newnumber);

local int adkey;

local Iarenaman *aman;
local Iconfig *cfg;
local Iobjects *objs;


local void ArenaAction(Arena *arena, int action)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);

	if (action == AA_CREATE || action == AA_CONFCHANGED)
	{
		ConfigHandle ch = arena->cfg;
		ad->cfg_digits = cfg->GetInt(ch, "DigitHUD", "Digits", 10);
		ad->cfg_prefixzeros = cfg->GetInt(ch, "DigitHUD", "PrefixZeros", 0);
		ad->cfg_backgroundobjid = cfg->GetInt(ch, "DigitHUD", "BackgroundObjectID", 0);
		ad->cfg_unitsobjid = cfg->GetInt(ch, "DigitHUD", "UnitsDigitObjectID", 0);
		ad->cfg_startimgid = cfg->GetInt(ch, "DigitHUD", "StartImageID", 0);
	}
}


local void Off(const Target *target)
{
	Arena *arena = NULL;
	struct adata *ad;
	short id[11];
	char ons[11];
	int i;

	if (target->type == T_ARENA)
	{
		arena = target->u.arena;
	}
	else if (target->type == T_PLAYER)
	{
		arena = target->u.p->arena;
	}
	else
	{
		/* only arena and player targets supported */
		return;
	}

	ad = P_ARENA_DATA(arena, adkey);

	id[0] = ad->cfg_backgroundobjid;
	ons[0] = FALSE;

	for (i = 0; i < ad->cfg_digits; i++)
	{
		id[i + 1] = ad->cfg_unitsobjid + i;
		ons[i + 1] = FALSE;
	}

	objs->ToggleSet(target, id, ons, ad->cfg_digits + 1);
}

local void Update(const Target *target, int newnumber)
{
	Arena *arena = NULL;
	struct adata *ad;
	short id[11];
	char ons[11];
	int i;
	int digit, value;

	if (target->type == T_ARENA)
	{
		arena = target->u.arena;
	}
	else if (target->type == T_PLAYER)
	{
		arena = target->u.p->arena;
	}
	else
	{
		/* only arena and player targets supported */
		return;
	}

	ad = P_ARENA_DATA(arena, adkey);
	value = newnumber;

	id[0] = ad->cfg_backgroundobjid;
	ons[0] = TRUE;

	for (i = 0; i < ad->cfg_digits; i++)
	{
		id[i + 1] = ad->cfg_unitsobjid + i;
		
		if (value == 0 && !ad->cfg_prefixzeros && i != 0)
		{
			ons[i + 1] = FALSE;
		}
		else
		{
			digit = value % 10;
			value /= 10;
			ons[i + 1] = TRUE;
			objs->Image(target,
				ad->cfg_unitsobjid + i,
				ad->cfg_startimgid + digit);
		}
	}

	objs->ToggleSet(target, id, ons, ad->cfg_digits + 1);
}


local Idigithud _myint =
{
	INTERFACE_HEAD_INIT(I_DIGITHUD, "digithud")
	Off, Update
};


EXPORT const char info_digithud[] =
	"v1 by smong <soinsg@hotmail.com>";

EXPORT int MM_digithud(int action, Imodman *mm, Arena *arena)
{
	if (action == MM_LOAD)
	{
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		objs = mm->GetInterface(I_OBJECTS, ALLARENAS);
		if (!aman || !cfg || !objs) return MM_FAIL;

		adkey = aman->AllocateArenaData(sizeof(struct adata));
		if (adkey == -1) return MM_FAIL;

		mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

		mm->RegInterface(&_myint, ALLARENAS);

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		if (mm->UnregInterface(&_myint, ALLARENAS))
			return MM_FAIL;

		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

		aman->FreeArenaData(adkey);

		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(objs);
		return MM_OK;
	}
	return MM_FAIL;
}

