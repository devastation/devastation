/* jpcounter2 (attach module)
 * requires: jackpot, digithud (which in turn requires objects)
 *
 * a jackpot meter for asss that uses the digithud module.
 * see digithud for lvz settings.
 *
 * aug 1 2007 started by smong for acekiller12 (and probably a load of other people I forgot about)
 *
 */

#include "asss.h"
#include "jackpot.h"

#include "digithud.h"

#define CFG_UPDATETICKS 2000

local void Kill(Arena *arena, Player *killer, Player *killed,
		int bounty, int flags, int pts, int green);
local void FlagReset(Arena *a, int freq, int points);

local void update_meter(Arena *arena);

local int timer(void *arena_);

local Imainloop *ml;
local Ijackpot *jackpot;
local Idigithud *digithud;


local void update_meter(Arena *arena)
{
	Target tgt;
	int jp = jackpot->GetJP(arena);
	tgt.type = T_ARENA;
	tgt.u.arena = arena;
	digithud->Update(&tgt, jp);
}

local void Kill(Arena *arena, Player *killer, Player *killed,
		int bounty, int flags, int pts, int green)
{
	update_meter(arena);
}

local void FlagReset(Arena *arena, int freq, int points)
{
	/* update after jp has been cleared */
	ml->ClearTimer(timer, arena);
	ml->SetTimer(timer, 100, CFG_UPDATETICKS, arena, arena);
}

/* periodically update the jp meter just incase we missed something */
local int timer(void *arena_)
{
	Arena *arena = arena_;
	update_meter(arena);
	return 1;
}


EXPORT const char info_jpcounter2[] =
	"v2 by smong <soinsg@hotmail.com>";

EXPORT int MM_jpcounter2(int action, Imodman *mm, Arena *arena)
{
	if (action == MM_LOAD)
	{
		ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
		jackpot = mm->GetInterface(I_JACKPOT, ALLARENAS);
		digithud = mm->GetInterface(I_DIGITHUD, ALLARENAS);
		if (!ml || !jackpot || !digithud) return MM_FAIL;
		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		ml->ClearTimer(timer, NULL);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(jackpot);
		mm->ReleaseInterface(digithud);
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		ml->SetTimer(timer, 100, CFG_UPDATETICKS, arena, arena);
		mm->RegCallback(CB_KILL, Kill, arena);
		mm->RegCallback(CB_FLAGRESET, FlagReset, arena);
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		ml->ClearTimer(timer, arena);
		mm->UnregCallback(CB_KILL, Kill, arena);
		mm->UnregCallback(CB_FLAGRESET, FlagReset, arena);
		return MM_OK;
	}
	return MM_FAIL;
}

