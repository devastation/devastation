/**************************************
 * Credits Counter Module             *
 *                                    *
 * Ported by Hallowed be thy name,    *
 * Modified by Hakaku                 *
 * + Thanks to MGB for help           *
 *                                    *
 **************************************/

//TODO: fix SendZero (INT_MAX vs 0), ArenaAction ConfChange

#include <math.h>
#include <limits.h>
#include "asss.h"
#include "credits.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
    char toggle;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Icredits *creds;
local Iobjects *obj;
local Iconfig *cfg;
local Ichat *chat;
local Iplayerdata *pd;
local Icmdman *cmd;

int sendZero;

int calculateID(int num, int offset)
{
	return 2000 + (10 * (offset + 1)) + num;
}

local void sendCredits(Player *p, int new_credits, int old_credits, int show_zeros)
{
	int offset;
	int new_num, old_num;
	int old_object, new_object;

//	printf("Old Credits: %d\n", old_credits);
//	printf("New Credits: %d\n", new_credits);
//	printf("Showing zeros: %s\n", show_zeros ? "TRUE" : "FALSE");
//	printf("\n");

    Target target;
    target.type = T_PLAYER;
    target.u.p = p;

    Pdata *data = PPDATA(p, playerKey);

	for (offset = 9; offset >= 0; offset--)
	{
        if (new_credits < 0) //send 0 if the number is negative
        {
            new_credits = 0;
        }

        obj->Toggle(&target, 2001, 1);

        new_num = new_credits % 10;
		old_num = old_credits % 10;
		new_credits /= 10;
		old_credits /= 10;

		// Calculate new object's ID
		if ((new_num == 0) && (new_credits == 0) && (!show_zeros))
		{
			new_object = 0;
		}
		else
		{
			new_object = calculateID(new_num, offset);
		}

		// Calculate old object's ID
		if (old_num == 0 && old_credits == 0 && !show_zeros)
		{
			old_object = 0;
		}
		else
		{
			old_object = calculateID(old_num, offset);
		}

		// Toggle objects only if they are different
		if ((old_object != new_object) && (data->toggle != 0))
		{
			if (old_object)
			    obj->Toggle(&target, old_object, 0); //remove

			if (new_object)
				obj->Toggle(&target, new_object, 1); //add
		}
		else if ((old_object == new_object && old_object) && (data->toggle != 0))
		{
//			printf("%d ignored\n", old_object);
		}

		if (!data->toggle)
		{
            obj->Toggle(&target, 2001, 0);
            obj->Toggle(&target, old_object, 0);
            obj->Toggle(&target, new_object, 0);
        }
	}
}

/* Update Credits */
local void cUpdate(Player *p, int old)
{
    int credits = creds->GetCredits(p);
    sendCredits(p, credits, old, sendZero);
}

/* Player entering arena, leaving arena */
local void cPlayerAction(Player *p, int action, Arena *arena)
{
    Pdata *data = PPDATA(p, playerKey);
    int credits = creds->GetCredits(p);
    if (action == PA_ENTERARENA)
    {
        data->toggle = 1;
        sendCredits(p, INT_MAX, 0, sendZero); //bad hack - fixme
        sendCredits(p, 0, INT_MAX, sendZero);
        sendCredits(p, credits, 0, sendZero);
    }
    if (action == PA_LEAVEARENA)
    {
        sendCredits(p, credits, 0, sendZero);
    }
}

local helptext_t togglecreds_help =
"Targets: none\n"
"Args: none\n"
"Using ?togglecreds will either turn the credits counter (display) on or off.\n";

/* ?togglecreds */
local void cTogglecreds(const char *command, const char *params, Player *p, const Target *target)
{
	Pdata *data = PPDATA(p, playerKey);
    int credits = (int)creds->GetCredits(p);

	if (data->toggle == 0)
	{
        data->toggle = 1;
        sendCredits(p, credits, 0, sendZero);
        chat->SendMessage(p, "Credits Counter ON. ");
    }
    else
    {
        data->toggle = 0;
        sendCredits(p, credits, 0, sendZero);
        chat->SendMessage(p, "Credits Counter OFF.");
    }
}

EXPORT const char info_c_counter[] = "(Devastation) v1.4 originally Hallowed be thy name, modified by Hakaku, thanks to Mine Go Boom for help";

EXPORT int MM_c_counter(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        creds = mm->GetInterface(I_CREDITS, ALLARENAS);
        obj = mm->GetInterface(I_OBJECTS, ALLARENAS);
        cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        cmd = mm->GetInterface(I_CMDMAN, arena);

        if (!creds || !obj || !cfg || !chat || !pd)
        {
            // release interfaces if loading failed
			mm->ReleaseInterface(pd);
			mm->ReleaseInterface(chat);
			mm->ReleaseInterface(cfg);
			mm->ReleaseInterface(obj);
			mm->ReleaseInterface(creds);
			mm->ReleaseInterface(cmd);

            return MM_FAIL;
        }
        else
        {
            playerKey = pd->AllocatePlayerData(sizeof(Pdata));
            if (!playerKey)
            {
                return MM_FAIL;
            }
            else
            {
                mm->RegCallback(CB_PLAYERACTION, cPlayerAction, ALLARENAS);
                mm->RegCallback(CB_CREDITS, cUpdate, ALLARENAS);

                cmd->AddCommand("togglecreds", cTogglecreds, ALLARENAS, togglecreds_help);
                cmd->AddCommand("togglecredits", cTogglecreds, ALLARENAS, togglecreds_help);
                sendZero = cfg->GetInt(GLOBAL, "CreditCounter", "SendZeroes", 0);
                return MM_OK;
            }
        }
    }
    else if (action == MM_UNLOAD)
    {
        cmd->RemoveCommand("togglecreds", cTogglecreds, ALLARENAS);
        cmd->RemoveCommand("togglecredits", cTogglecreds, ALLARENAS);

        mm->UnregCallback(CB_CREDITS, cUpdate, ALLARENAS);
        mm->UnregCallback(CB_PLAYERACTION, cPlayerAction, ALLARENAS);

        mm->ReleaseInterface(obj);
        mm->ReleaseInterface(creds);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(pd);

        return MM_OK;
    }
    else if (action == MM_ATTACH)
    {
        return MM_OK;
    }
    else if (action == MM_DETACH)
    {
        return MM_OK;
    }
    return MM_FAIL;
}
