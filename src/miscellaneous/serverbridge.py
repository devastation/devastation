from asss import *
import traceback

ARENA_TYPE_THINGY_PUB	= -1
ARENA_TYPE_THINGY_NAMED	= -3  #don't mess with this
ARENA_SEPRATOR = '2'
BRIDGECROSSING_MESSAGE = "%s has left the great server of %s to investigate activity in the %s arena of %s. Type ?go %s to join the fight!"
serverDB={
	#"deva"	:	("SSY Devastation","67.23.225.164",7022),
	#"3s"	:	("SSN Tri-Sector","108.59.253.96",6014),
	#"3smoo"	:	("SSN Tri-Sector","108.59.253.96",6014,moo),
	#"deva"	:	("SSCA Devastation","75.101.147.52",7022),
	#"3s"	:	("SSCL Tri-Sector","69.164.209.244",6014),
	#"deva"	:	("SSCX Devastation","108.61.44.148",7022),
	"deva"	:	("SSCJ Devastation","69.164.220.203",7022),
	#NOTE: Trench wars' sysadmins asked us to specifically redirect to this arena to prevent an issue with their bots
	"tw"	:	("SSCU Trench Wars","66.36.247.83",5400,'tw'),
	#NOTE: EG's sysops asked us to solve the unreachable arena issue, so I made everyone send to the afk subarena instead
	"eg"	:	("SSCU Extreme Games","66.235.184.102",7900),
	"cz"	:	("SSCX Chaos/League Zone SVS","66.234.184.102",13500,),
	"sw"	:	("SSCX Star Warzone","66.235.184.102",6000,),
	"pb"	:	("SSCX PowerBall","66.36.243.61",8600,),
	"hz"	:	("SSCE Hockey/Football Zone","208.122.59.226",7501,),
	"dsb"	:	("SSCU Death Star Battle","209.160.65.137",3600,),
	}
fakeDB={}
this_servername = "SSCL Tri-Sector"#default name, gets read from the config
redirect=None
fake=None
chat	= get_interface(I_CHAT)
cfg	 	= get_interface(I_CONFIG)
fake	= get_interface(I_FAKE)
try:
	redirect = get_interface(I_REDIRECT)
except:
	formatted_lines = traceback.format_exc().splitlines()
	for line in formatted_lines:
		chat.SendModMessage(line)
	raise
if redirect is None:
	chat.SendModMessage("redirect is still None")

try:
	this_servername = cfg.GetStr(None,'billing','servername')
except:
	pass
del cfg
	
def do_redirect(p):
	arenasplit = p.arena.name.split(ARENA_SEPRATOR)
	serverInfo = serverDB[arenasplit[0]]
	
	arena="0" #pub my default
	arenaname="public"
	arenaType=ARENA_TYPE_THINGY_PUB
	if len(serverInfo) == 4: #if serverInfo has default arena
		arenaname=arena=serverInfo[3]
		arenaType=ARENA_TYPE_THINGY_NAMED
	if len(arenasplit) > 1: #if zone-request contained a specific subarena
		arenaname=arena=ARENA_SEPRATOR.join(arenasplit[1:])
		arenaType=ARENA_TYPE_THINGY_NAMED
	# from numpf 2012-09-04:
	# I strongly suspect the PlayerListType is bugged, try to avoid using it for now.
	# you could implement the commented code below with:
	def cb_send(np):
		chat.SendMessage(np, BRIDGECROSSING_MESSAGE % (p.name,this_servername,arenaname,serverInfo[0],p.arena.name))
	for_each_player(cb_send)
	
	#	plist = PlayerListType()
	#	def cb_send(t):
	#		plist.append(t)
	#	for_each_player(cb_send)
	#	chat.SendAnyMessage(plist, MSG_FUSCHIA, 0, None, BRIDGECROSSING_MESSAGE % (p.name,this_servername,arenaname,serverInfo[0],p.arena.name))
	
	redirect.RawRedirect(p,serverInfo[1],serverInfo[2],arenaType,arena)
	#redirect.RawRedirect(p,ADDRESS,PORT,ARENA_TYPE_THINGY,ARENA_TO_REDIRECT)

class RedirectFakeHandler():
	def __init__(self,arena):
		self.ref=fake.CreateFakePlayer('Redirect Failed',arena,SHIP_SPEC,0)
	def __del__(self):
		fake.EndFaked(self.ref)

def cb_paction(p, action, data):
	if action == PA_ENTERARENA:
		arenaBaseName = p.arena.name.split(ARENA_SEPRATOR)[0].lower()
		if  arenaBaseName in serverDB:
			if arenaBaseName == p.arena.name and arenaBaseName not in fakeDB:
				fakeDB[arenaBaseName]=RedirectFakeHandler(p.arena)
			do_redirect(p)
callbacks = reg_callback(CB_PLAYERACTION, cb_paction)

