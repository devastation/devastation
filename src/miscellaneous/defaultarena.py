
import asss
chat = asss.get_interface(asss.I_CHAT)


defaultArenaName = ""

class ArenaPlace:
        iid = asss.I_ARENAPLACE
        
        def Place(me, player):
                global defaultArenaName
                placed = 0
                if defaultArenaName:
                        placed = 1
                return (placed, defaultArenaName, -1, -1) # placed true/false, arenaname, spawn x, spawn y


def Cdefaultarena(command, params, player, target):
        """\
Targets: none
Args: [-r] arenaname
Sets the arena where players will be placed if they connect to this zone.
Use -r to unset.
This is a temporary setting, it does not survive a server restart.
Useful for events.
"""
        global defaultArenaName
        params = params.strip()
        if not params:
                if defaultArenaName:
                        chat.SendCmdMessage(player, "The default arena is: " + defaultArenaName)
                        
                else:
                        chat.SendCmdMessage(player, "The default arena is not set")
                return
        
        if params == "-r":
                defaultArenaName = "" 
        else:
                defaultArenaName = params
        
        if defaultArenaName:
                chat.SendCmdMessage(player, defaultArenaName + " is now the default arena")
        else:
                chat.SendCmdMessage(player, "Default arena has been disabled")

intf = asss.reg_interface(ArenaPlace(), None)
cmd1 = asss.add_command("defaultarena", Cdefaultarena, None)
