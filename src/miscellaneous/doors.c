 /**************************************************************
 * Doors Module
 *
 * Opens, closes, and returns the door state back to normal.
 *
 * ************************************************************/
#include <string.h>

#include "asss.h"
#include "clientset.h"
#include <stdlib.h>
#include <string.h>

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Icmdman *cmd;
local Iplayerdata *pd;
local Iconfig *cfg;
local Iclientset *cs;
local Iarenaman *aman;

local override_key_t ok_Doormode;

/************************************************************************/
/*                          Player Commands                             */
/************************************************************************/

local helptext_t doors =
"Targets: none\n"
"Args: open, close, normal\n"
"Using ?doors <open/close/normal/bitfield made out of 8 '1's and '0's/doormode as a single int>\n"
"Will either open them, close them, randomize them, or set them to the requested state.\n";

/* When someone types ?doors*/
local void cDoors(const char *command, const char *params, Player *p, const Target *target)
{
    char *find;
	Player *g;
	Link *link;
    if ((find = strstr(params, "off")) || (find = strstr(params, "open")))
    {
        cs->ArenaOverride(p->arena, ok_Doormode, 0);
        chat->SendArenaSoundMessage(p->arena, 2, "Doors will now open.");
    }
    else if ((find = strstr(params, "on")) || (find = strstr(params, "close")))
    {
        cs->ArenaOverride(p->arena, ok_Doormode, 255);
        chat->SendArenaSoundMessage(p->arena, 2, "Doors will now close.");
    }
    else if ((find = strstr(params, "normal")) || (find = strstr(params, "random")))
    {
        cs->ArenaUnoverride(p->arena, ok_Doormode);
        chat->SendArenaSoundMessage(p->arena, 2, "Doors will now return to normal.");
    }
    else if (strspn(params, "01") == 8)
	{
		unsigned char flagmode = (char)0, bitval = 1;
		for (int i = 0; i<8; i++)
		{
			if (params[i] == '1')
				flagmode += bitval;
			bitval += bitval; //bitval *=2;
		}
        
        cs->ArenaOverride(p->arena, ok_Doormode, flagmode);
		chat->SendArenaSoundMessage(p->arena, 2, "Doors will now be set to %d.", (unsigned int)flagmode);
    }
    else
    {
		unsigned int flagmode = atoi(params);
		if (strlen(params) > 0 && 0<=flagmode && flagmode <= 255)
		{
			cs->ArenaOverride(p->arena, ok_Doormode, (unsigned char) flagmode);
			chat->SendArenaSoundMessage(p->arena, 2, "Doors will now be set to %d.", flagmode);
		}
		else //if bad Syntax
		{
        	chat->SendMessage(p, "Syntax: ?doors open/close/normal/bitfield made out of 8 '1's and '0's/doormode as a single int");
        	return; //skip sending setts to all players
		}
    }
    //send updated settings to all players
    pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == p->arena)
			cs->SendClientSettings(g);
	}
	pd->Unlock();
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_doors[] = "(Devastation) v0.6 Hakaku and a few addons by Avihay";

EXPORT int MM_doors(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;

        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
        cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);
        cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
        cs = mm->GetInterface(I_CLIENTSET, ALLARENAS);
        aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
        
        if (!chat || !pd || !cmd || !cfg || !cs || !aman)
        {
            mm->ReleaseInterface(aman);
            mm->ReleaseInterface(cs);
            mm->ReleaseInterface(cfg);
            mm->ReleaseInterface(cmd);
            mm->ReleaseInterface(pd);
            mm->ReleaseInterface(chat);
            return MM_FAIL;
        }
        
        return MM_OK;
    }
    else if (action == MM_UNLOAD)
    {
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(cs);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(chat);
        
        return MM_OK;
    }
    else if (action == MM_ATTACH)
    {
        cmd->AddCommand("doors", cDoors, arena, doors);
        cmd->AddCommand("doormode", cDoors, arena, doors);
            
        ok_Doormode = cs->GetOverrideKey("Door", "Doormode");

        return MM_OK;
    }
    else if (action == MM_DETACH)
    {
        cmd->RemoveCommand("doors", cDoors, arena);
        cmd->RemoveCommand("doormode", cDoors, arena);

        return MM_OK;
    }
    return MM_FAIL;
}
