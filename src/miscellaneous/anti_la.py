#LA prevention prototype B by Avihay.b
#seems the only way to detach a player without manually sending packets in 
#python is with a shipchange
#changelog:
#29/9/11 - first ver
#4/10/11 - added some fair LA time and renamed prototype B
import os, traceback,time# datetime
from asss import *
### How much time after official death will a player still be able to attach to target ###
#FAIR_LA_DELAY = 75
FAIR_LA_DELAY = 200
LA_PREVENTION_SOUND=None



chat = get_interface(I_CHAT)
game = get_interface(I_GAME)
#flagcore = get_interface(I_FLAGCORE)
#stats = get_interface(I_STATS)
#jackpot = get_interface(I_JACKPOT)
#objs = get_interface(I_OBJECTS)
#players = get_interface(I_PLAYERDATA)
cfg = get_interface(I_CONFIG)


class timer():
	def __init__(self, func, time, interval=None, args=None):
		self.func=func
		self.time=time
		self.interval=interval
		self.args=args
		if interval is None or interval<=0:
			self.ref=set_timer(self.dispatch, time, 10000)
		else:
			self.ref=set_timer(self.dispatch, time, interval)
	def dispatch(self):
		try:
			#if self.args is None:
			#	self.func()
			#else:
			#try:
			self.func(*self.args)
			#except TypeError: #this is dumb, I blame python!
			#	self.func(self.args)
			if self.interval is None or self.interval<=0:
				self.remref()
		except:
			formatted_lines = traceback.format_exc().splitlines()
			for line in formatted_lines:
				chat.SendModMessage(line)
	def remref(self):
		self.ref=None
		
def reg_debug_callback(id, cb, arena):
	def dispatch(*args):
		try:
			return cb(*args)
		except:
			formatted_lines = traceback.format_exc().splitlines()
			for line in formatted_lines:
				chat.SendModMessage(line)
	return reg_callback(id,dispatch, arena)
	
class Anti_la():
	def __init__(self, arena):
		self.arena=arena
		self.cb_readsettings()
		self.callbacks=[reg_debug_callback(CB_KILL, self.cb_kill, arena),
		                reg_debug_callback(CB_ATTACH, self.cb_attach, arena),
		                #reg_debug_callback(CB_PLAYERACTION, self.cb_paction, arena),
		                reg_debug_callback(CB_ARENAACTION, self.cb_arenaAction, arena),
		                reg_debug_callback(CB_GLOBALCONFIGCHANGED, self.cb_readsettings, arena)]
		self.dead={}
		#chat.SendArenaSoundMessage(self.arena, 13, "[Anti_La]LA prevention module prototype C by Avihay has been attached. your breakage may vary. you may only attach for %s msec after death." % (FAIR_LA_DELAY*10))
		chat.SendArenaSoundMessage(self.arena, 13, "[Anti_La]LA prevention module has been Enabled. you may only attach for %s msec after death." % (FAIR_LA_DELAY*10))

	def remref(self):
		#chat.SendArenaSoundMessage(self.arena, 22, "[Anti_La]LA prevention module prototype C by Avihay has been detached. sanity restored")
		chat.SendArenaSoundMessage(self.arena, 22, "[Anti_La]LA prevention module has been Disabled.")
		self.callbacks=None
		#for player in self.dead:
		#	self.dead[player].remref()
		self.dead=None
		
	def cb_kill(self,  arena, killer, killed, bty, flagcount, pts, green):
		if arena!=self.arena: return pts, green
		#self.dead[killed]=(timer(self.fairLA,FAIR_LA_DELAY,None,[killed]),time.clock())
		killed.anti_la_lastkilled = time.time()
		return pts, green
	#def fairLA(self,player):
		#self.dead[player][0].remref()
		#self.dead[player]=(timer(self.revive,self.enter_delay-FAIR_LA_DELAY,None,[player]),self.dead[player][1])
	#def revive(self,player):
		#self.dead[player][0].remref()
		#del self.dead[player]
		
	def cb_attach(self, player, target):
		if player.arena!=self.arena: return
		if target is None: return
		try:
			target.anti_la_lastkilled
		except:
			#chat.SendModMessage("[Anti_La]no deathstamp yet for player %s " % (target.name))
			return
		now=time.time()
		deadfor=(now-target.anti_la_lastkilled)*100.0
		if deadfor < self.enter_delay:
			if deadfor > FAIR_LA_DELAY:
				#chat.SendModMessage("[Anti_La]Aborting Lag attach by %s in (%s,%s) to %s who is considered dead for %s msec." % (player.name,player.position[0]/16,player.position[1]/16,target.name,deadfor*10.0  ))
				if LA_PREVENTION_SOUND is None:
					chat.SendMessage(player,"[Anti_La]Your attach has been aborted because the attach target is already dead for %s msec." % (deadfor*10.0 ))
				else:
					chat.SendMessage(player,LA_PREVENTION_SOUND,"[Anti_La]Your attach has been aborted because the attach target is already dead for %s msec." % (deadfor*10.0 ))
				ship=player.ship
				game.SetShip(player,(ship+1)%8)
				game.SetShip(player,ship)
			else:
				#chat.SendModMessage("[Anti_La]Successfull Lag attach by %s to %s who is considered dead for %s msec." % (player.name,target.name,deadfor*10.0 ))
				pass
			
	#def cb_paction(self, p, action, arena):
		#if arena!=self.arena: return
		#if action == PA_LEAVEARENA:
			#if p in self.dead:
				#self.revive(p)
	def cb_arenaAction(self,arena,action):
		if arena!=self.arena: return
		if action == AA_CREATE or action == AA_CONFCHANGED:
			self.cb_readsettings()
	def cb_readsettings(self):
		self.enter_delay=cfg.GetInt(self.arena.cfg,'Kill','EnterDelay',300)
		chat.SendModMessage("[Anti_La]Read respawn time as %s" % self.enter_delay)
		

def mm_attach(arena):
	try:
		arena.anti_la = Anti_la(arena)
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(line)

def mm_detach(arena):
	try:
		arena.anti_la.remref()
		arena.anti_la= None
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(line)

