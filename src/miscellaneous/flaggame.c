 /**************************************************************
 * FlagGame Module
 *
 * 1. Warn players entering safe with flags
 * 2. Message the jackpot after every so threshhold
 * 3. Take flag away from player with 25 flags entering safe
 * 4. ?flagstocenter
 * ************************************************************/

#include "asss.h"
#include "jackpot.h"
#include "chatbot.h"
#include "objects.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Icmdman *cmd;
local Iplayerdata *pd;
local Ichatbot *chatbot;
local Iflagcore *flags;
local Iconfig *cfg;
local Ijackpot *jp;
local Iobjects *obj;

local int jpBroadcastThreshold;
local int lastJP;
local char numFlags;

/************************************************************************/
/*                           Game Callbacks                             */
/************************************************************************/

/* Warn player with flags while entering safe. */
local void cFlagCheck(Player *p, int x, int y, int entering)
{
    if (entering > 0)
    {
        if (flags->CountPlayerFlags(p) > 0)
        {
            Link link = { NULL, p };
            LinkedList lst = { &link, &link };
            chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, SOUND_BEEP1, NULL,	"WARNING: It is illegal to enter a safety zone with flags!");
            chat->SendModMessage("[%s] entered safety zone with flags", p->name);
        }
    }
}
//This partial QuickwinCheck will verify if a player who enters a safe has over 24 flags.
//It will then take away a flag (id: 0) and place it at center.
//Flags that are dropped from safe are determined in the settings [flag] SafeOwned=0
local void cQuickwinCheck(Player *p, int x, int y, int entering)
{
    if (entering > 0)
    {
        int i = 0;
        FlagInfo fi;
        flags->GetFlags(p->arena, i, &fi, 1);
        if (flags->CountPlayerFlags(p) > 24)
        {
            fi.state = FI_NONE;
            fi.carrier = NULL;
            flags->SetFlags(p->arena, i, &fi, 1);
            chat->SendModMessage("A flag has been removed from player %s (possible Quickwin)", p->name);
        }
    }
}

/* Announce the jackpot every so many points. */
local void cKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    int jackpot = jp->GetJP(arena);
    if (jackpot > lastJP)
    {
        int old = lastJP  / jpBroadcastThreshold;
        int new = jackpot / jpBroadcastThreshold;

        if (old != new && old < jpBroadcastThreshold * 1)
        {
            lastJP = jackpot;
            chat->SendArenaSoundMessage(arena, 2, "Jackpot is now over %i million points!", jackpot / 1000000);
            
            if (chatbot)
                chatbot->SendChatMessage("Jackpot is now over %i million points!", jackpot / 1000000);
        }
    }
}

/************************************************************************/
/*                          Player Commands                             */
/************************************************************************/

local helptext_t flagstocenter =
"Targets: none\n"
"Args: {-c}\n"
"Neutralizes and warps all flags back to center\n"
"If {-c} is added, it will organise (but not neut) all flags in center.";

/* ?flagstocenter */
local void cCenterFlags(const char *command, const char *params, Player *p, const Target *target)
{
    int i=0;
    int x = 509, y = 512;
    FlagInfo fi;
    for (; i < numFlags; i++)
    {
        flags->GetFlags(p->arena, i, &fi, 1);
        if (fi.state != FI_CARRIED)
        {
            if (x == 515)
            {
                x = 509;
                y++;
            }
            fi.x = x;
            fi.y = y;
            x++;
            if (!strstr(params, "-c"))
            {
                /* Neutralize the flags sporadically */
                fi.state = FI_NONE;
            }
            flags->SetFlags(p->arena, i, &fi, 1);
        }
    }
    chat->SendArenaSoundMessage(p->arena, 26, "All uncarried flags have been sent to center!");
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_flaggame[] = "(Devastation) v0.5 Hakaku";

EXPORT int MM_flaggame(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        
        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
        chatbot = mm->GetInterface(I_CHATBOT, ALLARENAS);
        flags = mm->GetInterface(I_FLAGCORE, ALLARENAS);
        cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);
        cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
        jp = mm->GetInterface(I_JACKPOT, ALLARENAS);
        obj = mm->GetInterface(I_OBJECTS, ALLARENAS);

        if (!chat || !pd || !flags || !cmd || !cfg || !jp || !obj) //!chatbot
            return MM_FAIL;
        
        return MM_OK;
    }
    else if (action == MM_UNLOAD)
    {
        mm->ReleaseInterface(obj);
        mm->ReleaseInterface(jp);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(chatbot);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(chat);
        
        return MM_OK;
    }
    else if (action == MM_ATTACH)
    {
        mm->RegCallback(CB_SAFEZONE, cFlagCheck, arena);
        mm->RegCallback(CB_SAFEZONE, cQuickwinCheck, arena);
        mm->RegCallback(CB_KILL, cKill, arena);

        cmd->AddCommand("flagstocenter", cCenterFlags, arena, flagstocenter);

        jpBroadcastThreshold = cfg->GetInt(arena->cfg, "DevaJunk", "JPBroadcastThreshold", 3000000);
        numFlags = cfg->GetInt(arena->cfg, "Flag", "FlagCount", 3);

        return MM_OK;
    }
    else if (action == MM_DETACH)
    {
        cmd->RemoveCommand("flagstocenter", cCenterFlags, arena);

        mm->UnregCallback(CB_KILL, cKill, arena);
        mm->UnregCallback(CB_SAFEZONE, cFlagCheck, arena);
        mm->UnregCallback(CB_SAFEZONE, cQuickwinCheck, arena);
        
        return MM_OK;
    }
    return MM_FAIL;
}

