# Made by Hakaku, May 29th, 2007
# Based on Smong's module example

from asss import *

chat = get_interface(I_CHAT)

def c_how(cmd, params, p, targ):
# This does ?help how
    """\
Module: <py> how
Targets: none
Typing '?how', or '?how to play' displays intructions on how to play Devastation.
"""
    chat.SendMessage(p, "|--------------------------------- How to play Devastation ---------------------------------|")
    chat.SendMessage(p, "| a) Kill all enemies.                                                                      |")
    chat.SendMessage(p, "| b) Get flags from enemies or enemy base and take them to your team's base.                |")
    chat.SendMessage(p, "| c) Guard flags and defend base.                                                           |")
    chat.SendMessage(p, "| d) DO NOT KEEP FLAGS OR PICK UP YOUR OWN TEAM'S FLAGS - carry them to your base and neut. |")
    chat.SendMessage(p, "| * Picking up all the flags and dropping them causes the flag game to be won.              |")
    chat.SendMessage(p, "| * Not picking up causes the game to go longer and the jackpot to rise! $$$                |")
    chat.SendMessage(p, "| * Therefore, do not pick flags up unless you are really desperate to win!                 |")
    chat.SendMessage(p, "| e) To drop flags without triggering a victory (neut), just CHANGE SHIP -                  |")
    chat.SendMessage(p, "|    they will drop as neutral (white) flags.                                               |")
    chat.SendMessage(p, "|-------------------------------------------------------------------------------------------|")
    chat.SendMessage(p, "[If you do not see the full text, press ESC]")

# this adds the command "?how", so "?how to play" will also work
cmd1 = add_command("how", c_how)
cmd2 = add_command("howtoplay", c_how)
