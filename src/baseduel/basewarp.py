#Last change: 
#removed dependency in unused modules 11/8/12
#added arena descrimination and multiple freq warps 26/10/11
#added loading settings from file 25/2/11
from asss import *
import traceback

chat = get_interface(I_CHAT)
game = get_interface(I_GAME)
#flagcore = get_interface(I_FLAGCORE)
#stats = get_interface(I_STATS)
#jackpot = get_interface(I_JACKPOT)
#objs = get_interface(I_OBJECTS)
#cmd = get_interface(I_CMDMAN)

GAMESTART_SOUND = 104
CONFIG_FILE = 'conf/basewarp.conf'
#the name of the arena who's spawnpoints are registered for all arenas
GLOBAL_ARENA_NAME = 'global'
prefix = lambda s:'[BaseWarp]%s' % (s)
basewarpSpawns = { GLOBAL_ARENA_NAME:{} }
def readconf():
	arena=GLOBAL_ARENA_NAME
	i=0
	try:
		settings=open(CONFIG_FILE,'r')
		for line in settings:
			i+=1
			line=line.strip()
			if line[0]==';':
				continue
			if line[0]=='[' and line[-1]==']':
				arena=line[1:-1].lower()
				if arena not in basewarpSpawns:
					basewarpSpawns[arena] = {}
				continue
			params=line.split(' ')
			basewarpSpawns[arena][params[0]]=(int(params[1]),int(params[2]),int(params[3]),int(params[4]))
		settings.close()
	except:
		chat.SendModMessage(prefix('Loading config file "%s" failed at line: %s' %(CONFIG_FILE,i)))
		chat.SendModMessage(prefix('To set spawnpoints manually look at ?basewarp once the module is attached to an arena'))
		raise
def getSpawn(name,arenaname):
	arenaname=arenaname.lower()
	if arenaname in basewarpSpawns:
		if name in basewarpSpawns[arenaname]:
			return basewarpSpawns[arenaname][name]
	if name in basewarpSpawns[GLOBAL_ARENA_NAME]:
		return basewarpSpawns[GLOBAL_ARENA_NAME][name]
	return None
def isSpawn(name,arenaname):
	arenaname=arenaname.lower()
	if arenaname in basewarpSpawns:
		if name in basewarpSpawns[arenaname]:
			return True
	if name in basewarpSpawns[GLOBAL_ARENA_NAME]:
		return True
	return False
def setSpawn(name, arenaname, spawnpoints):
	if arenaname not in basewarpSpawns:
		basewarpSpawns[arenaname] = {}
	basewarpSpawns[arenaname][name] = spawnpoints


class basewarp():
	def __init__(self, arena):
		self.arena=arena
		#self.spawns={}
		self.cmd = add_command("basewarp", self.cmd_basewarp, arena)
		
		#chat.SendModMessage('registered command %s'% (self.cmd))

	def printusage(self,cmd,p):
		chat.SendMessage(p,
		prefix("Usage: ?%s <basename> or ?%s <basename> <basefreq> to start a game, or ?%s set <basename> <f0-x> <f0-y> <f1-x> <f1-y>" % (cmd,cmd,cmd)))
	def cmd_basewarp(self, cmd, param, p, targ):
		if(p.arena!=self.arena):
			return
		#chat.SendModMessage('invoking command %s'% (self.cmd))
		try:
			if param=='':
				self.printusage(cmd,p)
				return

			params=param.split(' ')
			if params[0]=='set':
				if len(params)!=6:
					self.printusage(cmd,p)
					return

				setSpawn(params[1],self.arena.basename.lower(),(int(params[2]),int(params[3]),int(params[4]),int(params[5])))
				return
			spawn=getSpawn(params[0],self.arena.basename.lower())
			if spawn is not None:
				basefreq=0
				if len(params) == 2:
					try:
						basefreq=int(params[1])
					except:
						chat.SendMessage(p,prefix('Could not interpret the basefreq %s. try %s %s 0 or %s %s 2' % (params[1],cmd,params[0],cmd,params[0])))
				game.WarpTo((self.arena, basefreq+0),spawn[0],spawn[1])
				game.WarpTo((self.arena, basefreq+1),spawn[2],spawn[3])
				game.ShipReset((self.arena, basefreq+0))
				game.ShipReset((self.arena, basefreq+1))
				chat.SendArenaSoundMessage(self.arena, GAMESTART_SOUND, prefix("GOGOGOGOGO!!!"))
			else:
				chat.SendMessage(p,prefix('That spawnpoint is not defined'))
		except:
			formatted_lines = traceback.format_exc().splitlines()
			for line in formatted_lines:
				chat.SendModMessage(line)
	def remref(self):
		self.cmd=None
		#chat.SendModMessage('removeing command %s'% (self.cmd))


def mm_attach(arena):
	try:
		arena.basewarp= basewarp(arena)
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(line)

def mm_detach(arena):
	try:
		arena.basewarp.remref()
		arena.basewarp= None
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(prefix(line))

			
#on module load (insmod)
readconf()



