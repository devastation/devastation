#ifndef BUY_H_INCLUDED
#define BUY_H_INCLUDED

#define I_BUY "buy-1"

typedef struct Ibuy
{
	INTERFACE_HEAD_DECL

    /** Adds a new buyable
     *
     * @param name, The name of the buyable.
     * @param description, an addional message that describes the buyable.
     * @param cost, The amount of credits needed to buy.
     * @param CallBack, The function to call when a player succesfully buys this item.
     * @return The ID of the buyable.
     */
    int (*AddBuyable)(char *name, char *description, int (*CallBack)(Player *p, int id, const char* name, const char *params, void *clos), void *clos);


    /** Adds a temporary restriction to a buyable.
     *
     * @param id, The ID of the buyable.
     * @param target, The target to restrict.
     * @param time, How long the restriction lasts in secconds.
     *
     * @return The ID of the restiction.
     */

    int (*Restrict_)(int id, Target target, unsigned int ltime);

} Ibuy;

#endif // BUY_H_INCLUDED
