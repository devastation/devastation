
#ifndef __DIGITHUD_H
#define __DIGITHUD_H

/** the interface id for digithud */
#define I_DIGITHUD "digithud-1"

/** the digithud interface struct */
typedef struct Idigithud
{
	INTERFACE_HEAD_DECL
	/* BROKENpyint: use */

	void (*Off)(const Target *target);
	/* BROKENpyint: target -> void */

	void (*Update)(const Target *target, int newnumber);
	/* BROKENpyint: target, int -> void */
} Idigithud;

#endif

