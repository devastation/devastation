#ifndef RESTRICT_H_INCLUDED
#define RESTRICT_H_INCLUDED

#include <time.h>
#define I_RESTRICT "restrict-1"

/**
  This module allows other modules to temporary restrict certain rights from
  players. It's mainly designed to be used for the buy module, but later changed
  so other modules can use it.

  |--------------------------------------------------------------------------|
  | Init                                                                     |
  |  \-----> Create a list and store the ID (CreateList)                     |
  |                                                                          |
  | Function                                                                 |
  |  \-----> Check if a player is restricted (IsRes)                         |
  |           \------> If not, continue and add the restriction. (Restrict)  |
  |           \------> If it is send the player a message and stop.          |
  |--------------------------------------------------------------------------|

**/


typedef struct Restriction
{
    int id;           //ID number.
    const char *name; //Name of the restriction.
    Target target;    //Target.
    time_t time;      //Time in secconds when it runs out.
    char used;        //True if this is used
    void *data;       //Pointer to extra data
} Restriction;

typedef struct Irestrict
{
	INTERFACE_HEAD_DECL

    /** Creates a list to store restrictions
     *  (each module should make its own list).
     *
     * @param name, The name you want the list to use.
     * @return the ID number of the list.
     */
	int (*CreateList)(const char *name);

    /** Adds a restrictions to a list.
     *
     * @param list, The number of the list to add the restriction to.
     * @param name, The name of the restriction.
     * @param target, The target to restrict.
     * @param ttime, The time in secconds before the restriction runs out.
     * @param clos, A pointer to some extra data you might want to use.
     * @return the ID number of the restriction (unused).
     */
    int (*restrict_)(int list, const char *name, Target target, unsigned int ttime, void *clos);

    /** Checks if a player is restricted.
     *
     * @param p, The player to check.
     * @param list, the number of the list to check.
     * @param name, The name of the restriction.
     */
    Restriction* (*IsRes)(Player *p, int list, const char *name);

} Irestrict;

#endif // RESTRICT_H_INCLUDED
