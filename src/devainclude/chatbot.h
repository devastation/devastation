#ifndef CHATBOT_H_INCLUDED
#define CHATBOT_H_INCLUDED

#define I_CHATBOT "chatbot-1"

/**
  This module is created so other modules are able to send messages to chat
  channels. The message you want to send is send to a bot who will in his turn
  send it to his first channel. The name of the bot is stored in global.conf
 */

typedef struct Ichatbot
{
	INTERFACE_HEAD_DECL

    /** Send a private message to the bot, which in his turn will send
     * it to the chat channel.
     *
     * @param s, the string to send, followed by the optional format.
     * @return 1 on success, 0 if bot couldn't be found
     */
    char (*SendChatMessage)(const char *s, ...);
} Ichatbot;

#endif // CHATBOT_H_INCLUDED
