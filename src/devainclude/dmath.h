#ifndef DMATH_H_INCLUDED
#define DMATH_H_INCLUDED

#include "asss.h"

typedef struct OrderList
{
    void *data;
    int value;
} OrderList;

void quickSort(OrderList*, int, int);

#endif
