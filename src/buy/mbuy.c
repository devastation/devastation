 /**************************************************************
 * Standard Buy Module
 *
 * The following module is a set of possible buyables available
 * in arenas. See the Buy Module (buy.c) for more details.
 *
 * By: Hallowed be thy name
 * Modified by: Hakaku
 *
 * ************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"
#include "clientset.h"

local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;
local Iclientset *cs;

local override_key_t ok_Antidoor;
local override_key_t ok_Repels;

/* MaxGuns Override keys */
local override_key_t ok_WbMG;
local override_key_t ok_JvMG;
local override_key_t ok_SpMG;
local override_key_t ok_LvMG;
local override_key_t ok_TeMG;
local override_key_t ok_WlMG;
local override_key_t ok_LaMG;
local override_key_t ok_ShMG;

/* MaxBombs Override keys */
local override_key_t ok_WbMB;
local override_key_t ok_JvMB;
local override_key_t ok_SpMB;
local override_key_t ok_LvMB;
local override_key_t ok_TeMB;
local override_key_t ok_WlMB;
local override_key_t ok_LaMB;
local override_key_t ok_ShMB;

/* TODO: SEPARATE INTO MULTIPLE FILES*/

/* Bacon! */
local int bacon(Player *p, int id, const char *name, const char *params, void *clos)
{
    chat->SendArenaSoundMessage(p->arena, 18, "%s just bought some bacon!", p->name);
    return 1;
}

int super = 17;
int brick = 26;
int rocket = 27;
int shields = 18;

/* Prize super, brick, rocket, shields */
local int basicPrize(Player *p, int id, const char *name, const char *params, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        Target target;
        target.type = T_PLAYER;
        target.u.p = p;
        int prize = *((int*)clos);
        game->GivePrize(&target, prize, 1);
        chat->SendArenaMessage(p->arena, "%s just bought a %s", p->name, name);

        return 1;
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/* Buy Spam*/
local int spam(Player *p, int id, const char *name, const char *params, void *clos)
{
    if (strlen(params) > 0 && strlen(params) < 230) //this needs better handling
    {
        chat->SendArenaMessage(p->arena, "%s -%s (?buy spam)", params, p->name);
        return 1;
    }
    else
    {
        chat->SendMessage(p, "Your message must be between 1 - 230 characters in length.");
    }
    return 0;
}

/* Turn Matrix Off*/
local int matrixCleanUp(void *param)
{
    Arena *arena = (Arena*)param;
    chat->SendArenaMessage(arena, "Repels are back to normal");

    Player *g;
    Link *link;

    cs->ArenaUnoverride(arena, ok_Repels);

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
    }
    pd->Unlock();
    
    return 0;
}

/* Matrix Repels */
local int matrix(Player *p, int id, const char *name, const char *params, void *clos)
{
    chat->SendArenaMessage(p->arena, "Repels are in matrix mode for a minute! (bribed by %s)", p->name);

    Player *g;
    Link *link;

    cs->ArenaOverride(p->arena, ok_Repels, 500);

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
    }
    pd->Unlock();
    
    ml->SetTimer(matrixCleanUp, 6000, 6000, p->arena, NULL);
    return 1;
}

/* Doors back to normal */
local int doorCleanUp(void *param)
{
    Arena *arena = (Arena*)param;
    chat->SendArenaMessage(arena, "Doors are back to normal");

    Player *g;
    Link *link;

    cs->ArenaUnoverride(arena, ok_Antidoor);

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
    }
    pd->Unlock();

    return 0;
}

/* Open doors */
local int antidoor(Player *p, int id, const char *name, const char *params, void *clos)
{
    chat->SendArenaMessage(p->arena, "All doors will be opened for a minute! (bot bribed by %s)", p->name);
    Player *g;
    Link *link;

    cs->ArenaOverride(p->arena, ok_Antidoor, 0);

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
    }
    pd->Unlock();

    ml->SetTimer(doorCleanUp, 6000, 6000, p->arena, NULL);
    return 1;
}

/* Get flag coordinates */
local int flagcoords(Player *p, int id, const char *name, const char *params, void *clos)
{
    int i = 0, j = 0;
    char str[256] = "Uncarried flags:";
    char buf[32];
    int flagcount = cfg->GetInt(p->arena->cfg, "Flag", "FlagCount", 0);
    FlagInfo fi;
    for (; i < flagcount; i++)
    {
        flags->GetFlags(p->arena, i, &fi, 1);
        if (fi.state != FI_CARRIED)
        {
            sprintf(buf, " %c%i ", (char)('A' + (fi.x * 20 / 1024)), (fi.y * 20 / 1024 + 1));
            strcat(str, buf);

            j++;
        }
    }
    if (j)
    {
        chat->SendMessage(p, str);
        return 1;
    }

    chat->SendMessage(p, "There are no uncarried flags");
    return 0;
}

// Basewarp coordinates are determined in the arena's conf file.
// The number of basewarps is also determined in conf file.
local int basewarp(Player *p, int id, const char *name, const char *params, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        char *next;
        char random = 0;
        float base = 0;
        int basenum = cfg->GetInt(p->arena->cfg, "Bases", "Basenum", 0); //This checks for the amount of bases in the arena
        int new = strtol(params, &next, 0);
        if ((new > basenum) || (new < 0))
        {
                chat->SendMessage(p, "Usage: ?buy basewarp #[1-%i]", basenum); //Usage: ?next #[1-%i]
                return 0;
        }
        if (next != params)
            base = new;
        if (!base)
        {
            base = (float)rand() / RAND_MAX * basenum + 1;
            random = 1;
        }

        char buf[basenum];
        sprintf(buf, "x%i", (int)base);
        int x = cfg->GetInt(p->arena->cfg, "Bases", buf, 0);
        sprintf(buf, "y%i", (int)base);
        int y = cfg->GetInt(p->arena->cfg, "Bases", buf, 0);

        if ((!x) || (!y))
        {
            chat->SendMessage(p, "Error in config file, please notify staff.");
            return 0;
        }


        if (random == 1)
            chat->SendMessage(p, "You were randomly warped to base %i", (int)base);
        else
            chat->SendMessage(p, "You were warped to base %i", (int)base);
        Target target;
        target.type = T_PLAYER;
        target.u.p = p;
        game->WarpTo(&target, x,y);
        return 1;
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/* Buy items */
local int items(Player *p, int id, const char *name, const char *param, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        chat->SendMessage(p, "Ok, you got a load of items, have fun.");
        Target target;
        target.type = T_PLAYER;
        target.u.p = p;
        game->GivePrize(&target, 0, 400);
        return 1;
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/* Return settings to normal*/
local int redCleanUp(void *param)
{
    Arena *arena = (Arena*)param;

    Player *g;
    Link *link;

    cs->ArenaUnoverride(arena, ok_WbMG);
    cs->ArenaUnoverride(arena, ok_JvMG);
    cs->ArenaUnoverride(arena, ok_SpMG);
    cs->ArenaUnoverride(arena, ok_LvMG);
    cs->ArenaUnoverride(arena, ok_TeMG);
    cs->ArenaUnoverride(arena, ok_WlMG);
    cs->ArenaUnoverride(arena, ok_LaMG);
    cs->ArenaUnoverride(arena, ok_ShMG);

    cs->ArenaUnoverride(arena, ok_WbMB);
    cs->ArenaUnoverride(arena, ok_JvMB);
    cs->ArenaUnoverride(arena, ok_SpMB);
    cs->ArenaUnoverride(arena, ok_LvMB);
    cs->ArenaUnoverride(arena, ok_TeMB);
    cs->ArenaUnoverride(arena, ok_WlMB);
    cs->ArenaUnoverride(arena, ok_LaMB);
    cs->ArenaUnoverride(arena, ok_ShMB);

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
    }
    pd->Unlock();

    chat->SendArenaMessage(arena, "No more red.");
    return 0;
}

/* Make all bullets and bombs red */
local int red(Player *p, int id, const char *name, const char *params, void *clos)
{
    chat->SendArenaSoundMessage(p->arena, 2, "RED! (bribed by %s)", p->name);

    Player *g;
    Link *link;

    cs->ArenaOverride(p->arena, ok_WbMG, 1);
    cs->ArenaOverride(p->arena, ok_JvMG, 1);
    cs->ArenaOverride(p->arena, ok_SpMG, 1);
    cs->ArenaOverride(p->arena, ok_LvMG, 1);
    cs->ArenaOverride(p->arena, ok_TeMG, 1);
    cs->ArenaOverride(p->arena, ok_WlMG, 1);
    cs->ArenaOverride(p->arena, ok_LaMG, 1);
    cs->ArenaOverride(p->arena, ok_ShMG, 1);

    cs->ArenaOverride(p->arena, ok_WbMB, 1);
    cs->ArenaOverride(p->arena, ok_JvMB, 1);
    cs->ArenaOverride(p->arena, ok_SpMB, 1);
    cs->ArenaOverride(p->arena, ok_LvMB, 1);
    cs->ArenaOverride(p->arena, ok_TeMB, 1);
    cs->ArenaOverride(p->arena, ok_WlMB, 1);
    cs->ArenaOverride(p->arena, ok_LaMB, 1);
    cs->ArenaOverride(p->arena, ok_ShMB, 1);

    Target target;
    target.type = T_PLAYER;

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        cs->SendClientSettings(g);
        target.u.p = g;
        game->GivePrize(&target, -8, 2);
        game->GivePrize(&target, -9, 2);
    }
    pd->Unlock();

    ml->SetTimer(redCleanUp, 30000, 30000, p->arena, NULL);
    return 1;
}

/* Buy Frequency */
local int freq(Player *p, int id, const char *name, const char *params, void *clos)
{
    int freq = atoi(params);
    if  (freq < 100)
    {
        chat->SendMessage(p, "You cannot purchase this frequency.");
        return 0;
    }
    if  (freq > 9999)
    {
        chat->SendMessage(p, "To purchase private freq, the number must be between 100 and 9999.");
        return 0;
    }
    if ((p->p_ship != SHIP_SPEC) && (100 < freq || freq < 9999))
    {
        int count = 0;
        Link *link;
        Player *g;
        pd->Lock();
        FOR_EACH_PLAYER(g)
        {
            if (g->p_freq == freq)
            {
                /* Count the players on the requested frequency */
                count++;
            }
        }
        pd->Unlock();
        
        /* If there are less than five players, setfreq */
        if (count < 5)
        {
            game->SetFreq(p, freq);
            chat->SendMessage(p, "You have just purchased freq '%s'.", params); 
            return 1;
        }
        /* Otherwise, deny the request */
        else
        {
            chat->SendMessage(p, "This team already has too many players.");
            return 0;
        }
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

local void releaseInterfaces()
{
    mm->ReleaseInterface(pd);
    mm->ReleaseInterface(ml);
    mm->ReleaseInterface(buy);
    mm->ReleaseInterface(cfg);
    mm->ReleaseInterface(chat);
    mm->ReleaseInterface(game);
    mm->ReleaseInterface(flags);
    mm->ReleaseInterface(obj);
    mm->ReleaseInterface(cs);
}

EXPORT int MM_stdbuy(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
        ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
        buy = mm->GetInterface(I_BUY, ALLARENAS);
        cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        game = mm->GetInterface(I_GAME, ALLARENAS);
        flags = mm->GetInterface(I_FLAGCORE, ALLARENAS);
        obj = mm->GetInterface(I_OBJECTS, arena);
        cs = mm->GetInterface(I_CLIENTSET, arena);

        if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj || !cs)
        {
            releaseInterfaces();
            return MM_FAIL;
        }
        
        buy->AddBuyable("bacon", "A nice, crisp slice of bacon.", bacon, NULL);
        buy->AddBuyable("brick", "A Brick!", basicPrize, &brick);
        buy->AddBuyable("super", "Temporary superpower (up to 8 seconds).", basicPrize, &super);
        buy->AddBuyable("rocket", "Gives you a rocket!", basicPrize, &rocket);
        buy->AddBuyable("shields", "Gives you Shields (For 30 Seconds)", basicPrize, &shields);
        buy->AddBuyable("spam", "Pester the arena with spam! (?buy spam blahblah).", spam, NULL);
        buy->AddBuyable("items", "Gives you a full set of items (the same ones you start with)", items, NULL);
        buy->AddBuyable("antidoor", "Opens all doors for a minute.", antidoor, NULL);
        buy->AddBuyable("matrix", "Matrix-slowmo repels for a minute.", matrix, NULL);
        buy->AddBuyable("flagcoords", "Sends you a list with the coords of all uncarried flags.", flagcoords, NULL);
        buy->AddBuyable("basewarp", "Warps you to the entrance of a base. basewarp 1 is top-left\nand 'basewarp 4' is bottom-left. No number is random base.", basewarp, NULL);
        buy->AddBuyable("red", "It's very red, isn't it?", red, NULL);
        buy->AddBuyable("freq", "Buy a private frequency between 100-9999. Other players can\n buy same freq. Up to 5 players per freq.", freq, NULL);
        
        ok_Antidoor = cs->GetOverrideKey("Door", "Doormode");
        ok_Repels = cs->GetOverrideKey("Repel", "RepelSpeed");
        
        ok_WbMG = cs->GetOverrideKey("Warbird", "MaxGuns");
        ok_JvMG = cs->GetOverrideKey("Javelin", "MaxGuns");
        ok_SpMG = cs->GetOverrideKey("Spider", "MaxGuns");
        ok_LvMG = cs->GetOverrideKey("Leviathan", "MaxGuns");
        ok_TeMG = cs->GetOverrideKey("Terrier", "MaxGuns");
        ok_WlMG = cs->GetOverrideKey("Weasel", "MaxGuns");
        ok_LaMG = cs->GetOverrideKey("Lancaster", "MaxGuns");
        ok_ShMG = cs->GetOverrideKey("Shark", "MaxGuns");
        
        ok_WbMB = cs->GetOverrideKey("Warbird", "MaxBombs");
        ok_JvMB = cs->GetOverrideKey("Javelin", "MaxBombs");
        ok_SpMB = cs->GetOverrideKey("Spider", "MaxBombs");
        ok_LvMB = cs->GetOverrideKey("Leviathan", "MaxBombs");
        ok_TeMB = cs->GetOverrideKey("Terrier", "MaxBombs");
        ok_WlMB = cs->GetOverrideKey("Weasel", "MaxBombs");
        ok_LaMB = cs->GetOverrideKey("Lancaster", "MaxBombs");
        ok_ShMB = cs->GetOverrideKey("Shark", "MaxBombs");

            
            return MM_OK;
        
    }
    else if (action == MM_UNLOAD)
    {
        releaseInterfaces();
        return MM_OK;
    }
    else if (action == MM_ATTACH)
    {
        return MM_OK;
    }
    else if (action == MM_DETACH)
    {
        return MM_OK;
    }

    return MM_FAIL;
}
