/**************************************************************
 * (Buy) Burn Module
 * ************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
    int burn;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;

local char burnbuf[1024];

/* Prize near */
local int BurnNear(Player *pp, int rad, int num)
{
	memset(burnbuf, '\0', sizeof(burnbuf));
	burnbuf[0] = '\0';

    rad *= 16;
    Player *g;
    Link *link;
    int count = 0;
    
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if (g->arena == pp->arena)
        {
            if (g->p_ship != SHIP_SPEC)
            {
                int w = (g->position.x - pp->position.x) * (g->position.x - pp->position.x);
                int h = (g->position.y - pp->position.y) * (g->position.y - pp->position.y);

                if (((w + h) < (rad * rad)) && (count != num))
                {
                    if (g->position.status & STATUS_SAFEZONE)
                    {
                        //do nothing
                    }
                    else
                    {
                        Pdata *data = PPDATA(g, playerKey);
                        data->burn = 1;
                        count++;
                    }
                }
            }
        }
    }
    pd->Unlock();

    if (count == 1)
    {
        chat->SendMessage(pp, "There were no players close enough.");
        return 0;
    }

    int i = 0;
    
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if ((g->arena == pp->arena) && (g->p_ship != SHIP_SPEC))
        {
            Pdata *data = PPDATA(g, playerKey);
            if (data->burn == 1)
            {
                i++;
				
				Target target;
				target.type = T_PLAYER;
                target.u.p = g;
                
                game->GivePrize(&target, -22, 3); //burst (shark has 3)
                game->GivePrize(&target, -23, 2); //decoys
                game->GivePrize(&target, -24, 2); //thor?
                game->GivePrize(&target, -26, 2); //brick
                game->GivePrize(&target, -21, 3); //repel (jav has 3)
                game->GivePrize(&target, -28, 2); //portal
                
                strcat(burnbuf, g->name);
                if (i != count)
                    strcat(burnbuf, ", ");

                data->burn = 0;
            }
        }
    }
    pd->Unlock();

    return 1;
}

/* Burn */
/* FIXME: Prizenear spam = x6 */
local int burn(Player *p, int id, const char *name, const char *params, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        int try = BurnNear(p, 75, 6);

		if (!try)
			return 0;
            
        chat->SendArenaMessage(p->arena, "Player(s) %s lost their items thanks to %s", burnbuf, p->name);
        return 1;
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_burn[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_burn(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        ml = mm->GetInterface(I_MAINLOOP, arena);
        buy = mm->GetInterface(I_BUY, arena);
        cfg = mm->GetInterface(I_CONFIG, arena);
        chat = mm->GetInterface(I_CHAT, arena);
        game = mm->GetInterface(I_GAME, arena);
        flags = mm->GetInterface(I_FLAGCORE, arena);
        obj = mm->GetInterface(I_OBJECTS, arena);

        if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj)
            return MM_FAIL;
        else
        {
            playerKey = pd->AllocatePlayerData(sizeof(Pdata));
            if (!playerKey)
            {
                return MM_FAIL;
            }
            else
            {
                buy->AddBuyable("burn", "Removes all items from 6 nearby players and you (safe players excluded).", burn, NULL);
                return MM_OK;
            }
        }
    }
    else if (action == MM_UNLOAD)
    {
        pd->FreePlayerData(playerKey);
        
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(buy);
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(obj);

        return MM_OK;
    }

    return MM_FAIL;
}
