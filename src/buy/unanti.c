/**************************************************************
 * (Buy) Noanti Module
 * ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
    int noanti;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;

local char noantibuf[1024];

/* NOTE: ONLY REMOVES ANTI FROM ANTYING PLAYER :s*/
/* Prize near */
local int AntiNear(Player *pp, int rad, int num, int prize)
{
	memset(noantibuf, '\0', sizeof(noantibuf));
	noantibuf[0] = '\0';

    rad *= 16;
    Player *g;
    Link *link;
    int count = 0;
    
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if (g->arena == pp->arena)
        {
            if (g->p_ship != SHIP_SPEC)
            {
                int w = (g->position.x - pp->position.x) * (g->position.x - pp->position.x);
                int h = (g->position.y - pp->position.y) * (g->position.y - pp->position.y);

                if (((w + h) < (rad * rad)) && (g->position.status & STATUS_ANTIWARP) && (g != pp) && (count != num)) //check if the player has anti
                {
                    Pdata *data = PPDATA(g, playerKey);
                    data->noanti = 1;
                    count++;
                }
            }
        }
    }
    pd->Unlock();

    if (count == 0) //because we don't include ourselves
    {
        chat->SendMessage(pp, "There were no players close enough.");
        return 0;
    }

    int i = 0;

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if ((g->arena == pp->arena) && (g->p_ship != SHIP_SPEC))
        {
            Pdata *data = PPDATA(g, playerKey);
            if (data->noanti == 1)
            {
                i++;
				
				Target target;
				target.type = T_PLAYER;
                target.u.p = g;
				
                game->GivePrize(&target, prize, 1);
				
                strcat(noantibuf, g->name);
//                if (i != count)
//                    strcat(noantibuf, ", ");

                data->noanti = 0;
            }
        }
    }
    pd->Unlock();

    return 1;
}

/* Remove antiwarp from a nearby player */
local int noanti(Player *p, int id, const char *name, const char *param, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        int try = AntiNear(p, 75, 1, -20);

		if (!try)
			return 0;
		
        chat->SendArenaMessage(p->arena, "Player %s removed %s's antiwarp", p->name, noantibuf);
        return 1;
    }

    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_remanti[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_remanti(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        ml = mm->GetInterface(I_MAINLOOP, arena);
        buy = mm->GetInterface(I_BUY, arena);
        cfg = mm->GetInterface(I_CONFIG, arena);
        chat = mm->GetInterface(I_CHAT, arena);
        game = mm->GetInterface(I_GAME, arena);
        flags = mm->GetInterface(I_FLAGCORE, arena);
        obj = mm->GetInterface(I_OBJECTS, arena);

        if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj)
            return MM_FAIL;
        else
        {
            playerKey = pd->AllocatePlayerData(sizeof(Pdata));
            if (!playerKey)
            {
                return MM_FAIL;
            }
            else
            {
                buy->AddBuyable("noanti", "Removes antiwarp from a nearby player.", noanti, NULL);
                return MM_OK;
            }
        }
    }
    else if (action == MM_UNLOAD)
    {
        pd->FreePlayerData(playerKey);
        
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(buy);
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(obj);

        return MM_OK;
    }

    return MM_FAIL;
}
