/**************************************************************
 * (Buy) Warp Module
 * ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
    int warp;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;

local char warpbuf[1024];

/* Prize near */
local int WarpNear(Player *pp, int rad, int num, int prize)
{
	memset(warpbuf, '\0', sizeof(warpbuf));
	warpbuf[0] = '\0';

    rad *= 16;
    Player *g;
    Link *link;
    int count = 0;
    
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if (g->arena == pp->arena)
        {
            if (g->p_ship != SHIP_SPEC)
            {
                int w = (g->position.x - pp->position.x) * (g->position.x - pp->position.x);
                int h = (g->position.y - pp->position.y) * (g->position.y - pp->position.y);

                int pflags = flags->CountPlayerFlags(g);
                if (((w + h) < (rad * rad)) && (count != num))
                {
					if (pflags == 0) {
						Pdata *data = PPDATA(g, playerKey);
						data->warp = 1;
						count++;
					} else if (g != pp) {
						chat->SendMessage(pp, "Could not warp %s: player is carrying flags.", g->name);
					}
                }
            }
        }
    }
    pd->Unlock();

    if (count <= 1)
    {
        chat->SendMessage(pp, "There were no players close enough.");
        return 0;
    }

    int i = 0;

    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if ((g->arena == pp->arena) && (g->p_ship != SHIP_SPEC))
        {
            Pdata *data = PPDATA(g, playerKey);
            if (data->warp == 1)
            {
                i++;
				
				Target target;
				target.type = T_PLAYER;
                target.u.p = g;
				
                game->GivePrize(&target, prize, 1);
				
                strcat(warpbuf, g->name);
                if (i != count)
                    strcat(warpbuf, ", ");

                data->warp = 0;
            }
        }
    }
    pd->Unlock();

    return 1;
}

/* Warp */
/* TODO: if player has flags don't allow. & don't warp player with flags. */
local int warp(Player *p, int id, const char *name, const char *param, void *clos)
{
    int pflags = flags->CountPlayerFlags(p);
    if (pflags != 0)
    {
        chat->SendMessage(p, "You cannot purchase this item while carrying flags.");
        return 0;
    }
    else if (p->p_ship != SHIP_SPEC)
    {
        int try = WarpNear(p, 100, 4, 7); //3 players + you

		if (!try)
			return 0;

        chat->SendArenaMessage(p->arena, "Player(s) %s got warped by %s.", warpbuf, p->name);
        return 1;
    }
    return 0;
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_warp[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_warp(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        ml = mm->GetInterface(I_MAINLOOP, arena);
        buy = mm->GetInterface(I_BUY, arena);
        cfg = mm->GetInterface(I_CONFIG, arena);
        chat = mm->GetInterface(I_CHAT, arena);
        game = mm->GetInterface(I_GAME, arena);
        flags = mm->GetInterface(I_FLAGCORE, arena);
        obj = mm->GetInterface(I_OBJECTS, arena);

        if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj)
            return MM_FAIL;
        else
        {
            playerKey = pd->AllocatePlayerData(sizeof(Pdata));
            if (!playerKey)
            {
                return MM_FAIL;
            }
            else
            {
                buy->AddBuyable("warp", "Warps you and up to 3 nearby players.", warp, NULL);
                return MM_OK;
            }
        }
    }
    else if (action == MM_UNLOAD)
    {
        pd->FreePlayerData(playerKey);
        
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(buy);
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(obj);

        return MM_OK;
    }

    return MM_FAIL;
}
