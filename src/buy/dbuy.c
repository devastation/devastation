 /**************************************************************
 * Buy Module
 *
 * Features:
 *
 * 1 - ?buy (lists available buyables depending on credits)
 *   [Buy] itemname = price
 *
 * 2 - ?buylist (lists all buyables)
 *
 * 3 - doubles price after X amount of credits.
 *   [Credits] DoubleCreds = #
 *
 * 4 - Restricts items from player, team, arena, or zone after
 *     a specific item has been purchased.
 *   [BuyDelay] itemname = delay
 *              itemnamemode = # (1=p, 2=a, 3=t, 4=z, 0=none)
 *
 * Created by: Hallowed be thy name
 * Modified by: Hakaku
 *
 **************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "asss.h"
#include "credits.h"
#include "buy.h"
#include "restrict.h"

local Imodman *mm;
local Icredits *creds;
local Ichat *chat;
local Iconfig *cfg;
local Iarenaman *aman;
local Iplayerdata *pd;
local Icmdman *cmd;
local Irestrict *res;

local int doublecreds;

typedef struct Buyable
{
	int id;
	const char *name;
	const char *description;
	int (*CallBack)(Player *p, int id, const char* name, const char *params, void *clos);
	void *clos;
} Buyable;

local Buyable* buyList;
local int number;
local int list;

typedef struct Pdata
{
	int test;
} Pdata;

typedef struct Adata
{
	char temp;
} Adata;

local int playerKey;
local int arenaKey;

local int addBuyable(char *name, char *description, int (*CallBack)(Player *p, int id, const char* name, const char *params, void *clos), void *clos)
{
	if (!buyList)
		buyList = (Buyable*)malloc(sizeof(Buyable));
	else
		buyList = (Buyable*)realloc(buyList, (sizeof(Buyable) * (number+1)));

	Buyable this = { number, name, description, CallBack, clos };
	buyList[number] = this;

	return number++;
}

local int restrict_(int id, Target target, unsigned int ltime)
{
	return res->restrict_(list, buyList[id].name, target, ltime, NULL);
}


local unsigned long getCost(Buyable *buy, Arena *arena)
{
	return cfg->GetInt(arena->cfg, "Buy", buy->name, 0);
}

/************************************************************************/
/*                          Player Commands                             */
/************************************************************************/
local void cBuy(const char *command, const char *params, Player *p, const Target *target)
{
	if (!isalpha(params[0]))
	{
		int i = 0, j = 0;
		//Link link = { NULL, p };
		//LinkedList lst = { &link, &link };
		for (; i < number; i++)
		{
			Buyable* this = &buyList[i];
			/* Retrieve the player's score */
			unsigned long got = creds->GetCredits(p);
			/* Let's check if the player is too rich (here 75M+) */
			int level = 1;
			if (got > doublecreds)
			{
				level = 2;
			}
			else
			{
				level = 1;
			}
			/* Retrieve the cost of the items */
			unsigned long cost = level * getCost(this, p->arena);
			if (cost > 0)
			{
				char ccost[10];
				if (cost < 1000000)
					sprintf(ccost, "%li ", cost);
				else
					sprintf(ccost, "%liM", cost/1000000);

				if (j==0)
				{
					chat->SendMessage(p, "--------------------------------------------------------------------------------------------");
					chat->SendMessage(p, "| Name            | Cost    | Description                                                  |");
					chat->SendMessage(p, "|-----------------+---------+--------------------------------------------------------------|");
					j = 1;
				}


				int clen = 60;
				int diff;
				char* find = strstr(this->description, "\n");
				if (find)
				{
					diff = strlen(this->description) - strlen(find);
					if (diff < 60)
						clen = diff;
				}

				char sdesc[60];
				strncpy (sdesc, this->description, clen);
				sdesc[60] = '\0';

				if (creds->GetCredits(p) >= cost)
					chat->SendMessage(p, "| %-15s | %7s | %-60s |", this->name, ccost, sdesc);
				else
					//Adding this line will make buyables that are too expensive appear in red (rather than not appear at all)
					//chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, 0, NULL,	"| %-15s | %7s | %-60s |", this->name, ccost, sdesc);
				memset (sdesc,' ',60);

				if (strlen(this->description) > (clen - 1))
				{
					int i = clen;
					for (; i < strlen(this->description); i += (clen + 1))
					{
						clen = 60;
						find = strstr(&this->description[i + 1], "\n");
						if (find)
						{
							diff = strlen(&this->description[i + 1]) - strlen(find);
							if (diff < 60)
							  clen = diff;
						}
						strncpy(sdesc, &this->description[i+1], clen);
						sdesc[60] = '\0';

						if (creds->GetCredits(p) >= cost)
							chat->SendMessage(p, "|                 |         | %-60s |", sdesc);
						else
							//Adding this line will make buyable descriptions (too long to fit on one line) appear in red
							//chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, 0, NULL, "|                 |         | %-60s |", sdesc);
						memset (sdesc,' ',60);
					}
				}
			}
		}
		if (j==1)
			chat->SendMessage(p, "--------------------------------------------------------------------------------------------");
		else
			chat->SendMessage(p, "You can't buy items in this arena.");
	}
	else
	{
		int i = 0;
		for (; i < number; i++)
		{
			Buyable* this = &buyList[i];
			char *find;
			int len = strlen(this->name);
			if (((find = strstr(params,this->name))) && (!isalnum(params[len])))
			{
				/* Retrieve the player's score */
				unsigned long got = creds->GetCredits(p);
				/* Let's check if the player is too rich (here 75M+) */
				int level = 1;
				if (got > doublecreds)
				{
					level = 2;
				}
				else
				{
					level = 1;
				}
				/* Retrieve the cost of the items */
				unsigned long cost = level * getCost(this, p->arena);
				if (cost < 1)
				{
					chat->SendMessage(p, "That item is not available in this arena.");
					return;
				}
				Restriction* r = res->IsRes(p, list, this->name);
				if (!r)
				{
					if (cost > got)
					{
						unsigned long diff = cost - got;
						char cdiff[10] = "";
						if (diff > 1000000)
							sprintf(cdiff, " (%liM)", diff/1000000);
						chat->SendMessage(p, "You don't have enough credits, you need %li%s more", diff, cdiff);
					}
					else
					{
						//Send a callback
						if (this->CallBack)
						{
							const char *spar = "";
							if (strlen(params) > len + 1)
								spar = &params[len+1];
							if (this->CallBack(p, this->id, this->name, spar, this->clos) == 1)
							{
								creds->RemoveCredits(p, cost);
								int delay = cfg->GetInt(p->arena->cfg, "BuyDelay", this->name, 0);
								if (delay > 0)
								{
									char modestr[64];
									sprintf(modestr, "%smode", this->name);
									int mode = cfg->GetInt(p->arena->cfg,  "BuyDelay", modestr, 0);
									if (mode > 0)
									{
										Target target;
										target.type = mode;
										if (mode == T_PLAYER)
											target.u.p = p;
										else if (mode == T_ARENA)
											target.u.arena = p->arena;
										else if (mode == T_FREQ)
										{
											target.u.freq.freq = p->p_freq;
											target.u.freq.arena = p->arena;
										}

										restrict_(this->id, target, delay);
									}
								}
							}
						}
					}
					return;
				}
				else
				{
					if (r->target.type == T_PLAYER)
						chat->SendMessage(p, "You have already recently bought this item.");
					else if (r->target.type == T_ARENA)
						chat->SendMessage(p, "This item was already recently bought.");
					else if (r->target.type == T_FREQ)
						chat->SendMessage(p, "Your team has already recently bought this item.");
					else if (r->target.type == T_ZONE)
						chat->SendMessage(p, "The purchase of this item is temporarily disabled.");

					return;
				}
			}
		}
		chat->SendMessage(p, "Unknown item");
	}
}

local Ibuy interface =
{
	INTERFACE_HEAD_INIT(I_BUY, "Ibuy")
	addBuyable, restrict_
};

local helptext_t buy =
"Targets: none\n"
"Args: <item>\n"
"This allows players to buy items (listed in ?buy).\n"
"To purchase an item, use ?buy <item> (e.g. ?buy bacon)\n";

local void cBuylist(const char *command, const char *params, Player *p, const Target *target)
{
		int i = 0, j = 0;
		Link link = { NULL, p };
		LinkedList lst = { &link, &link };
		for (; i < number; i++)
		{
			Buyable* this = &buyList[i];
			/* Retrieve the player's score */
			unsigned long got = creds->GetCredits(p);
			/* Let's check if the player is too rich (here 75M+) */
			int level = 1;
			if (got > doublecreds)
			{
				level = 2;
			}
			else
			{
				level = 1;
			}
			/* Retrieve the cost of the items */
			unsigned long cost = level * getCost(this, p->arena);
			if (cost > 0)
			{
				char ccost[10];
				if (cost < 1000000)
					sprintf(ccost, "%li ", cost);
				else
					sprintf(ccost, "%liM", cost/1000000);

				if (j==0)
				{
					chat->SendMessage(p, "----------------------------------------------------------------------------------");
					chat->SendMessage(p, "| Name            | Description                                                  |");
					chat->SendMessage(p, "+-----------------+--------------------------------------------------------------+");
					j = 1;
				}


				int clen = 60;
				int diff;
				char* find = strstr(this->description, "\n");
				if (find)
				{
					diff = strlen(this->description) - strlen(find);
					if (diff < 60)
						clen = diff;
				}

				char sdesc[60];
				strncpy (sdesc, this->description, clen);
				sdesc[60] = '\0';

				if (creds->GetCredits(p) >= cost)
					chat->SendMessage(p, "| %-15s | %-60s |", this->name, sdesc);
				else
					//Adding this line will make buyables that are too expensive appear in red (rather than not appear at all)
					chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, 0, NULL,	"| %-15s | %-60s |", this->name, sdesc);
				memset (sdesc,' ',60);

				if (strlen(this->description) > (clen - 1))
				{
					int i = clen;
					for (; i < strlen(this->description); i += (clen + 1))
					{
						clen = 60;
						find = strstr(&this->description[i + 1], "\n");
						if (find)
						{
							diff = strlen(&this->description[i + 1]) - strlen(find);
							if (diff < 60)
							  clen = diff;
						}
						strncpy(sdesc, &this->description[i+1], clen);
						sdesc[60] = '\0';

						if (creds->GetCredits(p) >= cost)
							chat->SendMessage(p, "|				 | %-60s |", sdesc);
						else
							//Adding this line will make buyable descriptions (too long to fit on one line) appear in red
							chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, 0, NULL,	"|				 | %-60s |", sdesc);
						memset (sdesc,' ',60);
					}
				}
			}
		}
		if (j==1)
			chat->SendMessage(p, "----------------------------------------------------------------------------------");
		else
			chat->SendMessage(p, "Items are not available in this arena.");
}


EXPORT int MM_dbuy(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		creds = mm->GetInterface(I_CREDITS, ALLARENAS);
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		chat = mm->GetInterface(I_CHAT, ALLARENAS);
		cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		res = mm->GetInterface(I_RESTRICT, ALLARENAS);
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);

		if (!creds || !aman || !chat || !cmd || !cfg || !pd || !res)
			return MM_FAIL;
		else
		{
			playerKey = pd->AllocatePlayerData(sizeof(Pdata));
			arenaKey  = aman->AllocateArenaData(sizeof(Adata));

			if ((!playerKey) || (!arenaKey))
			{
				return MM_FAIL;
			}
			else
			{
				list = res->CreateList("Buy Module");
				
				doublecreds = cfg->GetInt(GLOBAL, "Credits", "DoubleCreds", 75000000);

				mm->RegInterface(&interface, ALLARENAS);

				return MM_OK;
			}
		}
	}
	else if (action == MM_UNLOAD)
	{
		if (mm->UnregInterface(&interface, ALLARENAS))
		{
			return MM_FAIL;
		}

		free(buyList);

		pd->FreePlayerData(playerKey);
		aman->FreeArenaData(arenaKey);

		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(res);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(creds);

		return MM_OK;
	}
        else if (action == MM_ATTACH)
        {
                cmd->AddCommand("buy", cBuy, arena, buy);
		cmd->AddCommand("buylist", cBuylist, arena, NULL);
                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
                cmd->RemoveCommand("buy", cBuy, arena);
		cmd->RemoveCommand("buylist", cBuylist, arena);
                return MM_OK;
        }

	return MM_FAIL;
}
