/**************************************************************
 * (Buy) Engine ShutDown Module
 * ************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
    int esd;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;

local char buf[1024];

/* Prize near */
local int EsdNear(Player *pp, int rad, int num, int prize)
{
	memset(buf, '\0', sizeof(buf));
	buf[0] = '\0';

    rad *= 16;
    Player *g;
    Link *link;
    int count = 0;
    
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if (g->arena == pp->arena)
        {
            if (g->p_ship != SHIP_SPEC)
            {
                int w = (g->position.x - pp->position.x) * (g->position.x - pp->position.x);
                int h = (g->position.y - pp->position.y) * (g->position.y - pp->position.y);

                if (((w + h) < (rad * rad)) && (count != num) && (g->p_freq != pp->p_freq))
                {
                    Pdata *data = PPDATA(g, playerKey);
                    data->esd = 1;
                    count++;
                }
            }
        }
    }
    pd->Unlock();

    if (count == 0) //because we don't include ourselves
    {
        chat->SendMessage(pp, "There were no players close enough.");
        return 0;
    }

    int i = 0;
	
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        if ((g->arena == pp->arena) && (g->p_ship != SHIP_SPEC))
        {
            Pdata *data = PPDATA(g, playerKey);
            if (data->esd == 1)
            {
                i++;
				
				Target target;
				target.type = T_PLAYER;
                target.u.p = g;
				
                game->GivePrize(&target, prize, 1);
				
                strcat(buf, g->name);
//                if (i != count)
//                    strcat(buf, ", ");

                data->esd = 0;
            }
        }
    }
    pd->Unlock();
    
    return 1;
}

/* Engine Shut Down*/
local int esd(Player *p, int id, const char *name, const char *param, void *clos)
{
    if (p->p_ship != SHIP_SPEC)
    {
        int try = EsdNear(p, 60, 1, 14);

		if (!try)
			return 0;

        chat->SendArenaMessage(p->arena, "Player %s received an engine shutdown by %s", buf, p->name);
        return 1;
    }
    else
    {
        chat->SendMessage(p, "You cannot purchase this item in spec.");
    }
    return 0;
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_engineshutdown[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_engineshutdown(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        pd = mm->GetInterface(I_PLAYERDATA, arena);
        ml = mm->GetInterface(I_MAINLOOP, arena);
        buy = mm->GetInterface(I_BUY, arena);
        cfg = mm->GetInterface(I_CONFIG, arena);
        chat = mm->GetInterface(I_CHAT, arena);
        game = mm->GetInterface(I_GAME, arena);
        flags = mm->GetInterface(I_FLAGCORE, arena);
        obj = mm->GetInterface(I_OBJECTS, arena);

        if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj)
            return MM_FAIL;
        else
        {
            playerKey = pd->AllocatePlayerData(sizeof(Pdata));
            if (!playerKey)
            {
                return MM_FAIL;
            }
            else
            {
                buy->AddBuyable("shutdown", "Shuts down the engines of a nearby enemy for 12 seconds.", esd, NULL);
                return MM_OK;
            }
        }
    }
    else if (action == MM_UNLOAD)
    {
        pd->FreePlayerData(playerKey);
        
        mm->ReleaseInterface(flags);
        mm->ReleaseInterface(game);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(buy);
        mm->ReleaseInterface(ml);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(obj);

        return MM_OK;
    }

    return MM_FAIL;
}
