/**************************************************************
 * (Buy) Zero Module
 * ************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "asss.h"
#include "buy.h"
#include "dmath.h"
#include "objects.h"

/* Player Data */
typedef struct Pdata
{
	int zero;
} Pdata;

local int playerKey;

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Ibuy *buy;
local Igame *game;
local Iconfig *cfg;
local Imainloop *ml;
local Iflagcore *flags;
local Iplayerdata* pd;
local Iobjects *obj;

local char zerobuf[1024];

/* Prize near */
local int ZeroNear(Player *pp, int rad, int num, int prize)
{
	memset(zerobuf, '\0', sizeof(zerobuf));
	zerobuf[0] = '\0';

	rad *= 16;
	Player *g;
	Link *link;
	int count = 0;
	
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == pp->arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
                int w = (g->position.x - pp->position.x) * (g->position.x - pp->position.x);
                int h = (g->position.y - pp->position.y) * (g->position.y - pp->position.y);

				if (((w + h) < (rad * rad)) && (count != num))
				{
					Pdata *data = PPDATA(g, playerKey);
					data->zero = 1;
					count++;
				}
			}
		}
	}
	pd->Unlock();

	if (count == 1)
	{
		chat->SendMessage(pp, "There were no players close enough.");
		return 0;
	}

	int i = 0;
		
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if ((g->arena == pp->arena) && (g->p_ship != SHIP_SPEC))
		{
			Pdata *data = PPDATA(g, playerKey);
			if (data->zero == 1)
			{
				i++;
				
				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				
				game->GivePrize(&target, prize, 1);
				
				strcat(zerobuf, g->name);
				if (i != count)
					strcat(zerobuf, ", ");
				
				data->zero = 0;
			}
		}
	}
	pd->Unlock();

	return 1;
}

/* Buy zero (energy depletion) */
local int zero(Player *p, int id, const char *name, const char *param, void *clos)
{
	if (p->p_ship != SHIP_SPEC)
	{
		int try = ZeroNear(p, 75, 5, -13);

		if (!try)
			return 0;

		chat->SendArenaMessage(p->arena, "Player(s) %s received an energy depletion from %s!", zerobuf, p->name);
		
		return 1;
	}
	else
	{
		chat->SendMessage(p, "You cannot purchase this item in spec.");
	}
	return 0;
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_zero[] = "(Devastation) v0.2 Hakaku";

EXPORT int MM_zero(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		pd = mm->GetInterface(I_PLAYERDATA, arena);
		ml = mm->GetInterface(I_MAINLOOP, arena);
		buy = mm->GetInterface(I_BUY, arena);
		cfg = mm->GetInterface(I_CONFIG, arena);
		chat = mm->GetInterface(I_CHAT, arena);
		game = mm->GetInterface(I_GAME, arena);
		flags = mm->GetInterface(I_FLAGCORE, arena);
		obj = mm->GetInterface(I_OBJECTS, arena);

		if (!buy || !cfg || !chat || !game || !flags || !pd || !ml || !obj)
			return MM_FAIL;
		else
		{
			playerKey = pd->AllocatePlayerData(sizeof(Pdata));
			if (!playerKey)
			{
				return MM_FAIL;
			}
			else
			{
				buy->AddBuyable("zero", "Depletes the energy of up to 5 nearby players.", zero, NULL);
				return MM_OK;
			}
		}
	}
	else if (action == MM_UNLOAD)
	{
		pd->FreePlayerData(playerKey);
		
		mm->ReleaseInterface(flags);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(buy);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(obj);

		return MM_OK;
	}

	return MM_FAIL;
}
