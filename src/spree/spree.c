/*
 * spree
 *
 * sends arena messages with optional bong depending on how many
 *  consecutive kills a player has. simple ?spree command.
 *
 * [Spree]
 * IgnoreKilledFake = 1 ; Your spree does not rise when killing a fake player
 * IgnoreKillerFake = 0 ; Your spree does not end when killed by a fake player
 * FakeSpree = 0 ; Can fake players can be on a spree?
 *
 * ResetSpreeOnFrequencyChange=0
 * ResetSpreeOnShipFreqChange=0
 * SpreeEndMinimum=4
 * SpreeEndMessage=has had their spree cut short
 * SpreeEndBong=22
 * Message3=%13is dominating (3:0)
 * PlayerObject3=100
 * ArenaObject3=101
 *
 * 15/02/04 - Created by Smong
 *
 * 16/02/04 - Added stdio.h
 *
 * 17/02/04 - added ?spree, get a player's current and best spree or
 *  display all people in the current arena on a spree.
 *
 * 26/02/04 - added %macros: killer, killed, bounty, coord
 *  added ResetSpreeOnFlagWin, default is off
 *
 * 14/03/04 - attempted fix for the %macro support.
 *
 * 21/03/04 - added last spree stat.
 *
 * 03/08/04 - updated prototype for Kill().
 *
 * 24/11/04 - updated to asss 1.3.2.
 *  made command arena specific.
 *  added PlayerObjectN and ArenaObjectN support (i think i heard someone say
 *   they wanted this).
 *
 * 12/01/05 - updated to asss 1.3.5.
 *  added full support for %bong using expand_message code from race
 *  cleaned up a couple of bits
 *
 * 24/june/07 - JoWie: added IgnoreKilledFake, IgnoreKillerFake, FakeSpree
 *
 * 8/june/09 - Hakaku: messages cleanup and apostrophes
 * august/2012 - updated callbacks for 1.5.0
 */

#include <stdio.h>  //sprintf
#include <stdlib.h> //strtol
#include <string.h> //strstr, strncat, strcat, strlen, strcpy
#include <ctype.h>

#include "util.h"
#include "asss.h"
#include "objects.h"

#define NO_APPEND_DOUBLE

struct pdata
{
	u16 wins;
	u16 best;
	u16 last;
};

#ifndef NO_APPEND_STRING
local inline char *append_string(char *dest, const char *src);
#endif

#ifndef NO_APPEND_INTEGER
local inline char *append_integer(char *dest, int num);
#endif

#ifndef NO_APPEND_DOUBLE
local inline char *append_double(char *dest, double num);
#endif

local int expand_message(Arena *arena, Player *killer, Player *killed,
	const char *format, char *out, int outsize);

local void PlayerAction(Player *p, int action, Arena *arena);
local void Kill(Arena *arena, Player *killer, Player *killed,
	int bounty, int flagscarried, int pts, int green);
//local void FreqChange(Player *p, int newfreq);
local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq);
local void FlagReset(Arena *arena, int freq, int points);

local void Cspree(const char *command, const char *params,
	Player *p, const Target *target);


local int pdkey;

local Imodman     *mm;
local Iplayerdata *pd;
local Iconfig     *cfg;
local Ichat       *chat;
local Icmdman     *cmd;
local Iobjects    *objs;

/* macro code */

/* valid macros:
 * killer - killer's name
 * killed - killed player's name
 * bounty - bounty of killer
 * coord  - coord of killer */

#ifndef NO_APPEND_STRING
/* returns a pointer to the end of dest */
local inline char *append_string(char *dest, const char *src)
{
	while(*src) *dest++ = *src++;
	return dest;
}
#endif

#ifndef NO_APPEND_INTEGER
/* returns a pointer to the end of dest */
local inline char *append_integer(char *dest, int num)
{
	char buf[11], *p = buf;
	snprintf(buf, 11, "%d", num);
	while(*p) *dest++ = *p++;
	return dest;
}
#endif

#ifndef NO_APPEND_DOUBLE
/* returns a pointer to the end of dest */
local inline char *append_double(char *dest, double num)
{
	char buf[11], *p = buf;
	snprintf(buf, 11, "%.2f", num);
	while(*p) *dest++ = *p++;
	return dest;
}
#endif

/* returns a %sound. */
local int expand_message(Arena *arena, Player *killer, Player *killed,
	const char *format, char *out, int outsize)
{
	char *t = out;
	int sound = 0;

	/* keep looping until we run out of space or
	 * hit the end of the format string */
	while(t - out < outsize && *format)
	{
		/* keep copying until we hit a % delimeter */
		if (*format != '%')
			*t++ = *format++;
		else
		{
			const char *macro = ++format;
			if (!strncmp(macro, "killer", 6))
			{
				t = append_string(t, killer->name);
				format += 6;
			}
			else if (!strncmp(macro, "killed", 6))
			{
				t = append_string(t, killed->name);
				format += 6;
			}
			else if (!strncmp(macro, "bounty", 6))
			{
				t = append_integer(t, killer->position.bounty);
				format += 6;
			}
			else if (!strncmp(macro, "coord", 5))
			{
				char buf[4];

				snprintf(buf, 4, "%c%d",
					'A' + killer->position.x / 819,
					1 + killer->position.y / 819);

				t = append_string(t, buf);
				format += 5;
			}
			/* todo: add more macros before this final one */
			else
			{
				/* maybe it is a number (%bong) */
				char *next;
				int num = strtol(macro, &next, 0);
				if (next != macro)
				{
					sound = num;
					format = next;
				}
			}
		}
	}

	*t = 0;
	return sound;
}

local void PlayerAction(Player *p, int action, Arena *arena)
{
	if (action == PA_ENTERARENA)
	{
		struct pdata *d = PPDATA(p, pdkey);
		d->wins = 0;
		d->best = 0;
		d->last = 0;
	}
}

local void Kill(Arena *arena, Player *killer, Player *killed, int bounty, int flagscarried, int pts, int green)
{
	char key[16], msgbuf[256];
	const char *format;
	struct pdata *d;

	if (killed->type == T_FAKE && (cfg->GetInt(arena->cfg, "Spree", "IgnoreKilledFake", 1) == 1) ) 
		return;
	
	if (killer->type == T_FAKE && (cfg->GetInt(arena->cfg, "Spree", "IgnoreKillerFake", 0) == 1) ) 
		return;
	
	

	/* operate on killed player */
	d = PPDATA(killed, pdkey);
	if (d->wins >= cfg->GetInt(arena->cfg, "Spree", "SpreeEndMinimum", 65535))
	{
		/* spree end */
		format = cfg->GetStr(arena->cfg, "Spree", "SpreeEndMessage");
		if (format)
		{
			int bong = cfg->GetInt(arena->cfg, "Spree", "SpreeEndBong", 0);
			char test[50];
			strncpy(test,format,4);
			test[1] = '\0';
			if (strcmp( test, "\'" ) == 0)
    			chat->SendArenaSoundMessage(arena, bong, "%s%s", killed->name, format);
    		else
    			chat->SendArenaSoundMessage(arena, bong, "%s %s", killed->name, format);
		}
	}

	/* store last spree */
	d->last = d->wins;

	/* reset killed stat */
	d->wins = 0;

	if (killer->type != T_FAKE || (cfg->GetInt(arena->cfg, "Spree", "FakeSpree", 0) == 1) )
	{
		/* operate on killer */
		
		/* increment killer stat */
		d = PPDATA(killer, pdkey);
		d->wins++;
	
		/* update record stat */
		if (d->wins > d->best)
			d->best = d->wins;
	
		/* construct and send message */
		sprintf(key, "Message%d", d->wins);
		format = cfg->GetStr(arena->cfg, "Spree", key);
		if (format)
		{
			int bong = expand_message(arena, killer, killed, format,
				msgbuf, sizeof(msgbuf));
	
			/* send message */
			char ttest[50];
			strncpy(ttest,format,4);
			ttest[1] = '\0';
			if (strcmp( ttest, "\'" ) == 0)
    			chat->SendArenaSoundMessage(arena, bong, "%s%s", killer->name, msgbuf); //added p->name
    		else
    			chat->SendArenaSoundMessage(arena, bong, "%s %s", killer->name, msgbuf); //added p->name
			
		}
	
		/* toggle lvz objects */
		if (objs)
		{
			Target tgt;
			int id;
	
			/* personal object */
			sprintf(key, "PlayerObject%d", d->wins);
			id = cfg->GetInt(arena->cfg, "Spree", key, -1);
	
			if (id != -1)
			{
				tgt.u.p = killer;
				tgt.type = T_PLAYER;
				objs->Toggle(&tgt, id, TRUE);
			}
	
			/* arena object */
			sprintf(key, "ArenaObject%d", d->wins);
			id = cfg->GetInt(arena->cfg, "Spree", key, -1);
	
			if (id != -1)
			{
				tgt.u.arena = arena;
				tgt.type = T_ARENA;
				objs->Toggle(&tgt, id, TRUE);
			}
		}
	}
}

/*local void FreqChange(Player *p, int newfreq)
{
	if (cfg->GetInt(p->arena->cfg, "Spree", "ResetSpreeOnFrequencyChange", 0))
	{
		struct pdata *d = PPDATA(p, pdkey);
		d->last = d->wins;
		d->wins = 0;
	}
}*/

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	if (cfg->GetInt(p->arena->cfg, "Spree", "ResetSpreeOnShipFreqChange", 0))
	{
		struct pdata *d = PPDATA(p, pdkey);
		d->last = d->wins;
		d->wins = 0;
	}
}

local void FlagReset(Arena *arena, int freq, int points)
{
	if (cfg->GetInt(arena->cfg, "Spree", "ResetSpreeOnFlagWin", 0))
	{
		Link *link;
		Player *p;
		struct pdata *d;
	
		pd->Lock();
		FOR_EACH_PLAYER_P(p, d, pdkey)
		{
			if (p->arena != arena) continue;
			
			d->last = d->wins;
			d->wins = 0;
		}
		pd->Unlock();
	}
}

local helptext_t spree =
	"Module: spree\n"
	"Targets: arena, player\n"
	"Args: [<#>]\n"
	"Prints out the target player's current spree and best spree,\n"
	"if no target, yourself, or all players in the current arena\n"
	"with the specified spree or higher\n";

local void Cspree(const char *command, const char *params,
	Player *p, const Target *target)
{
	struct pdata *d;

	if (target->type == T_ARENA)
	{
		int min = strtol(params, NULL, 0);
		
		if (min > 0)
		{
			Player *t;
			Link *link;
			
			pd->Lock();
			FOR_EACH_PLAYER_P(t, d, pdkey)
			{
				if (t->arena != p->arena) continue;
				if (d->wins < min) continue;
				
				chat->SendMessage(p, "%s's spree: Current %d Best"
					" %d Last %d", t->name, d->wins,
					d->best, d->last);
			}
			pd->Unlock();
		}
		else
		{
			/* show player stats */
			d = PPDATA(p, pdkey);
			chat->SendMessage(p, "Spree: Current %d Best %d Last %d",
				d->wins, d->best, d->last);
		}
	}
	else if (target->type == T_PLAYER)
	{
		/* show target stats */
		d = PPDATA(target->u.p, pdkey);
		chat->SendMessage(p, "%s's spree: Current %d Best %d Last %d",
			target->u.p->name, d->wins, d->best, d->last);
	}
}

EXPORT const char info_spree[] = "v1.7b smong <soinsg@hotmail.com>";

EXPORT int MM_spree(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		pd       = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		cfg      = mm->GetInterface(I_CONFIG,     ALLARENAS);
		chat     = mm->GetInterface(I_CHAT,       ALLARENAS);
		cmd      = mm->GetInterface(I_CMDMAN,     ALLARENAS);
		objs     = mm->GetInterface(I_OBJECTS,    ALLARENAS);
		if (!pd || !cfg || !chat || !cmd)
			return MM_FAIL;

		pdkey = pd->AllocatePlayerData(sizeof(struct pdata));
		if (pdkey == -1) return MM_FAIL;

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		pd->FreePlayerData(pdkey);

		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(objs);
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		mm->RegCallback(CB_PLAYERACTION, PlayerAction, arena);
		mm->RegCallback(CB_KILL, Kill, arena);
		//mm->RegCallback(CB_FREQCHANGE, FreqChange, arena);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
		mm->RegCallback(CB_FLAGRESET, FlagReset, arena);

		cmd->AddCommand("spree", Cspree, arena, spree);

		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		cmd->RemoveCommand("spree", Cspree, arena);

		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, arena);
		mm->UnregCallback(CB_KILL, Kill, arena);
		//mm->UnregCallback(CB_FREQCHANGE, FreqChange, arena);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, arena);
		mm->UnregCallback(CB_FLAGRESET, FlagReset, arena);
	
		return MM_OK;
	}

	return MM_FAIL;
}

