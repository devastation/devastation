#spree by resoL & Avihay

#4-28-11 - resoL added upgrades with sprees and fixed spree ended "[0:0]" message
#	also added spree messages to match spree sounds
#4-30-11 - resoL added doublekill system
#4-30-11 - Avihay changed posting sounds to a sound queue
#10/10/11 refreactored as spree3 with all the features resoL and I could think about -Avihay
#14/10/11 lot's of bugfixes, and added extra bugs with the persistency feature -Avihay
#16/10/11 fixed some loading bugs and the disappearing data bug, hopefully -Avihay
#20/10/11 made the file loader more robust so loading failures won't effect module registration - Avihay
#25/3/12  moved multikill notifications to end of kill timer to reduce message spam - Avihay
#27/4/12  added automatic disableing of doublekill messages when there are more active players - Avihay
#28/4/12  hardened the modlue agains failing to register callbacks and dangaling callbacks - Avihay
from asss import *
import traceback,cPickle

game = get_interface(I_GAME)
stats = get_interface(I_STATS)
chat = get_interface(I_CHAT)


#delay between consecutive sound messages in centisec
TIME_BETWEEN_SOUNDS=150
#determins if a shipchange reset the sprees
STOP_SPREE_ON_SHIPCHANGE=False
#determins for how long will the next kill be considered a multi_kill
#MULTIKILL_RESET_TIME=200
#determins how often will a message appear
SPREE_HISTORY_LEN=5
### decides how often spree messages appear ###
#SPREE_PER_KILLS=5
###how many players to remember when they quit the arena###
#NUM_PLAYERS_TO_REMEMBER=10

###A list of sounds to use###
#It's size can change arbitrarily or be empty
SPREE_SOUNDS=[80, 81, 82, 83, 84, 85, 86, 87, 88]
### Overkill sound ###
#Used when the sound list is exhusted
OVERSPREE_SOUND=89
### Spree end sound ###
#Used when the sound list is exhusted
SPREEOVER_SOUND=90
###Spree Messages###
#SPREE_MESSAGES=["Killing Spree!",
	#"Rampage!",
	#"Dominating!",
	#"Headshot!",
	#"God-Like!",
	#"Monster Kill!",
	#"Wicked Sick!",
	#"Unstopable!",
	#"Holy Shit!"]
#Spree Messages
#SPREE_MESSAGES=["Killing Spree! - Recharge Upgraded",
	#"Rampage! - Energy Upgraded",
	#"Dominating! - Rotation Upgraded",
	#"Headshot! - Thrusters Upgraded",
	#"God-Like! - Recharge Upgraded",
	#"Monster Kill! - Energy Upgraded",
	#"Wicked Sick! - Rotation Upgraded",
	#"Unstopable! - Thrusters Upgraded",
	#"Holy Shit! - Upgrades MAX"]
### Overkill msg ###

#SPREE_GIVES_UPGRADES = False#True
SPREE_UPGRADES = [PRIZE_RECHARGE, PRIZE_ENERGY, PRIZE_ROTATION, PRIZE_THRUST, PRIZE_RECHARGE, PRIZE_ENERGY, PRIZE_ROTATION, PRIZE_THRUST]
OVERSPREE_UPGRADE = PRIZE_REPEL

#Multi Kill Messages
MULTIKILL_MESSAGES=["Double Kill!",
		    "Triple Kill!",
		    "Multi Kill!"]
### Overkill msg ###
#Used when the message list is exhusted
OVERMULTIKILL_MESSAGE="Ultra Kill!"

### A list of sounds to use ###
#It's size can change arbitrarily or be empty
MULTIKILL_SOUNDS=[94, 95, 96]
### Overkill sound ###?attmod spree3
#Used when the sound list is exhusted
OVERMULTIKILL_SOUND=89
###Shortend ship names to use when displaying output###
SHIP_NAMES=[' wb','jav','spd','lev','ter','wsl','lnc','srk']
###How many ships to display in a single line###
SHIPS_PER_LINE=2


############deva###########
SPREE_GIVES_UPGRADES = False#True
SPREE_MESSAGES=["Killing Spree!",
	"Rampage!",
	"Dominating!",
	"Headshot!",
	"God-Like!",
	"Monster Kill!",
	"Wicked Sick!",
	"Unstopable!",
	"Holy Shit!"]
#Used when the message list is exhusted
OVERSPREE_MESSAGE="Ultra Kill!"
MULTIKILL_RESET_TIME=200
SPREE_PER_KILLS=5
NUM_PLAYERS_TO_REMEMBER=22
DISABLE_DOUBLEKILL_THRESHOLD=8 #active players
############3S###########
#SPREE_GIVES_UPGRADES = True
#SPREE_MESSAGES=["Killing Spree! - Recharge Upgraded",
	#"Rampage! - Energy Upgraded",
	#"Dominating! - Rotation Upgraded",
	#"Headshot! - Thrusters Upgraded",
	#"God-Like! - Recharge Upgraded",
	#"Monster Kill! - Energy Upgraded",
	#"Wicked Sick! - Rotation Upgraded",
	#"Unstopable! - Thrusters Upgraded",
	#"Holy Shit! - Upgrades MAX"]
#Used when the message list is exhusted
#OVERSPREE_MESSAGE="Ultra Kill! - Upgrades MAX"
#MULTIKILL_RESET_TIME=400
#SPREE_PER_KILLS=3
#NUM_PLAYERS_TO_REMEMBER=10
#DISABLE_DOUBLEKILL_THRESHOLD=20 #active players


class timer():
	def __init__(self, func, time, interval=None, args=None):
		self.func=func
		self.time=time
		self.interval=interval
		self.args=args
		if interval is None or interval<=0:
			self.ref=set_timer(self.dispatch, time, 10000)
		else:
			self.ref=set_timer(self.dispatch, time, interval)
	def dispatch(self):
		try:
			if self.args is None:
				self.func()
			else:
				try:
					self.func(*self.args)
				except TypeError: #this is dumb, I blame python!
					self.func(self.args)
			if self.interval is None or self.interval<=0:
				self.remref()
		except:
			formatted_lines = traceback.format_exc().splitlines()
			for line in formatted_lines:
				chat.SendModMessage(line)
	def remref(self):
		self.ref=None

def reg_debug_callback(id, cb, arena):
	def dispatch(*args):
		try:
			return cb(*args)
		except:
			formatted_lines = traceback.format_exc().splitlines()
			for line in formatted_lines:
				chat.SendModMessage(line)
	return reg_callback(id, dispatch, arena)
	
def count_playing(arena):
	players = [ 0 ]
	def cb_count(p):
		if p.arena == arena and p.ship < SHIP_SPEC: 
			players[0] += 1
	for_each_player(cb_count)
	#chat.SendArenaMessage(arena, "playing players: %d" % (players[0]))
	return players[0]


#def SendDelayedMessage(args):
	#args:
	#0 - arena
	#1 - sound number
	#2 - string
	#3 - self reference to kill
#	chat.SendArenaSoundMessage(args[0], args[1], args[2])
#	args[3][0]=None
	
class ShipSpree():
	def __init__(self):
		self.clear()
	def clear(self):
		self.curr_spree=0
		self.prev_sprees=[]
		self.best_spree=0
		#self.multi_kill=0
		#self.multi_timer=None
	def onKill(self):
		self.curr_spree+=1
		#self.multi_kill+=1
		#if self.multi_timer is not None:
		#	self.multi_timer.remref()
		#self.multi_timer=timer(self.cb_clearMK,MULTIKILL_RESET_TIME)
		#if self.curr_spree > self.best_spree:
		#	self.best_spree+=1
		return self.curr_spree
	def onDeath(self):
		if self.curr_spree > self.best_spree:
			self.best_spree=self.curr_spree
		else:
			pass
		#chat.SendModMessage("there's a syntax error here and I can't find it")
		#chat.SendModMessage('%s %s'%(SPREE_HISTORY_LEN,type(SPREE_HISTORY_LEN)))
		self.prev_sprees=[self.curr_spree]+self.prev_sprees[:(int(SPREE_HISTORY_LEN)-1)]
		self.curr_spree=0
		return self.prev_sprees[0]
	#def cb_clearMK:
		#self.multi_kill=0
		#if self.multi_timer is not None:
			#self.multi_timer.remref()
			#self.multi_timer=None
	def remref(self):
		pass
		#if self.multi_timer is not None:
			#self.multi_timer.remref()
			#self.multi_timer=None

spreeDataToSpreeDict={}

class SpreeData():
	def __init__(self):
		self.shipSpree=[]
		for ship in range(8):
			self.shipSpree.append(ShipSpree())
		self.clear()
	def clear(self):
		#self.curr_spree=0
		#self.prev_sprees=[]
		#self.best_spree=0
		self.multi_kill=0
		self.multi_timer=None
	def onKill(self,ship,minimal_multikill):
		self.multi_kill+=1
		if self.multi_timer is not None:
			self.multi_timer.remref()
		self.multi_timer=timer(self.cb_clearMK, MULTIKILL_RESET_TIME,None,minimal_multikill)
		#if self.curr_spree > self.best_spree:
		#	self.best_spree+=1
		return self.shipSpree[ship].onKill(),self.multi_kill
	def onDeath(self,ship):
		#if self.curr_spree > self.best_spree:
			#self.best_spree=self.curr_spree
		#self.prev_sprees=[self.curr_spree]+self.prev_sprees[:SPREE_HISTORY_LEN-2]
		#self.curr_spree=0
		return self.shipSpree[ship].onDeath()
	def cb_clearMK(self,minimal_multikill):
		#moved multi_kills announce to here due to demand for less spam
		multi_kills=self.multi_kill
		if multi_kills >= minimal_multikill:
				sound = OVERMULTIKILL_SOUND
				if multi_kills <= 1+len(MULTIKILL_SOUNDS) :
					sound=MULTIKILL_SOUNDS[multi_kills-2]
				msg = OVERMULTIKILL_MESSAGE + "(%s)" % (multi_kills)
				if multi_kills <= 1+len(MULTIKILL_MESSAGES) :
					msg=MULTIKILL_MESSAGES[multi_kills-2] + "(%s)" % (multi_kills)
				whereAndWho = spreeDataToSpreeDict[self]
				whereAndWho[0].QueueArenaSoundMessage(sound, "[SPREE] %s - %s" % (whereAndWho[1], msg))
		
		self.multi_kill=0
		if self.multi_timer is not None:
			self.multi_timer.remref()
			self.multi_timer=None
	def remref(self):
		self.cb_clearMK(2)
		#if self.multi_timer is not None:
			#self.multi_timer.remref()
			#self.multi_timer=None

class Spree3():
	def __init__(self,arena):
		try:
			self.arena=arena
			#self.callbacks=[reg_debug_callback(CB_KILL, self.cb_kill, arena),
							#reg_debug_callback(CB_PLAYERACTION, self.cb_paction, arena)]
			self.callbacks=[reg_callback(CB_KILL, self.cb_kill, arena),
							reg_callback(CB_PLAYERACTION, self.cb_paction, arena),
							reg_debug_callback(CB_SHIPFREQCHANGE, self.cb_shipchange_disable_doublekill, arena),
							#reg_debug_callback(CB_SHIPCHANGE, self.cb_shipchange_disable_doublekill, arena),
							]
			if STOP_SPREE_ON_SHIPCHANGE:
				self.callbacks.append(reg_debug_callback(CB_SHIPFREQCHANGE, self.cb_shipchange, arena))
			self.command=add_command("spree", self.c_spree, arena)
			self.ReenterDB={}
			self.ReenterList=[]
			self.minimal_multikill=0#unset it so it won't cry like a baby next function!!!!!
			self.disable_doublekill_check()
			try:
				reenterFile=open('arenas/'+self.arena.basename+'/'+self.arena.name+'.spree','rb')
				self.ReenterDB , self.ReenterList = cPickle.load(reenterFile)
				reenterFile.close()
			except IOError:
				chat.SendModMessage("[SPREE]Couldn't load spree data for arena %s.    Reason: couldn't open file" % (self.arena.name))
				chat.SendModMessage("[SPREE]Starting with empty sprees." )
			except:
				chat.SendModMessage("[SPREE]Couldn't load spree data for arena %s.    Reason:" % (self.arena.name))
				formatted_lines = traceback.format_exc().splitlines()
				for line in formatted_lines:
					chat.SendModMessage(line)
				chat.SendModMessage("[SPREE]Starting with empty sprees." )
			self.soundQueue=[]
			self.soundTimer=None
			def cb_addData(p):
				if p.arena==self.arena:
					try:
						p.spreeData
					except AttributeError:
						self.popFromReenter(p)
			for_each_player(cb_addData)
		except:
			self.callbacks=None
			raise
	def remref(self):
		self.command=None
		self.callbacks=None
		self.soundQueue=None
		#if self.soundTimer is not None:
			#self.soundTimer.remref()
		self.soundTimer=None
		for name in self.ReenterDB:
			self.ReenterDB[name].remref()#should just clear timers
		#print "db before saveing live ones"
		#print (self.ReenterDB , self.ReenterList)
		def cb_addData(p):#let's add who's left to the db if they have the data
			if p.arena==self.arena:
				try:
					p.spreeData
					self.pushToReenter(p)
					del p.spreeData
				except AttributeError:
					pass
		for_each_player(cb_addData)
		#print "db after saveing live ones"
		#print (self.ReenterDB , self.ReenterList)
		reenterFile=open('arenas/'+self.arena.basename+'/'+self.arena.name+'.spree','wb')
		#chat.SendModMessage('%s ' % (reenterFile))
		#chat.SendModMessage(', %s' % (reenterFile.write))
		cPickle.dump((self.ReenterDB , self.ReenterList),reenterFile)
		reenterFile.close()
			
	def QueueArenaSoundMessage(self,sound,message):
		self.soundQueue.append((sound,message))#store a tuple
		if self.soundTimer == None:
			def dispatch():
				#chat.SendModMessage("%s"%(self.soundQueue))
				if (len(self.soundQueue)==0):
					self.soundTimer = None
				else:
					chat.SendArenaSoundMessage (self.arena, self.soundQueue[0][0], self.soundQueue[0][1])
					self.soundQueue.pop(0)
			self.soundTimer = set_timer(dispatch, 0, TIME_BETWEEN_SOUNDS)
	def pushToReenter(self,p):
		name=p.name.lower()
		self.ReenterDB[name]=p.spreeData
		if name in self.ReenterList:
			self.ReenterList.remove(name)
		self.ReenterList.append(name)
		if len(self.ReenterList)>NUM_PLAYERS_TO_REMEMBER:
			name=self.ReenterList[0]
			del self.ReenterDB[name]
			self.ReenterList.remove(name)
		del spreeDataToSpreeDict[p.spreeData] #needed to know where to queue sound messages
	def popFromReenter(self,p):
		name=p.name.lower()
		if name in self.ReenterDB:
			p.spreeData=self.ReenterDB[name]
			del self.ReenterDB[name]
			self.ReenterList.remove(name)
		else:
			p.spreeData=SpreeData()
			chat.SendMessage(p,"[SPREE]You currently don't have any Spree data. If you are not active it will be reset over time. To keep your spree data please log in often. You may check players' sprees with ?spree")
		spreeDataToSpreeDict[p.spreeData]=(self,p.name) #needed to know where to queue sound messages
	
	def cb_paction(self, p, action, arena):
		if action == PA_ENTERARENA:
			self.popFromReenter(p)
		elif action == PA_LEAVEARENA:
			self.pushToReenter(p)
			try:
				del p.spreeData
			except AttributeError:
				pass
			#self.ReenterDB[name]=p.spreeData
			#self.ReenterList.append(name)
			#if len(self.ReenterList)>NUM_PLAYERS_TO_REMEMBER:
				#name=self.ReenterList[0]
				#del self.ReenterDB[name]
				#self.ReenterList.remove(name)
#start callback to reset kills on ship and/or freq change
	def cb_shipchange(self,p, newship, oldship,  newfreq,  oldfreq):
		try:
			if newship != SHIP_SPEC and p.spreeData.curr_spree != 0:
				chat.SendMessage(p, "[SPREE] %s, you just changed to ship %s on freq %s. As a result, your Spree was reset" % (p.name, newship+1, newfreq))
				p.spreeData.onDeath(oldship)
		except AttributeError:
			pass
	def cb_shipchange_disable_doublekill(self,p, newship, oldship,  newfreq,  oldfreq):
	#def cb_shipchange_disable_doublekill(self,p, newship, newfreq): #asss 1.4.4
		self.disable_doublekill_check()
	def disable_doublekill_check(self):
		if count_playing(self.arena)>=DISABLE_DOUBLEKILL_THRESHOLD:
			if self.minimal_multikill!=3:
				self.minimal_multikill=3
				chat.SendArenaMessage(self.arena,"[SPREE] %s or more active players, disableing doublekills." % DISABLE_DOUBLEKILL_THRESHOLD )
		else:
			if self.minimal_multikill!=2:
				self.minimal_multikill=2
				chat.SendArenaMessage(self.arena,"[SPREE] Less then %s active players, enableing doublekills." % DISABLE_DOUBLEKILL_THRESHOLD )
	


	#start callback to check wins
	def cb_kill(self,arena, killer, killed, bty, flagcount, pts, green):
		if killed.freq != killer.freq:
			killedspree , killerspree , multi_kills = (None,None,None)
			try:
				killedspree = killed.spreeData.onDeath(killed.ship)
			except AttributeError:
				self.popFromReenter(killed)
				killedspree = killed.spreeData.onDeath(killed.ship)
			try:
				killerspree , multi_kills = killer.spreeData.onKill(killer.ship,self.minimal_multikill)
			except AttributeError:
				self.popFromReenter(killer)
				killerspree , multi_kills = killer.spreeData.onKill(killer.ship,self.minimal_multikill)
			if killedspree >= SPREE_PER_KILLS:
				if killerspree == 0:
					self.QueueArenaSoundMessage(SPREEOVER_SOUND,
					"[SPREE] %s's [%s:0] killing spree ended by %s" % (killed.name,killedspree,killer.name))
				else:
					self.QueueArenaSoundMessage(SPREEOVER_SOUND,
					"[SPREE] %s's [%s:0] killing spree ended by %s [%s:0]" % (killed.name,killedspree,killer.name,killerspree))
			if (killerspree % SPREE_PER_KILLS) == 0:
				sound = OVERSPREE_SOUND
				if (killerspree / SPREE_PER_KILLS) <= len(SPREE_SOUNDS) :
					sound=SPREE_SOUNDS[(killerspree / SPREE_PER_KILLS)-1]
				msg = OVERSPREE_MESSAGE
				if (killerspree / SPREE_PER_KILLS) <= len(SPREE_SOUNDS) :
					msg=SPREE_MESSAGES[(killerspree / SPREE_PER_KILLS)-1]
				self.QueueArenaSoundMessage(sound, "[SPREE] %s [%s:0] %s" % (killer.name, killerspree, msg))
				if SPREE_GIVES_UPGRADES:
					upgrade=OVERSPREE_UPGRADE
					if (killerspree / SPREE_PER_KILLS) <= len(SPREE_UPGRADES) :
						upgrade=SPREE_UPGRADES[(killerspree / SPREE_PER_KILLS)-1]
					game.GivePrize(killer, (upgrade), 1)
			#Moveing multi_kills messages to end of timer because they spam alot
			#and peopel complain
			#if multi_kills > 1:
				#sound = OVERMULTIKILL_SOUND
				#if multi_kills <= 1+len(MULTIKILL_SOUNDS) :
					#sound=MULTIKILL_SOUNDS[multi_kills-2]
				#msg = OVERMULTIKILL_MESSAGE + "(%s)" % (multi_kills)
				#if multi_kills <= 1+len(MULTIKILL_MESSAGES) :
					#msg=MULTIKILL_MESSAGES[multi_kills-2]
				#self.QueueArenaSoundMessage(arena, sound, "[SPREE] %s - %s" % (killer.name, msg))
		return pts, green


	def c_spree(self,cmd, params, p, targ):
		"""\
	Sent in pub chat will tell you how many kills since your last death.
	Sent to a player will tell you how many kills since their last death.
	"""
		if p.arena!=self.arena: return #stupid asss filter
		invalid=True
		if isinstance(targ, ArenaType):
			chat.SendMessage(p, "[SPREE]%s, your sprees are:" % (p.name))
			invalid=False
			targ=p
		else:
			if isinstance(targ, PlayerType) and targ.arena==self.arena:
				chat.SendMessage(p, "[SPREE]%s's sprees are:" % (targ.name))
				invalid=False
		if invalid: return
		linebuff=[]
		i=-1 #ship counter
		printed_none=True
		try:
			targ.spreeData
		except AttributeError:
			self.popFromReenter(targ)
		for shipSpree in targ.spreeData.shipSpree:
			i+=1
			#if ship has no sprees in it, don't print
			if shipSpree.curr_spree+shipSpree.best_spree == 0: continue
			printed_none=False
			linebuff.append("%s: current:%s history:%s best:%s"%(SHIP_NAMES[i],shipSpree.curr_spree, shipSpree.prev_sprees, shipSpree.best_spree))
			if len(linebuff)==SHIPS_PER_LINE:
				chat.SendMessage(p, "[SPREE]%s" % (' , '.join(linebuff)))
				linebuff=[]
		if printed_none:
			linebuff=['No sprees']
		if len(linebuff)!=0:
			chat.SendMessage(p, "[SPREE]%s" % (' , '.join(linebuff)))

   
def mm_attach(arena):
	try:
		arena.spree3=Spree3(arena)
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(line)

def mm_detach(arena):
	try:
		arena.spree3.remref()
		del arena.spree3
	except:
		formatted_lines = traceback.format_exc().splitlines()
		for line in formatted_lines:
			chat.SendModMessage(line)