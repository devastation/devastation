 /**************************************************************
 * Command List Module
 *
 * The following module provides users with a rough guide to
 * the commands they are able to use based on their level.
 *
 * Last updated: January 19th, 2010.
 *
 * ************************************************************/
 
#include <string.h>
#include "asss.h"

/* Interfaces */
local Imodman *mm;
local Ichat *chat;
local Icmdman *cmd;
local Icapman *capman;

/* Help Text */
local helptext_t commandlist =
"Targets: none\n"
"Args: none\n"
"Displays a list of all available commands to the player.";

/* ?CommandList */
local void CommandList(const char *command, const char *params, Player *p, const Target *target)
{
    chat->SendMessage(p,"Devastation Server Commands :");
    
    /* All Players */
    chat->SendMessage(p,"|-----------------|-----------------|-----------------|");
    chat->SendMessage(p,"| help            | go              | arena           |");
    chat->SendMessage(p,"| spec            | stats           | jackpot         |");
    chat->SendMessage(p,"| cheater         | where           | zone            |");
    chat->SendMessage(p,"| obscene         | listmod         | lag             |");
    chat->SendMessage(p,"| last            | listarena       | usage           |");
    chat->SendMessage(p,"| sheep           | passwd          | sendfile        |");
    chat->SendMessage(p,"| cancelfile      | acceptfile      | ping            |");
    chat->SendMessage(p,"| owner           | find            | how to play     |");
    chat->SendMessage(p,"| lagme           | lp              | lagcheck        |");
    chat->SendMessage(p,"| credits         | creds           | donate          |");
    chat->SendMessage(p,"| buy             | buylist         | spree           |");
    chat->SendMessage(p,"| togglecredits   | togglecreds     | c               |");
    chat->SendMessage(p,"|-----------------|-----------------|-----------------|");
    
    /* If the player is Mod+ */
    if (capman->HasCapability(p,CAP_IS_STAFF))
    {
        chat->SendMessage(p,"| info            | a               | z               |");
        chat->SendMessage(p,"| warn            | kick            | where           |");
        chat->SendMessage(p,"| setship         | setfreq         | specall         |");
        chat->SendMessage(p,"| prize           | shipreset       | *spec           |");
        chat->SendMessage(p,"| lock            | unlock          | lockarena       |");
        chat->SendMessage(p,"| unlockarena     | warpto          | addcredits      |");
        chat->SendMessage(p,"| addcreds        | removecredits   | removecreds     |");
        chat->SendMessage(p,"| setcredits      | setcredits      | resetcredits    |");
        chat->SendMessage(p,"| resetcreds      | flagstocenter   | chatbot         |");
        chat->SendMessage(p,"| doors           | doormode        | restrictions    |");
        chat->SendMessage(p,"| setcm           | getcm           | ballcount       |");
        chat->SendMessage(p,"| host            | start           | getargs         |");
        chat->SendMessage(p,"| stop            | stopevent       | alias           |");
        chat->SendMessage(p,"|-----------------|-----------------|-----------------|");
    }
    
    /* If the player is Smod+ */
    if (capman->HasCapability(p,"higher_than_mod"))
    {
        chat->SendMessage(p,"| aa              | az              |                 |");
        chat->SendMessage(p,"| moveflag        | watchdamage     | watchgreen      |");
        chat->SendMessage(p,"| energy          | quickfix        | getsettings     |");
        chat->SendMessage(p,"| putmap          | /?usage         | resetturrets    |");
        chat->SendMessage(p,"| attmod          | setjackpot      | *info           |");
        chat->SendMessage(p,"| *einfo          | *tinfo          | recyclearena    |");
        chat->SendMessage(p,"| neutflag        | flaginfo        | listmidbans     |");
        chat->SendMessage(p,"| delmidban       |                 |                 |");
        chat->SendMessage(p,"|-----------------|-----------------|-----------------|");
    }
    
    /* If the player is Sysop+ */
    if (capman->HasCapability(p,"higher_than_smod"))
    {
        chat->SendMessage(p,"| shutdown -r     | netstats        | lastlog         |");
        chat->SendMessage(p,"| *log            | lsmod           | lsmod -a        |");
        chat->SendMessage(p,"| modinfo         | rmmod           | attmod          |");
        chat->SendMessage(p,"| points          | getfile         | putfile         |");
        chat->SendMessage(p,"| cd              | pwd             | delfile         |");
        chat->SendMessage(p,"| makearena       | reloadconf      | dropturret      |");
        chat->SendMessage(p,"| set_local_password                | setgroup        |");
        chat->SendMessage(p,"| setgroup lag    | getgroup        | destroy         |");
        chat->SendMessage(p,"|-----------------|-----------------|-----------------|");
    }
    chat->SendMessage(p,"Notes : * Most Subgame *commands equally work in Devastation.");
    chat->SendMessage(p,"        * Some commands, such as ?lag or ?credits, can also be sent");
    chat->SendMessage(p,"           as a private message to another player to reveal their info.");
    chat->SendMessage(p,"        * Use ?c <command name> for more information on what each command does.");
    chat->SendMessage(p,"[If you do not see the full text, press ESC]");
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

EXPORT const char info_commandlist[] = "(Devastation) v0.2b Hakaku";

EXPORT int MM_commandlist(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;

        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);
        capman = mm->GetInterface(I_CAPMAN, ALLARENAS);

        if (!chat || !cmd || !capman)
        {
            mm->ReleaseInterface(capman);
            mm->ReleaseInterface(cmd);
            mm->ReleaseInterface(chat);
            
            return MM_FAIL;
        }
        else
        {
            cmd->AddCommand("commandlist", CommandList, ALLARENAS, commandlist);
            cmd->AddCommand("commands", CommandList, ALLARENAS, commandlist);
            cmd->AddCommand("list", CommandList, ALLARENAS, commandlist);

            return MM_OK;
        }
    }
    else if (action == MM_UNLOAD)
    {
        cmd->RemoveCommand("list", CommandList, ALLARENAS);
        cmd->RemoveCommand("commands", CommandList, ALLARENAS);
        cmd->RemoveCommand("commandlist", CommandList, ALLARENAS);

        mm->ReleaseInterface(capman);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(chat);

        return MM_OK;
    }
    return MM_FAIL;
}

