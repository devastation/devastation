#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "asss.h"
#include "reldb.h"
#include "jackpot.h"

/* Interfaces */
local Imodman *mm;
local Ireldb *db;
local Ichat *chat;
local Iplayerdata *pd;
local Iarenaman *aman;
local Imainloop *ml;
local Istats *stats;
local Ijackpot *jp;

//scores
#define CREATE_SCORES_TABLE \
" CREATE TABLE IF NOT EXISTS `scores` (" \
"   `playername` varchar(24) NOT NULL default ''," \
"   `squad` varchar(24) NOT NULL default ''," \
"   `flagpoints` int(11) NOT NULL default'0'," \
"   `killpoints` int(11) NOT NULL default'0'," \
"   `totalpoints` int(11) NOT NULL default'0'," \
"   `wins` int(11) NOT NULL default'0'," \
"   `losses` int(11) NOT NULL default'0'," \
"   `ratio` int(11) NOT NULL default'0'," \
"   `rating` int(11) NOT NULL default'0'," \
"   `average` int(11) NOT NULL default'0'," \
"   PRIMARY KEY  (`playername`)" \
" );"

//jackpot
#define CREATE_JACKPOT_TABLE \
" CREATE TABLE IF NOT EXISTS `currentjackpot` (" \
"   `jackpot` int(11) NOT NULL default'0'," \
"   PRIMARY KEY  (`jackpot`)" \
" );"

/************************************************************************/
/*                    Main Database Interaction                         */
/************************************************************************/

/* Create the scores table */
local void init_db(void)
{
	//make sure the logins table exists
	db->Query(NULL, NULL, 0, CREATE_SCORES_TABLE);
	db->Query(NULL, NULL, 0, CREATE_JACKPOT_TABLE);
}

/* Save single player's scores */
local void savePlayer(Player *p)
{
	int wins = stats->GetStat(p, STAT_KILLS, INTERVAL_RESET);
	int losses = stats->GetStat(p, STAT_DEATHS, INTERVAL_RESET);
	int flagpoints = stats->GetStat(p, STAT_FLAG_POINTS, INTERVAL_RESET);
	int killpoints = stats->GetStat(p, STAT_KILL_POINTS, INTERVAL_RESET);
	int totalpoints = flagpoints + killpoints;

	int ratio = 0;
	if (losses <= 0)
		ratio = wins;
	else
		ratio = wins / losses;

	int rating = ((totalpoints * 10 + (wins - losses) * 100) / (wins + 100));

	int average = 0;
	if (wins <= 0)
		average = killpoints;
	else
		average = killpoints / wins;
	
	/* Rather than checking, then either 'update' or 'insert', we use 'replace' here instead. */
	db->Query(NULL,NULL,0,"REPLACE INTO scores (playername, squad, flagpoints, killpoints, "
		"totalpoints, wins, losses, ratio, rating, average) VALUES (?, ?, #, #, #, #, #, #, #, #)", p->name, p->squad, flagpoints, killpoints, totalpoints, wins, losses, ratio, rating, average);
}

/* Save every player's scores */
local void updateDB()
{
	Player *p;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		savePlayer(p);
	}
	pd->Unlock();
}

//sendjackpot
local void SendJackpot(Arena *arena)
{
	int jackpot = jp->GetJP(arena);
	db->Query(NULL,NULL,0,"UPDATE `currentjackpot` SET `jackpot` = #", jackpot);
	return;
}

//timer
local int jackpottimer(void *arena_)
{
	Arena *arena = arena_;
	SendJackpot(arena);
	return 1;
}

//flag reset
local void FlagReset(Arena *arena, int freq, int points, int stat, int interval, Player *p, Target *target)
{
	SendJackpot(arena);
}

/* When a player exits the arena, save his score */
local void cPlayerAction(Player *p, int action, Arena *arena)
{
	if (!IS_HUMAN(p))
		return;

	if (action == PA_LEAVEARENA || action == PA_DISCONNECT)
	{
		savePlayer(p);
	}
}

/************************************************************************/
/*                             Module Init                              */
/************************************************************************/

EXPORT const char info_playerastats[] = "(Devastation) v.alpha Hakaku";

EXPORT int MM_playerastats(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;

		db = mm->GetInterface(I_RELDB, ALLARENAS);
		chat = mm->GetInterface(I_CHAT, ALLARENAS);
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
		stats = mm->GetInterface(I_STATS, ALLARENAS);
		jp = mm->GetInterface(I_JACKPOT, ALLARENAS);

		if (!db || !chat || !pd || !aman || !ml || !stats || !jp)
		{
			mm->ReleaseInterface(jp);
			mm->ReleaseInterface(stats);
			mm->ReleaseInterface(ml);
			mm->ReleaseInterface(aman);
			mm->ReleaseInterface(pd);
			mm->ReleaseInterface(chat);
			mm->ReleaseInterface(db);
			
			return MM_FAIL;
		}
		else
		{
			init_db();

			return MM_OK;
		}
	}
	else if (action == MM_UNLOAD)
	{
		updateDB();

		mm->ReleaseInterface(jp);
		mm->ReleaseInterface(stats);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(db);

		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		mm->RegCallback(CB_PLAYERACTION, cPlayerAction, arena);
		mm->RegCallback(CB_FLAGRESET, FlagReset, arena);
		
		ml->SetTimer(jackpottimer, 15000, 15000, arena, arena);
		
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		ml->ClearTimer(jackpottimer, arena);

		mm->UnregCallback(CB_FLAGRESET, FlagReset, arena);
		mm->UnregCallback(CB_PLAYERACTION, cPlayerAction, arena);
		
		return MM_OK;
	}
	return MM_FAIL;
}
