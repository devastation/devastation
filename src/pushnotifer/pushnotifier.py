from asss import *
import re
import urllib
import urllib2
import json

#This ASSS Python module checks to see the deva bd bot announce a game,
#  then it does an http post to the Firebase Cloud Messaing (FCM) server,
#  which should notify subscribed FCM clients (ie. an Android app).

CONFIG_NAME = "Push_Notifier"
settings = {"url":"", "key":"", "topics":"", "time_to_live":""}

cfg = get_interface(I_CONFIG)
for item in settings:
   settings[item] = cfg.GetStr(None, CONFIG_NAME, item)

def check_chat_message(player, type, sound, target, freq, text):
    #Deva-baseduel2-bot> Starting baseduel 2v2 in 10 seconds, ?go bd
    name_match = re.search("^Deva-baseduel[\d]?-bot$", player.name)
    if name_match:
        announce_match = re.search("Starting baseduel (\d+)v(\d+)", text)

        if announce_match:
            freq_0_count = announce_match.group(1)
            freq_1_count = announce_match.group(2)
            push_notify(freq_0_count, freq_1_count)

def push_notify(freq_0_count, freq_1_count):
    message = freq_0_count + "vs" + freq_1_count + " Base Duel Starting!"

    values = {
        "priority": "high",
        "data": {"message": message},
        "to": "/topics/" + settings["topics"],
        "time_to_live": int(settings["time_to_live"])
    }

    headers = {
       "authorization": "key=" + settings["key"],
       "cache-control": "no-cache",
       "content-type": "application/json"
    }

    data = json.dumps(values)
    req = urllib2.Request(settings["url"], data, headers)

    try:
        response = urllib2.urlopen(req, timeout=1)
        the_page = response.read()
        print(the_page)
    except urllib2.HTTPError as e:
        raise Exception("HTTP Error " + str(e.code) + ": " + e.read())

chat_cb = reg_callback(CB_CHATMSG, check_chat_message)

#Checking the log for "*arena" messages is hackish,
#  so for now rely on the baseduel bot sending MSG_CHAT messages
#def check_arena_message(text):
#    #I <cmdman> {bd} [Deva-baseduel2-bot] command (arena): aa Starting baseduel 2v2 in 10 seconds, ?go bd
#    match = re.match("^I <cmdman> \{\w+\} \[Deva-baseduel\d-bot\] command \(arena\): aa Starting baseduel (\d+)v(\d+)", text)
#    if match:
#        freq_0_count = match.group(1)
#        freq_1_count = match.group(2)
#        push_notify(freq_0_count, freq_1_count)
#log_cb = reg_callback(CB_LOGFUNC, check_arena_message)