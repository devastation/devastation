Install Steps:
==============

- The pushnotifier directory should be copied into ASSS' src/
- Make
- In <asss>/conf/global.conf set:

```ini
[ Push_Notifier ]
url = https://fcm.googleapis.com/fcm/send
key = <FCM Key>
topics = games
time_to_live = 1800
```

- In <asss>/conf/modules.conf set: <py> pushnotifier
- Restart ASSS