/*
 * ref:noanti
 *
 * [Misc]
 * NoAntiRadius=0
 *
 * Takes antiwarp away from any player with it activated in the
 *  center of the map.
 *
 * 23/11/03 - Created by Smong
 *  Warn message copied from MERVBot
 *
 * Jul 5 08 - minor update by Hakaku for 1.4.4
 *  Replaced "SPEC" by "SHIP_SPEC"
 *  Fixed warn message
 *
 * Aug 26 08 - fix duo spamming
 */

#include "asss.h"
#include "util.h"

typedef struct Pdata
{
    char spam;
} Pdata;

local int playerKey;

typedef struct {
	unsigned char active;
} ArenaData;

local void ArenaAction(Arena *arena, int action);
local int  timer(void *);

local int adkey;

local Imodman     *mm;
local Iconfig     *cfg;
local Iplayerdata *pd;
local Igame       *game;
local Ichat       *chat;
local Iarenaman   *aman;
local Imainloop   *mainloop;

local void ArenaAction(Arena *arena, int action)
{
	ArenaData *adata = P_ARENA_DATA(arena, adkey);
	int radius = cfg->GetInt(arena->cfg, "Misc", "NoAntiRadius", 0);
	if (action == AA_CREATE)
	{
		if (radius == 0)
		{
			adata->active = 0;
			return;
    	}
    	
		mainloop->SetTimer(timer, 500, cfg->GetInt(arena->cfg,
			"Misc", "SendPositionDelay", 20) * 2, arena, arena);
  	  	adata->active = 1;
  	}
	else if (action == AA_CONFCHANGED)
	{
		if (adata->active == 1)
		{
			mainloop->ClearTimer(timer, arena);
			adata->active = 0;
		}
		
		if (radius == 0) return;
		
		mainloop->SetTimer(timer, 500, cfg->GetInt(arena->cfg,
			"Misc", "SendPositionDelay", 20) * 2, arena, arena);
  	  	adata->active = 1;
	}
	else if (action == AA_DESTROY)
	{
		mainloop->ClearTimer(timer, arena);
		adata->active = 0;
	}
}

local void PlayerAction(Player *p, int action)
{
	if (action == PA_ENTERARENA)
	{
        Pdata *data = PPDATA(p, playerKey);
        data->spam = 0;
    }
}

local int spamOff(void *arena_)
{
    Player *p;
    
    Link*link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        Pdata *data = PPDATA(p, playerKey);
        data->spam = 0;
    }
    pd->Unlock();
    return 0;
}

local int timer(void *arena_)
{
	Arena *arena = arena_;
	int x, y, r, radius, cost;
//	Link wlink;
//	LinkedList lst = { &wlink, &wlink };
	Target tgt;

	Link *link;
	Player *p;
	
	radius = cfg->GetInt(arena->cfg, "Misc", "NoAntiRadius", 0);
	radius = radius * radius;
	cost = cfg->GetInt(arena->cfg, "Cost", "AntiWarp", 0);

	tgt.type = T_PLAYER;

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena != arena) continue;
		if (p->p_ship == SHIP_SPEC) continue;
		if ( !IS_STANDARD(p) ) continue;
		if (p->status != S_PLAYING) continue;

		if ( !(p->position.status & 0x08) ) continue;

		x = (p->position.x / 16) - 512;
		y = (p->position.y / 16) - 512;
		r = x*x + y*y;
		if (r > radius) continue;
/*		
		wlink.data = p;
		chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, SOUND_BEEP1,
				"Antiwarp is not allowed in center. Type ?buy "
				"AntiWarp to regain antiwarp at cost of %d pts.",
				cost);

		chat->SendSoundMessage(p, SOUND_BEEP1, "Antiwarp is not "
			"allowed in center. Type ?buy AntiWarp to regain "
			"antiwarp at cost of %d pts.", cost);
*/

        Pdata *data = PPDATA(p, playerKey);
        if (data->spam != 1)
        {
 	    	Link link = { NULL, p };
            LinkedList lst = { &link, &link };
            chat->SendAnyMessage(&lst, MSG_SYSOPWARNING, SOUND_BEEP1, NULL, "WARNING: Antiwarp is not allowed in center.\n");
        
    		tgt.u.p = p;
    		tgt.type = T_PLAYER;
	    	game->GivePrize(&tgt, - PRIZE_ANTIWARP, 1);
	    	chat->SendModMessage("Player %s was using antiwarp in center.", p->name);
	    	/* Spam protection */
	    	data->spam = 1;
	    	mainloop->SetTimer(spamOff, 500, 500, p->arena, NULL);
        }
	}
	pd->Unlock();
	return 1;
}

EXPORT const char info_noanti[] = "(Devastation) v1.02 modified by Hakaku";

EXPORT int MM_noanti(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		cfg      = mm->GetInterface(I_CONFIG,     ALLARENAS);
		pd       = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		game     = mm->GetInterface(I_GAME,       ALLARENAS);
		chat     = mm->GetInterface(I_CHAT,       ALLARENAS);
		aman     = mm->GetInterface(I_ARENAMAN,   ALLARENAS);
		mainloop = mm->GetInterface(I_MAINLOOP,   ALLARENAS);
		if (!cfg || !pd || !game || !chat || !aman || !mainloop)
			return MM_FAIL;
		
		adkey = aman->AllocateArenaData( sizeof(ArenaData) );
		if (adkey == -1) return MM_FAIL;
		
		playerKey = pd->AllocatePlayerData(sizeof(Pdata));
        if (!playerKey)
            return MM_FAIL;
		
		mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->RegCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);
		
		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		//Unloading this module before all arenas have been
		mainloop->ClearTimer(timer, NULL);
		mainloop->ClearTimer(spamOff, NULL);
		// destroyed will probably cause the server to crash
		aman->FreeArenaData(adkey);
		pd->FreePlayerData(playerKey);

		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);

		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(mainloop);
		return MM_OK;
	}

	return MM_FAIL;
}

