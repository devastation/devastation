/**
 * Handicap
 * 
 * Decrease taken damage for teams with lesser players.
 * Currently supports team 0 and team 1   
 */

#include "asss.h"
#include "attrman.h"
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#ifdef NDEBUG
#define assertlm(x)       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

static Imodman     *mm;
static Ilogman     *lm;
static Iconfig     *cfg;
static Iarenaman   *aman;
static Iplayerdata *pd;
static Iattrman    *attrman;

typedef struct
{
        bool active;
        AttrmanSetter setter;
} ArenaData;
static int arenaKey = -1;

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<handicap> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static ArenaData* GetArenaData(Arena *arena)
{
        assertlm(arena);
        assertlm(arenaKey != -1);
        return P_ARENA_DATA(arena, arenaKey);
}

static void UpdateAttribute(Arena *arena)
{
        Link *link;
        Player *p;
        int freqcount[] = {0, 0};
        ArenaData *ad = GetArenaData(arena);
        Target tgt;
        
        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (p->arena != arena || !IS_STANDARD(p) || p->p_ship == SHIP_SPEC)
                {
                        continue;
                }
                
                if (p->p_freq >= 0 && p->p_freq < 2)
                {
                        ++freqcount[p->p_freq];
                }
        }
        pd->Unlock();
        
        attrman->Lock();
        attrman->UnsetAll(ad->setter);
        
        int freq = -1;
        double ratio = 1;
        double ratioInverse = 1;
        if (freqcount[0] > 0 && freqcount[1] > 0)
        { 
                if (freqcount[0] > freqcount[1])
                {
                        ratio = freqcount[1] / (double) freqcount[0];
                        freq = 1;
                }
                else if (freqcount[0] < freqcount[1])
                {
                        ratio = freqcount[0] / (double) freqcount[1];
                        freq = 0;
                }
        }
        
        ratioInverse = ratio == 0 ? 1 : 1 / ratio;
        
        if (freq >= 0)
        {
                tgt.type = T_FREQ;
                tgt.u.freq.arena = arena;
                tgt.u.freq.freq = freq;
                
                #define X(attr, inverse) \
                do { \
                        double val = cfg->GetInt(arena->cfg, #attr, NULL, 0); \
                        val *= (inverse) ? ratioInverse : ratio; \
                        val = round(val); \
                        attrman->SetValue(&tgt, ad->setter, "cset::" #attr, false, ATTRMAN_NO_SHIP, (long) val); \
                } while (0)
                
                #define XS(attr, inverse) \
                do { \
                    for (signed char ship = 0; ship < 8; ++ship) \
                    {\
                            double val = cfg->GetInt(arena->cfg, cfg->SHIP_NAMES[ship], #attr, 0); \
                            val *= (inverse) ? ratioInverse : ratio; \
                            val = round(val); \
                            attrman->SetValue(&tgt, ad->setter, "cset::" #attr, false, ship, (long) val); \
                    } \
                } while (0)
                
                X(Bomb:BombDamageLevel, 0);
                X(Bomb:EBombShutdownTime, 0);
                X(Bomb:JitterTime, 0);
                X(Bullet:BulletDamageLevel, 0);
                X(Bullet:BulletDamageUpgrade, 0);
                X(Burst:BurstDamageLevel, 0);
                X(Misc:DecoyAliveTime, 0);
                X(Shrapnel:InactiveShrapDamage, 0);
                X(Shrapnel:ShrapnelDamagePercent, 0);

                /*XS(AfterburnerEnergy, 0);
                XS(AntiWarpEnergy, 0);
                XS(BombFireEnergy, 0);
                XS(BombFireEnergyUpgrade, 0);
                XS(BulletFireEnergy, 0);
                XS(CloakEnergy, 0);
                XS(LandmineFireEnergy, 0);
                XS(LandmineFireEnergyUpgrade, 0);
                XS(MultiFireEnergy, 0);
                XS(StealthEnergy, 0);
                XS(XRadarEnergy, 0);
                XS(InitialEnergy, 1);
                XS(MaximumEnergy, 1);
                XS(UpgradeEnergy, 1);*/

                #undef X
                #undef XS
                
                lm->LogA(L_DRIVEL, "handicap", arena, "Applied a damage reduction of %d%% to freq %d",
                        (int) round(100 - ratio * 100),
                        freq
                );
        }
        
        attrman->UnLock();
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
        if (IS_STANDARD(p))
        {
                UpdateAttribute(p->arena);
        }
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
        if (IS_STANDARD(p) && arena)
        {
                UpdateAttribute(arena);
        }
}

EXPORT const char info_handicap[] = "Handicap (" ASSSVERSION ", " BUILDDATE ") by JoWie <jowie@welcome-to-the-machine.com>\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cfg    );
        mm->ReleaseInterface(lm     );
        mm->ReleaseInterface(aman   );
        mm->ReleaseInterface(pd     );
        mm->ReleaseInterface(attrman);
}

EXPORT int MM_handicap(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;
                
                cfg     = mm->GetInterface(I_CONFIG    , ALLARENAS);
                lm      = mm->GetInterface(I_LOGMAN    , ALLARENAS);
                aman    = mm->GetInterface(I_ARENAMAN  , ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                attrman = mm->GetInterface(I_ATTRMAN   , ALLARENAS);

                if (!cfg || !lm || !aman || !pd || !attrman)
                {
                        printf("<handicap> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(ArenaData));

                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }
                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                aman->FreeArenaData(arenaKey);

                ReleaseInterfaces();
                return MM_OK;
        }
        else if (action == MM_ATTACH)
        {
                ArenaData* ad = GetArenaData(arena);
                
                attrman->Lock();
                ad->setter = attrman->RegisterSetter();
                attrman->UnLock();
                
                ad->active = true;
                
                mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
                mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, arena);
                
                UpdateAttribute(arena);
                
                return MM_OK;
        }
        else if (action == MM_DETACH)
        {
                ArenaData* ad = GetArenaData(arena);
                ad->active = false;
                
                mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, arena);
                mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, arena);
                
                attrman->Lock();
                attrman->UnregisterSetter(ad->setter);
                ad->setter = 0;
                attrman->UnLock();
                
                return MM_OK;
        }

        return MM_FAIL;
}
