#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "asss.h"

static Imodman     *mm;
static Icmdman     *cmd;
static Ichat       *chat;
static Iplayerdata *pd;

static int playerKey = -1;

// todo save the idiot status to disk

static helptext_t idiot_help = "Toggles idiot mode for the given player";
static void CIdiot(const char *command, const char *params, Player *p, const Target *target)
{
        if (target->type != T_PLAYER)
        {
                return;
        }
        
        bool *isIdiot = PPDATA(target->u.p, playerKey);
        *isIdiot = !*isIdiot;
        chat->SendCmdMessage(p, "Idiot mode has been turned %s for %s", *isIdiot ? "ON" : "OFF", target->u.p->name);
}

static const size_t c2sposition_weapon_offset = offsetof(struct C2SPosition, weapon);
int EditIndividualPPK(Player *from, Player *to, struct C2SPosition *pos, int *extralen)
{
        bool from_isIdiot = * (bool*) PPDATA(from, playerKey);
        bool to_isIdiot = * (bool*) PPDATA(to, playerKey);
        
        // idiots can not send weapons
        if (!from_isIdiot) {
                return false;
        }
        
        // idiots only see weapons from other idiots
        if (from_isIdiot && to_isIdiot) {
                return false;
        }
        
        pos->status = STATUS_SAFEZONE;
        *extralen = 0;
        memset( ((char*) pos) + c2sposition_weapon_offset, 0, sizeof(struct C2SPosition) - c2sposition_weapon_offset);
        return true; // modified
}

static Appk my_ppk_adviser = {
        ADVISER_HEAD_INIT(A_PPK)
        NULL,
        EditIndividualPPK
};

EXPORT const char info_idiot[] = "idiot (" ASSSVERSION ", " BUILDDATE ")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(pd     );
        mm->ReleaseInterface(cmd    );
        mm->ReleaseInterface(chat   );
}

EXPORT int MM_idiot(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;
                
                pd      = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
                cmd     = mm->GetInterface(I_CMDMAN    , ALLARENAS);
                chat    = mm->GetInterface(I_CHAT      , ALLARENAS);

                if (!cmd || !chat)
                {
                        printf("<idiot> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }
                
                playerKey = pd->AllocatePlayerData(sizeof(bool));
                
                if (playerKey == -1) // check if the player data buffer is full
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}
                
                cmd->AddCommand("idiot", CIdiot, ALLARENAS, idiot_help);
                mm->RegAdviser(&my_ppk_adviser, ALLARENAS);
                
                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                cmd->RemoveCommand("idiot", CIdiot, ALLARENAS);
                mm->UnregAdviser(&my_ppk_adviser, ALLARENAS);
                
                pd->FreePlayerData(playerKey);
        
                ReleaseInterfaces();
                return MM_OK;
        }
        
        return MM_FAIL;
}
