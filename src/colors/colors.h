#ifndef __COLORS_H
#define __COLORS_H

#define I_COLOR "colors-1"

//2 blue 1 macro 4 bluefreq 76 ?
//3 yellow
//5 greenspace in/out 6 green/pink 0 green 7 remote green
//8 mod warn red 10 staff chat red
//9 orange
//33 1st grey
//79 pink

//the color constants
typedef enum
{
	COLOR_BLUE		=2,
	COLOR_YELLOW		=3,
	COLOR_GREEN		=5,
	COLOR_RED		=8,
	COLOR_ORANGE		=9,
	COLOR_GRAY		=33,
	COLOR_PINK		=79
} ColorCode;

typedef struct Icolor
{
	INTERFACE_HEAD_DECL

	//send colored message to the zone, an arena, a freq, or a player
	void (*ColorZ)(ColorCode color, const char *format, ...);
	void (*ColorA)(ColorCode color, Arena *arena, const char *format, ...);
	void (*ColorF)(ColorCode color, Arena *a, int freq, const char *format, ...);
	void (*ColorP)(ColorCode color, Player *p, const char *format, ...);
	
} Icolor;

#endif
