Description:
============
This module adds commands like /?pink <message> which will then output the message in the given color.  Both public and private commands are supported.

Install:
========
1. In <asss\>/conf/modules.conf add:

```ini
colors:colors
```

2. In <asss\>/conf/global.conf add/modify:
Because these commands can be used to impersonate another user. Add logging to everything but ?pink.

```ini
log_staff:commands = blue yellow green red orange gray
```

3.  In <asss\>/conf/asss/dist/conf/groupdef.dir/<level\> add:

```ini
cmd_blue
privcmd_blue

cmd_yellow
privcmd_yellow

cmd_green
privcmd_green

cmd_red
privcmd_red

cmd_orange
privcmd_orange

cmd_gray
privcmd_gray

cmd_pink
privcmd_pink
```


Previous notes:
===============
from Cheese (original author): https://forums.minegoboom.com/viewtopic.php?t=8384&postdays=0&postorder=asc&start=0

I have made a module
which allows anyone to send an arena message or zone message
in any color capable by continuum,
both can be either signed or anonymous.
Powers are limited by groupdef.
Source attached.

***NEW***
You can now create modules which can send colored player or arena messages!
Simply use the interface and insert color->Pink(p,"bleh") into your code!

***NEWER***
The colors module has now become 3 modules in 1!
The core of the module has changed, using less memory and all that junk n stuff.
Also, the interface has been greatly extended, allowing you to send any color to the zone, an arena, or a player.
Additionally, you can now use the ?say command, just like the old *say in subgame, and !say in mervbot (kind of), to make any player, real or fake, say whatever you want them to! (I would suggest limiting these powers by groupdef to only those who you can -truly- trust)
Also, a ?voice command has been added to change your boring old blue pub chats into a more fun color, like pink or gray!
(Disclaimer: currently an echo due to technical limitations)

***NEWEST***
The core of the module and ?say has been streamlined.
Additionally, the interface has gained freq color functionality!

***FINAL***
The core of the module has undergone a final redesign, achieving perfection.
This includes code optimization, in which the number of lines was literally cut in half.
Also, an unloading bug has been fixed.
Additionally, the interface has been changed, and is now easier to use.

***Update***
Have gone through code and cleaned a lot of things up, and updated everything to be compatible with ASSS 1.6.0
Also removed voice and xchat modules, since they are cruft and require the not yet existing chat advisor.
Seperated the modules into different files for those that only want certain parts.

lines for modules.conf:

colors:colors


lines for groupdef:

cmd_blue
privcmd_blue

cmd_yellow
privcmd_yellow

cmd_green
privcmd_green

cmd_red
privcmd_red

cmd_orange
privcmd_orange

cmd_gray
privcmd_gray

cmd_pink
privcmd_pink

Update:
=======
from X-Demo:  
	customized / cleaned up code
	added private command support
	disabled say
	added logging config


lines for global.conf: (notably pink is not logged/reported)

log_staff:commands = blue yellow green red orange gray