//Colors Module
//By Cheese
//2021-08-17: customizations/fixes by X-Demo

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <strings.h>

#include "asss.h"
#include "colors.h"

local Imodman *mm;
local Ilogman *lm;
local Ichat *chat;
local Icmdman *cmd;
local Iplayerdata *pd;

#ifdef WIN32
#define strcasecmp stricmp
#endif

typedef enum
{
	MODE_PLAYER=1,
	MODE_ARENA,
	MODE_FREQ,
	MODE_ZONE
} TargetMode;

void send_msg(Arena *a, int freq, Player *p, int sound, ColorCode color, TargetMode mode, bool anon, Player *from, Player *nosend, const char *msg)
{
	char message[256];
	if(anon == TRUE) snprintf(message,sizeof(message),"%s",msg);
	else snprintf(message,sizeof(message),"%s -%s",msg,p->name);

	LinkedList playerlist;
	LLInit(&playerlist);
	
	if(mode == MODE_PLAYER) LLAdd(&playerlist,p);
	else
	{
		pd->Lock();
		Player *pp;
		Link *link;
		FOR_EACH_PLAYER(pp)
		{
			if(mode == MODE_ZONE) LLAdd(&playerlist,pp);
			else if((mode == MODE_ARENA) && (pp->arena == a)) LLAdd(&playerlist,pp);
			else if((mode == MODE_FREQ) && (pp->arena == a) && (pp->pkt.freq == freq)) LLAdd(&playerlist,pp);
		}
		pd->Unlock();
	}
	
	if(nosend) LLRemove(&playerlist,nosend); //no send to sender
	chat->SendAnyMessage(&playerlist,color,sound,from,message);
	LLEmpty(&playerlist);
}

local helptext_t cmd_color=
"Targets: player or none\n"
"Args: none\n"
"Allows user to send an arena message in any color desired.\n";

local void color(const char *cmd, const char *params, Player *p, const Target *t)
{
	//default target arena
	Player *target = p;
	TargetMode mode = MODE_ARENA;
	bool anon = TRUE;

	//set target if valid (private command)
	if (t->type == T_PLAYER) {
		target = t->u.p;
		mode = MODE_PLAYER;
	}

	//default color
	ColorCode color = COLOR_BLUE;

	//set color
	if(!strcasecmp(cmd,"blue")) color = COLOR_BLUE;
	else if(!strcasecmp(cmd,"yellow")) color = COLOR_YELLOW;
	else if(!strcasecmp(cmd,"green")) color = COLOR_GREEN;
	else if(!strcasecmp(cmd,"red")) color = COLOR_RED;
	else if(!strcasecmp(cmd,"orange")) color = COLOR_ORANGE;
	else if(!strcasecmp(cmd,"gray")) color = COLOR_GRAY;
	else if(!strcasecmp(cmd,"pink")) color = COLOR_PINK;

	//send message
	send_msg(target->arena,-1,target,0,color,mode,anon,NULL,NULL,params);
}

local void ColorZ(ColorCode color, const char *format, ...)
{
	char msg[256];
	va_list args;
	va_start(args,format);
	vsnprintf(msg,sizeof(msg),format,args);
	send_msg(NULL,-1,NULL,0,color,MODE_ZONE,TRUE,NULL,NULL,msg);
	va_end(args);
}

local void ColorA(ColorCode color, Arena *a, const char *format, ...)
{
	char msg[256];
	va_list args;
	va_start(args,format);
	vsnprintf(msg,sizeof(msg),format,args);
	send_msg(a,-1,NULL,0,color,MODE_ARENA,TRUE,NULL,NULL,msg);
	va_end(args);
}

local void ColorF(ColorCode color, Arena *a, int freq, const char *format, ...)
{
	char msg[256];
	va_list args;
	va_start(args,format);
	vsnprintf(msg,sizeof(msg),format,args);
	send_msg(a,freq,NULL,0,color,MODE_FREQ,TRUE,NULL,NULL,msg);
	va_end(args);
}

local void ColorP(ColorCode color, Player *p, const char *format, ...)
{
	char msg[256];
	va_list args;
	va_start(args,format);
	vsnprintf(msg,sizeof(msg),format,args);
	send_msg(NULL,-1,p,0,color,MODE_PLAYER,TRUE,NULL,NULL,msg);
	va_end(args);
}

local Icolor colorint=
{
	INTERFACE_HEAD_INIT(I_COLOR,"color")
	ColorZ, ColorA, ColorF, ColorP
};

EXPORT const char info_colors[]=
".\n"
"Colors Module\n"
"by Cheese with customizations by X-Demo\n"
"Allows user to send colorful arena and zone messages.";

EXPORT int MM_colors(int action, Imodman *mm2, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm=mm2;
		chat=mm->GetInterface(I_CHAT,ALLARENAS);
		cmd=mm->GetInterface(I_CMDMAN,ALLARENAS);
		pd=mm->GetInterface(I_PLAYERDATA,ALLARENAS);
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		if(!chat || !cmd || !pd || !lm) return MM_FAIL;

		cmd->AddCommand("blue",color,ALLARENAS,cmd_color);
		cmd->AddCommand("yellow",color,ALLARENAS,cmd_color);
		cmd->AddCommand("green",color,ALLARENAS,cmd_color);
		cmd->AddCommand("red",color,ALLARENAS,cmd_color);
		cmd->AddCommand("orange",color,ALLARENAS,cmd_color);
		cmd->AddCommand("gray",color,ALLARENAS,cmd_color);
		cmd->AddCommand("pink",color,ALLARENAS,cmd_color);

		mm->RegInterface(&colorint,ALLARENAS);

		lm->Log(L_ERROR,"<colors> Colors Module has loaded.");
		
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"colors",arena,"Colors Module has attached to the arena.");

		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"colors",arena,"Colors Module has detached from the arena.");

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<colors> Colors Module has unloaded.");

		if(mm->UnregInterface(&colorint,ALLARENAS)) return MM_FAIL;
		
		cmd->RemoveCommand("blue",color,ALLARENAS);
		cmd->RemoveCommand("yellow",color,ALLARENAS);
		cmd->RemoveCommand("green",color,ALLARENAS);
		cmd->RemoveCommand("red",color,ALLARENAS);
		cmd->RemoveCommand("orange",color,ALLARENAS);
		cmd->RemoveCommand("gray",color,ALLARENAS);
		cmd->RemoveCommand("pink",color,ALLARENAS);
		
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}
