 /**************************************************************
 * Event: Standard Events
 *
 * The Standard Events Module handles everything pertaining
 * to the following six events:
 *         - Elim
 *         - Team Elim
 *         - Killrace
 *         - Team Killrace
 *         - Conquer
 *         - Zombies
 *
 * Originally a Mervbot Module created by either SOS or XDOOM,
 * then partially ported by Hallowed be thy name, and ultimately
 * remade by Hakaku.
 *
 **************************************************************/

/* TODO: LOCKARENA, CONQUER STARS, MISSING ABORT */

#include "asss.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

/* Player data */
typedef struct Pdata
{
	int kills;        //the number of kills a player has acquired
	int lives;        //the number of lives a player has
	int tlives;       //the total number of deaths a team has accumulated (per-player: lagout-proof)
} Pdata;

local int playerKey;

/* Arena data */
typedef struct Adata
{
	/* All events */
	int eventid;       //1 = elim, 2 = teamelim, 3 = killrace, 4 = teamkillrace, 5 = conquer, 6 = zombies
	int lockships;     //0 = no, 1 = ships are restricted
	int started;       //0 = no game, 1 = pending, 2 = started
	int defaultship;   //this is the default ship if someone happens to change to a restricted ship
	
	int obj;           //the objective to attain (e.g. number of kills, of deaths)
	
	/* Specific to Zombies event */
	int gship;         //the zombie's ship
	int glockship;     //0 = no, 1 = zombie's ship is restricted
	Player *zombie;    //the zombie
} Adata;

local int arenaKey;

/* Interfaces */
local Imodman *mm;
local Iarenaman *aman;
local Icapman *capman;
local Ichat *chat;
local Icmdman *cmd;
local Igame *game;
local Imainloop *ml;
local Iplayerdata *pd;

#define ZOMBIE_FREQ 999
#define HUMAN_FREQ 0
local int allships[7];

/************************************************************************/
/*                              Prototypes                              */
/************************************************************************/

//interface functions
local char* getOption(const char *string, char param);
local void OnePlayerFreqs(Arena *arena);
local void MultiPlayerFreqs(Arena *arena, int teamsof);
local void TMultiPlayerFreqs(Arena *arena, int teams);
Player* RandomPlayer(Arena *arena);
local int GetPlayerCount(Arena *arena);

//game functions
local void Abort(Arena *arena, Player *host, int debug);
local void Begin(Player *host, Arena *arena, const char *params);
local void LegalShip(int ship, Arena *arena);
local void CheckLegalShip(Arena *arena);
local void Stop(Arena* arena);


/************************************************************************/
/*                          Interface Functions                         */
/************************************************************************/

/* Retrieve the content in optional parameters such as -o(...) */
local char* getOption(const char *string, char param)
{
	if (!param)
		return NULL;
	
	char search[4] = "-d(\0";
	search[1] = param;

	char *buf = strstr(string,search);
	if (buf)
	{
		char *result = calloc(64, sizeof(char));
		int i = 3, j = 0;
		for(; ((i < 67) && (j == 0)); i++)
		{
			if (buf[i] != ')')
			{
				result[i-3] = buf[i];
			}
			else
			{
				j = 1;
			}
		}
		
		result[i-2] = '\0';
		return result;
	}
	else
		return "";
}

/* Put players on single teams */
local void OnePlayerFreqs(Arena *arena)
{
	int freq = 10;
	Player *p;
	Link *link;

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
		{
			game->SetFreq(p, 0);
		}
	}
	pd->Unlock();

	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
		{
			game->SetFreq(p, freq);
			Target t;
			t.type = T_PLAYER;
			t.u.p = p;
			game->GivePrize(&t, 13, 1);

			freq++;
		}
	}
	pd->Unlock();
}

/* Arrange players into teams of a specified number */
local void MultiPlayerFreqs(Arena *arena, int teamsof)
{
    int freq = 10;
    Player *p;
    Link *link;

    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {
            game->SetFreq(p, 0);
        }
    }
    pd->Unlock();
    
    int count = 0;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {
            if (count < teamsof)
            {
                game->SetFreq(p, freq);
                Target t;
                t.type = T_PLAYER;
                t.u.p = p;
                game->GivePrize(&t, 13, 1);
                
                count++;
            }
            else if (count == teamsof)
            {
                freq ++;
                
                game->SetFreq(p, freq);
                Target t;
                t.type = T_PLAYER;
                t.u.p = p;
                game->GivePrize(&t, 13, 1);
                count = 1;
            }
        }
    }
    pd->Unlock();
}

/* Arrange players into a set number of teams */
local void TMultiPlayerFreqs(Arena *arena, int teams)
{
    int freq = 10;
    Player *p;
    Link *link;

    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {
            game->SetFreq(p, 0);
        }
    }
    pd->Unlock();

    int count = 0;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {
            if (count == teams)
            {
                count = 0;
            }
            if (count < teams)
            {
                game->SetFreq(p, freq + count);
                Target t;
                t.type = T_PLAYER;
                t.u.p = p;
                game->GivePrize(&t, 13, 1);

                count++;
            }
        }
    }
    pd->Unlock();
}

/* Select a random player */
Player* RandomPlayer(Arena *arena)
{
	int count = 0;
	Player *p;
	Link *link;
	pd->Lock();
    FOR_EACH_PLAYER(p)
	{
		if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
			count++;
	}
	pd->Unlock();
	
	srand(time(NULL));
	int selection = rand() % count;
	int i = 0;
	
	Player *selected;
	
	pd->Lock();
    FOR_EACH_PLAYER(p)
	{
		if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && !selected) {
			if (selection == i) {
				selected = p;
			}
			else
				i++;
		}
	}
	pd->Unlock();
	
	return selected;
}

/* Count the number of players who are neither spectators nor fake players within an arena */
local int GetPlayerCount(Arena *arena)
{
    int i = 0;
    Player *p; Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p->type != T_FAKE))
        {
            i++;
        }
    }
    pd->Unlock();
    return i;
}


/************************************************************************/
/*                            Game Functions                            */
/************************************************************************/

/* Stop and end the current game */
local void Stop(Arena *arena)
{
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	
	Player *p;
	Link *link;
	
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if (p->arena == arena)
		{
			Pdata *pdata = PPDATA(p, playerKey);
			
			//reset player values
			pdata->kills = 0;
			pdata->lives = 0;
			pdata->tlives = 0;
		}
	}
	pd->Unlock();
	
	//reset arena values
	adata->eventid = 0;
	adata->lockships = 0;
	adata->started = 0;
	adata->defaultship = 0;
	adata->gship = 0;
	
	adata->obj = 0;
	
	adata->glockship = 0;
	adata->zombie = NULL;
}

/* Abort the current game */
local void Abort(Arena *arena, Player *host, int debug)
{
	Stop(arena);
	chat->SendMessage(host, "Game aborted: Invalid syntax. Please type '?start' for more help.");
}

#include "teamelim.c"
#include "elim.c"
#include "teamkillrace.c"
#include "killrace.c"
#include "conquer.c"
#include "zombies.c"

local void Begin(Player *host, Arena *arena, const char *params)
{
	if (strstr(params, "teamelim") != NULL)
	{
		Start_TeamElim(host, arena, params);
	}
	else if (strstr(params, "elim") != NULL)
	{
		Start_Elim(host, arena, params);
	}
	else if (strstr(params, "teamkillrace") != NULL)
	{
		Start_TeamKillRace(host, arena, params);
	}
	else if (strstr(params, "killrace") != NULL)
	{
		Start_KillRace(host, arena, params);
	}
	else if (strstr(params, "conquer") != NULL)
	{
		Start_Conquer(host, arena, params);
	}
	else if (strstr(params, "zombies") != NULL)
	{
		Start_Zombies(host, arena, params);
	}
}

/* Set as legal ship  */
local void LegalShip(int ship, Arena *arena)
{
	allships[ship] = 1;
}

/* Check if the player is in a legal ship */
local void CheckLegalShip(Arena *arena)
{
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	
	/* Check the legal ships, and if zombies, check the zombie ship */
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if ((g->arena == arena) && g->p_ship != SHIP_SPEC)
		{
			if (adata->lockships && g->p_freq != ZOMBIE_FREQ) {
				int i, legal = 0;
				for (i = 0; i < 8; i++)
				{
					if ((g->p_ship == i) && (allships[i] == 1))
						legal = 1;
				}
				if (!legal)
					game->SetShip(g, adata->defaultship);
			}
			else if (adata->glockship && g->p_freq == ZOMBIE_FREQ) {
				if (g->p_ship != adata->gship) {
					game->SetShip(g, adata->gship);
				}
			}
		}
	}
	pd->Unlock();
}

/************************************************************************/
/*                           Player Commands                            */
/************************************************************************/

/* ?host help information */
local helptext_t host_help =
"Targets: none\n"
"Args: request, none, event\n"
"By default, ?host <msg> will send a message to any online moderator requesting\n"
"that an event be hosted. If the player using this command is a staff member,\n"
"and if no parameters are given, ?host will list the available events in that arena.\n"
"If an event is specified (e.g. ?host elim), it will attempt to start that event.\n";

/* ?host (staff message, event list, start an event) */
local void cHost(const char *command, const char *params, Player *p, const Target *target)
{
	/* Send a host message if the player isn't a staff member. */
	if (!capman->HasCapability(p,CAP_IS_STAFF))
	{
		if (IS_ALLOWED(chat->GetPlayerChatMask(p), MSG_MODCHAT))
		{
			Arena *arena = p->arena;
			if (params)
			{
				chat->SendModMessage("(Host) {%s} %s: %s", arena->name, p->name, params);
		   		chat->SendMessage(p, "Message has been sent to online staff");
			}
			else
			{
				chat->SendMessage(p, "Invalid syntax. Please use ?host <arena/event> to request an event be hosted.");
			}
			return;
		}
	}

	/* Host list or an event if the player is a staff member. */
	if (!isalpha(params[0]) && params[0] != '-')
	{
		chat->SendMessage(p, "---------------------------------------------------------------------------------------"); //Event ID
		chat->SendMessage(p, "| elim         | Brawl your way to victory: last man standing wins! | -d(#)           |"); //1
		chat->SendMessage(p, "| teamelim     | Grab a partner and eliminate all opponents.        | -d(#), -t(#)    |"); //2
		chat->SendMessage(p, "| killrace     | Kill as many as fast as you can!                   | -k(#)           |"); //3
		chat->SendMessage(p, "| teamkillrace | Grab a partner and fire away!                      | -k(#), -t(#)    |"); //4
		chat->SendMessage(p, "| conquer      | Conquer or be conquered.                           |                 |"); //5
		chat->SendMessage(p, "| zombies      | Braaaaaiiiinnnssss!                                | -z(name), -g(#) |"); //6
		chat->SendMessage(p, "---------------------------------------------------------------------------------------");
		chat->SendMessage(p, "Parameters: s = ships, d = deaths, t = players per team, k = kills,");
		chat->SendMessage(p, "            z = initial zombie, g = zombie ship");
		chat->SendMessage(p, "Example: ?start zombies -z(tm_master) -s(1,2,3) -g(3)");
	}
	else
	{
		Begin(p, p->arena, params);
	}
}

/* ?stopevent help information */
local helptext_t stop_help =
"Targets: none\n"
"Args: none\n"
"Using ?stop or ?stopevent will end the game or event currently active in the arena.\n";

/* ?stopevent */
local void cStopEvent(const char *command, const char *params, Player *p, const Target *target)
{
	Adata *adata = P_ARENA_DATA(p->arena, arenaKey);
	if (adata->started != 0)
	{
		switch(adata->eventid)
        {
            case 1: elimStop(p->arena); break;
            case 2: teamelimStop(p->arena); break;
            case 3: killraceStop(p->arena); break;
            case 4: teamkillraceStop(p->arena); break;
            case 5: conquerStop(p->arena); break;
            case 6: zombieStop(p->arena); break;
        }
		chat->SendArenaSoundMessage(p->arena, 1, "Game stopped by moderator.");
	}
	else
	{
		chat->SendMessage(p, "There does not appear to be a game going on here.");
	}
}

/* ?rules help information */
local helptext_t rules_help =
"Targets: none\n"
"Args: none\n"
"If a game is currently in play, this command will display the rules.\n";

/* Show the rules of the game to the player. */
local void cRules(const char *command, const char *params, Player *p, const Target *target)
{
    Adata *adata = P_ARENA_DATA(p->arena, arenaKey);
    if (adata->started)
    {
        switch(adata->eventid)
        {
            case 1: chat->SendMessage(p, "Elim: Take up arms and try to eliminate all your enemies before they eliminate you!"); break;
            case 2: chat->SendMessage(p, "TeamElim: Partner up and eliminate all enemy teams! Lives are collectively shared."); break;
            case 3: chat->SendMessage(p, "KillRace: Choose a ship and fire as much as possible to crush as many enemies as fast as possible."); break;
            case 4: chat->SendMessage(p, "TeamKillRace: Grab a partner and destroy your enemies as fast as possible. Kills are collectively added."); break;
            case 5: chat->SendMessage(p, "Conquer: Fire away and assimilate your opponents. Kill an enemy captain and their whole team will be yours!"); break;
            case 6: chat->SendMessage(p, "Zombies: Braaaainnnss! One zombie is selected to propagate the virus and multiply in numbers. Last human survivor wins."); break;
        }
    }
    else
    {
        chat->SendMessage(p, "There does not appear to be a game going on here.");
    }
}

/************************************************************************/
/*                            Module Init                               */
/************************************************************************/

/* Module information (?modinfo) */
EXPORT const char info_standard[] = "(Devastation) v0.1 Hakaku";

/* The entry point: */
EXPORT int MM_standard(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		
		aman = mm->GetInterface(I_ARENAMAN, arena);
		capman = mm->GetInterface(I_CAPMAN, arena);
		chat = mm->GetInterface(I_CHAT, arena);
		cmd = mm->GetInterface(I_CMDMAN, arena);
		game = mm->GetInterface(I_GAME, arena);
		ml = mm->GetInterface(I_MAINLOOP, arena);
		pd = mm->GetInterface(I_PLAYERDATA, arena);

		if (!aman || !chat || !cmd || !game || !ml || !pd)
		{
			mm->ReleaseInterface(pd);
			mm->ReleaseInterface(ml);
			mm->ReleaseInterface(game);
			mm->ReleaseInterface(cmd);
			mm->ReleaseInterface(chat);
			mm->ReleaseInterface(capman);
			mm->ReleaseInterface(aman);
			return MM_FAIL;
		}
		
		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(capman);
		mm->ReleaseInterface(aman);
		
		return MM_OK;
	}
	else if (action == MM_ATTACH)
	{
		playerKey = pd->AllocatePlayerData(sizeof(Pdata));
		arenaKey  = aman->AllocateArenaData(sizeof(Adata));

		if (!playerKey  || !arenaKey)
			return MM_FAIL;

		cmd->AddCommand("host", cHost, arena, host_help);
		cmd->AddCommand("start", cHost, arena, host_help);
		cmd->AddCommand("stopevent", cStopEvent, arena, stop_help);
		cmd->AddCommand("stop", cStopEvent, arena, stop_help);
		cmd->AddCommand("rules", cRules, arena, rules_help);
		return MM_OK;
	}
	else if (action == MM_DETACH)
	{
		pd->FreePlayerData(playerKey);
		aman->FreeArenaData(arenaKey);
	
		cmd->RemoveCommand("rules", cRules, arena);
		cmd->RemoveCommand("stop", cStopEvent, arena);
		cmd->RemoveCommand("stopevent", cStopEvent, arena);
		cmd->RemoveCommand("start", cHost, arena);
		cmd->RemoveCommand("host", cHost, arena);
		return MM_OK;
	}
	return MM_FAIL;
}
