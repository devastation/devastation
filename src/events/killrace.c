local void killraceStop(Arena* arena);
local int TimeUp_KillRace(void *p);

/* Check Killrace win conditions (only killer left) */
local void killraceLCheck(Arena *arena, Player *pe)
{
    int i = 0;
    Player *p , *winner = NULL;
    Link *link;
	
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p!=pe))
        {
            winner = p;
            i++;
        }
    }
    pd->Unlock();
	
    if (i == 1)
    {
        chat->SendArenaSoundMessage(arena, 5, "Game Over! This round's winner is %s.", winner->name);
        killraceStop(arena);
    }
}

//Callback: ShipFreqChange
local void killraceShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        killraceLCheck(p->arena, p);
    }
}

//Callback: PlayerAction
local void killracePlayerAction(Player *p, int action, Arena *arena)
{
   if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
        killraceLCheck(arena, p);
}

/* Check killrace win conditions (fastest killer) */
local void killraceCheck(Player *p)
{
    Pdata *pdata = PPDATA(p, playerKey);
	Adata *adata = P_ARENA_DATA(p->arena, arenaKey);

    if (pdata->kills == adata->obj)
    {
        chat->SendArenaSoundMessage(p->arena, 5, "Game Over! This round's winner is %s.", p->name);
        killraceStop(p->arena);
    }
}

//Callback: Kill
local void killraceKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    Pdata *data = PPDATA(k, playerKey);
    data->kills++;

    killraceCheck(k);
}

/* Start the game and check parameters */
local void Start_KillRace(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 3; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options */
	char *next, *string;
	
	//kills
	int kills;
	string = getOption(params, 'k');

	if (string != next)
	{
		kills = atoi(string);
		if ((kills <= 50) && (kills > 0))
			adata->obj = kills;
		else
			adata->obj = 10;
	}
	else
	{
		adata->obj = 10;
	}

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}


	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "KillRace will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_KillRace, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_KillRace(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset kills

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	OnePlayerFreqs(host->arena); //put everyone on single teams
	
	chat->SendArenaSoundMessage(host->arena, 104, "KillRace started, you'll win after %i %s.", adata->obj, adata->obj == 1 ? "kill" : "kills");

    mm->RegCallback(CB_KILL, killraceKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, killracePlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, killraceShipFreqChange, host->arena);
	return 0;
}

/* Abort the current game */
local void killraceStop(Arena* arena)
{
    ml->ClearTimer(TimeUp_KillRace, arena);
    mm->UnregCallback(CB_KILL, killraceKill, arena);
    mm->UnregCallback(CB_PLAYERACTION, killracePlayerAction, arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, killraceShipFreqChange, arena);

    Stop(arena);
}
