#	Hockey for ASSS version 1.4.4
#	Created By: resoL
#	08-02-2011
#	TODO: set up ref vs host functions and modify commands.

from asss import *
import random, time

drop=51
buzz=52
goal0=111
goal1=112

### A list of IDs to use for Team Zero###
idz=[1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,
	1010,1011,1012,1013,1014,1015,1016,1017,1018,1019]
### A list of IDs to use for Team One###
ido=[1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,
	1110,1111,1112,1113,1114,1115,1116,1117,1118,1119]
ids=[idz, ido]

balls = get_interface(I_BALLS)
mapdata = get_interface(I_MAPDATA)
objs = get_interface(I_OBJECTS)
chat = get_interface(I_CHAT)
game = get_interface(I_GAME)

class player_data():
	def __init__(self):
		self.steals=0
		self.passes=0
		self.checks=0
		self.rapid_kill=0
		self.penalty=0
		self.puck_time=0
		self.total_puck_time=0
		self.hats=0
		self.assists=0
		self.goals=0
		self.saves=0

# vote

# allows players to say any positive number into pub chat and it will get
# recorded as a vote.
def vote_chatmsg(p, mtype, sound, to, freq, text):
    if mtype == MSG_PUB:
        value = 0
        try:
            value = int(text)
        except:
            pass

        if value > 0:
            p.vote_value = value

def vote_make_timer(arena, maxoption, timelimit, callback, freq):
    def vote_timer():
        # stop allowing votes
        arena.vote_ref1 = None
        arena.vote_ref2 = None

        # initialise the array
        sums = []
        for i in range(maxoption):
            sums.append(0)

        # count the votes
        def cb_count_votes(p):
            if p.arena == arena and \
                p.vote_value > 0 and \
                p.vote_value <= maxoption:
                sums[p.vote_value - 1] += 1
        for_each_player(cb_count_votes)

        # find the most popular vote
        besti = -1
        best = 0
        for i in range(maxoption):
            if sums[i] > best:
                best = sums[i]
                besti = i

        callback(arena, best, besti + 1, freq)
        #arena.vote_ref3 = None
        return 0
    return set_timer(vote_timer, timelimit, 1500)

def vote_paction(p, action, arena):
    if action == PA_ENTERARENA:
        p.vote_value = 0

def vote_start(arena, question, maxoption, timelimit, callback, freq):
    # reset all votes for this arena
    def cb_reset_votes(p):
        if p.arena == arena:
            p.vote_value = 0
    for_each_player(cb_reset_votes)

    chat.SendArenaMessage(arena, "%s" % question)
    arena.vote_ref1 = reg_callback(CB_CHATMSG, vote_chatmsg, arena)
    arena.vote_ref2 = reg_callback(CB_PLAYERACTION, vote_paction, arena)
    arena.vote_ref3 = vote_make_timer(arena, maxoption, timelimit, callback, freq)

# stops the voting process
def vote_stop(arena):
    arena.vote_ref1 = None
    arena.vote_ref2 = None
    arena.vote_ref3 = None

def make_killpuck_timer(initial, interval, arena):
	def timer():
		balls.SetBallCount (arena, 0)
		return 0
	return set_timer(timer, initial, interval)

def hat_trick(arena, p):
	arena.playerDB[p.name].hats+=1
	arena.just_got_hat = p.name

def hat_trick_check(arena, p):
	if arena.playerDB[p.name].goals == 9:
		hat_trick(arena, p)
		chat.SendArenaSoundMessage (arena, 19, "[ %s ] - Triple Hat Trick!" % (p.name))
	elif arena.playerDB[p.name].goals == 6:
		hat_trick(arena, p)
		chat.SendArenaSoundMessage (arena, 19, "[ %s ] - Double Hat Trick!" % (p.name))
	elif arena.playerDB[p.name].goals == 3:
		hat_trick(arena, p)
		chat.SendArenaSoundMessage (arena, 19, "[ %s ] - Hat Trick!" % (p.name))

def assist_check(arena, p):
	assistant = arena.is_assistant
	if not assistant in arena.playerDB:
		arena.playerDB[assistant]=player_data()
	arena.playerDB[assistant].assists+=1

def make_puck_timer(arena, p, time):
	def puck_timer():
		if arena.is_assistant == None:
			chat.SendArenaSoundMessage (arena, 104, "[ %s ] Scores for Team %s!" % (p.name, p.freq))
			hat_trick_check(arena, p)
		else:
			chat.SendArenaSoundMessage (arena, 104, "[ %s ] Scores for Team %s!" % (p.name, p.freq))
			chat.SendArenaSoundMessage (arena, 104, "[ %s ] With the assist!" % (arena.is_assistant))
			hat_trick_check(arena, p)
			assist_check(arena, p)
			arena.is_assistant = None
	return set_timer(puck_timer, time, 0)

def goal(arena, p, bid, x, y):
	if arena.game_is_on:
		arena.made_goal = p.name
		arena.playerDB[p.name].goals+=1
		arena.hockey_pucktimer = make_puck_timer(arena, p, 10)
		objs.Toggle(arena, ids[p.freq][arena.score[p.freq]], 0)
		arena.sb = make_sb_timer(1610, 100, arena, p.freq)
		arena.killpuck = make_killpuck_timer(100, 100, arena)
		start_votes(arena, p.freq)
		if p.freq == 0:
			objs.Toggle(arena, goal0, 1)
		elif p.freq == 1:
			objs.Toggle(arena, goal1, 1)

hockey_goal_options = [ "Clean", "No Count" ]

def show_stats(arena):
	chat.SendArenaMessage (arena, "|--------------------------------------------------------------|")
	chat.SendArenaMessage (arena, "|     Player Name     | G | PA | ST | C | A | H | S | P | TPTS |")
	chat.SendArenaMessage (arena, "|---------------------+---+----+----+---+---+---+---+---+------|")
	for pname in arena.playerDB:
		stats=arena.playerDB[pname]
		chat.SendArenaMessage (arena, "|%20s |%2s |%3s |%3s |%2s |%2s |%2s |%2s |%2s |%5s |" % (pname, stats.goals, stats.passes, stats.steals, stats.checks, stats.assists, stats.hats, stats.saves, stats.penalty, round(stats.total_puck_time, 1)))
	chat.SendArenaMessage (arena, "|---------------------+---+----+----+---+---+---+---+---+------|")
	chat.SendArenaMessage (arena, "| G = Goals, PA = Passes, ST = Steals, C = Checks, A = Assists |")
	chat.SendArenaMessage (arena, "| H = Hat Tricks, S = Saves, P = Penalties                     |")
	chat.SendArenaMessage (arena, "| TPTS = Total Puck Time Seconds                               |")
	chat.SendArenaMessage (arena, "|--------------------------------------------------------------|")

def end_game(arena):
	if arena.score[0] > arena.score[1]:
		chat.SendArenaSoundMessage (arena, 5, "NOTICE: Game Over!")
		chat.SendArenaMessage (arena, "Freq 0 Wins the game! [ %s to %s ]" % (arena.score[0], arena.score[1]))
		show_stats(arena)
	elif arena.score[0] < arena.score[1]:
		chat.SendArenaSoundMessage (arena, 5, "NOTICE: Game Over!")
		chat.SendArenaMessage (arena, "Freq 1 Wins the game! [ %s to %s ]" % (arena.score[0], arena.score[1]))
		show_stats(arena)
	elif arena.score[0] == arena.score[1]:
		chat.SendArenaSoundMessage (arena, 5, "NOTICE: Game Over!")
		chat.SendArenaMessage (arena, "Tie Game! There is no winner of this game.")
		show_stats(arena)

def check_score(arena):
	if (arena.score[0] > 6 or arena.score[1] > 6) and (arena.score[0] > arena.score[1] +1 or arena.score[1] > arena.score[0] +1):
		end_game(arena)
		turn_game_off(arena)
	elif arena.score[0] == arena.score[1]:
		chat.SendArenaSoundMessage (arena, 20, "Tied Game!")

def make_goals_timer(initial, interval, arena, freq):
	def timer():
		scored = arena.made_goal
		if arena.hockey_goal == 1:
			chat.SendArenaSoundMessage (arena, drop, "Goal Counted!")
			arena.score[freq]+=1
			chat.SendArenaSoundMessage (arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (arena.score[0], arena.score[1]))
			check_score(arena)
		elif arena.hockey_goal == 2:
			if scored == arena.just_got_hat:
				arena.playerDB[scored].hats-=1
			chat.SendArenaSoundMessage (arena, drop, "No Goal!")
			chat.SendArenaSoundMessage (arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (arena.score[0], arena.score[1]))
			arena.playerDB[scored].goals-=1
		return 0
	return set_timer(timer, initial, interval)

def vcb_goal(arena, votes, vid, freq):
	# default vote
	if vid == 0:
		vid = 1

	arena.hockey_goal = vid
	if arena.hockey_goal == 1:
		chat.SendArenaMessage(arena, "The goal was voted [ Clean ] (%d Votes)" % (votes))
		arena.goals = make_goals_timer(100, 100, arena, freq)
	if arena.hockey_goal == 2:
		chat.SendArenaMessage(arena, "The goal was voted [ NC ] (%d Votes)" % (votes))
		arena.goals = make_goals_timer(100, 100, arena, freq)

def start_votes(arena, freq):
    # make the message
    msg = "VOTE: Was that goal: "
    for i in range(len(hockey_goal_options)):
        msg += " %s=%d" % (hockey_goal_options[i], i + 1)
    vote_start(arena, "%s" % msg, len(hockey_goal_options), 1500, vcb_goal, freq)

    # mark something so we don't keep sending the 'when 2 players or more' message
    arena.hockey_goal = -1

def save(p, arena):
	if p.freq == 0:
		rgn = mapdata.FindRegionByName(arena, "cr1")
		if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
			chat.SendArenaMessage (arena, "Freq %s Save by: %s" % (p.freq, p.name))
			if not p.name in arena.playerDB:
				arena.playerDB[p.name]=player_data()
			arena.playerDB[p.name].saves+=1
	elif p.freq == 1:
		rgn = mapdata.FindRegionByName(arena, "cr0")
		if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
			chat.SendArenaMessage (arena, "Freq %s Save by: %s" % (p.freq, p.name))
			if not p.name in arena.playerDB:
				arena.playerDB[p.name]=player_data()
			arena.playerDB[p.name].saves+=1
			#chat.SendModMessage ("playerDB names: %s" % (arena.playerDB))
			#chat.SendModMessage ("playerDB stats: %s" % (arena.playerDB[p.name].saves))

def puck_time_start(arena, p):
	if not p.name in arena.playerDB:
		arena.playerDB[p.name]=player_data()
	arena.playerDB[p.name].puck_time=time.time()

def puck_time_stop(arena, p):
	elapsed = (time.time() - arena.playerDB[p.name].puck_time)
	arena.playerDB[p.name].total_puck_time+=elapsed

def ballpickup(arena, p, bid):
	if arena.game_is_on:
		arena.has_puck = p.name
		puck_time_start(arena, p)
		if arena.assistant_tracking[1] == p.freq and not arena.assistant_tracking[0] == p.name:
			arena.is_assistant = arena.assistant_tracking[0]
			passed = arena.assistant_tracking[0]
			arena.playerDB[passed].passes+=1
		if arena.assistant_tracking[0] == p.name:
			arena.is_assistant = None
		if not arena.assistant_tracking[1] == p.freq:
			arena.playerDB[p.name].steals+=1
			arena.is_assistant = None	
			if p.ship == SHIP_SHARK:
				arena.playerDB[p.name].steals-=1
				save(p, arena)
			elif p.ship == SHIP_LANCASTER:
				arena.playerDB[p.name].steals-=1
				save(p, arena)
def make_rapid_kill_timer(arena, killer, time):
	def rapid_kill_timer():
		arena.playerDB[killer.name].rapid_kill = 0
	return set_timer(rapid_kill_timer, time, 0)

def kill(arena, killer, killed, bty, p, pts, green):
	if not killer.name in arena.playerDB:
		arena.playerDB[killer.name]=player_data()
		arena.playerDB[killer.name].penalty+=1
	if killed == arena.has_puck:
		arena.puck_time_stop(arena, killed)
		arena.playerDB[killer.name].checks+=1
		arena.playerDB[killer.name].rapid_kill+=1
	if arena.playerDB[killer.name].rapid_kill > 2:
		arena.playerDB[killer.name].penalty+=1
		chat.SendModMessage ("Rapid kill detected from [ %s ] with [ %s ] Pentalties" % (killer.name, arena.playerDB[killer.name].penalty))
		arena.hockey_rapidkilltimer = make_rapid_kill_timer(arena, killer, 300)
	else:
		arena.playerDB[killer.name].checks+=1
		arena.playerDB[killer.name].rapid_kill+=1
		arena.hockey_rapidkilltimer = make_rapid_kill_timer(arena, killer, 300)

	return pts, green

def crease0(p, arena):
	rgn = mapdata.FindRegionByName(arena, "cr0") 
	if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
		arena.playerDB[p.name].penalty+=1
		chat.SendArenaSoundMessage (arena, buzz, "[ %s ] - Penalized for a Crease Violation and now has [ %s ] Penalties." % (p.name, arena.playerDB[p.name].penalty ))
		chat.SendArenaMessage (arena, "Goal not counted. Prepare for Faceoff!")
		balls.SetBallCount (arena, 0)

def crease1(p, arena):
	rgn = mapdata.FindRegionByName(arena, "cr1") 
	if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
		arena.playerDB[p.name].penalty+=1
		chat.SendArenaSoundMessage (arena, buzz, "[ %s ] - Penalized for a Crease Violation and now has [ %s ] Penalties." % (p.name, arena.playerDB[p.name].penalty ))
		chat.SendArenaMessage (arena, "Goal not counted. Prepare for Faceoff!")
		balls.SetBallCount (arena, 0)

def ballfire (arena, p, bid):
	if arena.game_is_on:
		arena.assistant_tracking = (p.name, p.freq)
		puck_time_stop(arena, p)
		if p.ship == SHIP_WARBIRD:
			if p.freq == 0:
				crease0(p, arena)
			elif p.freq == 1:
				crease1(p, arena)
		elif p.ship == SHIP_JAVELIN:
			if p.freq == 0:
				crease0(p, arena)
			elif p.freq == 1:
				crease1(p, arena)
		elif p.ship == SHIP_SPIDER:
			if p.freq == 0:
				crease0(p, arena)
			elif p.freq == 1:
				crease1(p, arena)
		elif p.ship == SHIP_LEVIATHAN:
			if p.freq == 0:
				crease0(p, arena)
			elif p.freq == 1:
				crease1(p, arena)

def count_playing(arena):
	players = [ 0 ]
	def cb_count(p):
		if p.arena == arena and p.ship < SHIP_SPEC:
			players[0] += 1
	for_each_player(cb_count)
	return players[0]

def turn_game_off(arena):
	arena.host = None
	arena.game_is_on = False
	arena.hockey_ref1 = None
	arena.hockey_ref2 = None
	arena.hockey_ref3 = None
	arena.hockey_ref5 = None
	arena.hockey_ref4 = None
	arena.hockey_ref6 = None
	arena.score=[0, 0]
	arena.hockey_kill_ref = None
	arena.hockey_ref7 = None
	arena.hockey_ref8 = None
	arena.hockey_ref9 = None
	reset(arena)
	arena.hockey_pucktimer = None
	arena.hockey_rapidkilltimer = None
	arena.playerDB={}
	arena.just_got_hat = None
	arena.assistant_tracking = None, None
	arena.is_assistant = None
	#reset_score(p)

def turn_game_on(arena):
	arena.game_is_on = True
	arena.hockey_ref1 = reg_callback(CB_BALLFIRE, ballfire, arena)
	arena.hockey_ref2 = add_command("faceoff", c_faceoff, arena)
	arena.hockey_ref3 = add_command("stop", c_stop, arena)
	arena.hockey_ref5 = reg_callback(CB_GOAL, goal, arena)
	arena.hockey_ref4 = reg_callback(CB_BALLPICKUP, ballpickup, arena)
	arena.hockey_kill_ref = reg_callback(CB_KILL, kill, arena)
	arena.score=[0, 0]
	arena.hockey_ref6 = add_command("apfz", c_apfz, arena)
	arena.hockey_ref7 = add_command("rpfz", c_rpfz, arena)
	arena.hockey_ref8 = add_command("apfo", c_apfo, arena)
	arena.hockey_ref9 = add_command("rpfo", c_rpfo, arena)
	objs.Toggle(arena, 1000, 1)
	objs.Toggle(arena, 1100, 1)
	arena.hockey_pucktimer = None
	arena.hockey_rapidkilltimer = None
	arena.playerDB={}
	arena.assistant_tracking = None, None
	arena.is_assistant = None
	#reset_score(p)

def reset(arena):
	objs.Toggle(arena, 1000, 0)
	objs.Toggle(arena, 1001, 0)
	objs.Toggle(arena, 1002, 0)
	objs.Toggle(arena, 1003, 0)
	objs.Toggle(arena, 1004, 0)
	objs.Toggle(arena, 1005, 0)
	objs.Toggle(arena, 1006, 0)
	objs.Toggle(arena, 1007, 0)
	objs.Toggle(arena, 1008, 0)
	objs.Toggle(arena, 1009, 0)
	objs.Toggle(arena, 1100, 0)
	objs.Toggle(arena, 1101, 0)
	objs.Toggle(arena, 1102, 0)
	objs.Toggle(arena, 1103, 0)
	objs.Toggle(arena, 1104, 0)
	objs.Toggle(arena, 1105, 0)
	objs.Toggle(arena, 1106, 0)
	objs.Toggle(arena, 1107, 0)
	objs.Toggle(arena, 1108, 0)
	objs.Toggle(arena, 1109, 0)

def shipfreqchange(p, newship, oldship, newfreq, oldfreq):
	if newship != SHIP_SPEC:
		if count_playing(p.arena) < 2:
			if p.arena.game_is_on:
				chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: Game Over. Not enough players to continue.")
				end_game(p.arena)
				turn_game_off(p.arena)

def make_start_timer(initial, interval, arena):
	def timer():
		if arena.game_is_on == False:
			chat.SendArenaSoundMessage(arena, buzz, "Hockey has been enabled by [ %s ] You need to follow their directions." % (arena.host))
			chat.SendArenaSoundMessage(arena, buzz, "They are the ref of this game, and their call is final.")
			balls.SetBallCount (arena, 0)
			turn_game_on(arena)
			return 0
	return set_timer(timer, initial, interval)

def make_rules_timer(initial, interval, arena):
	def timer():
		chat.SendArenaSoundMessage(arena, 4, "Rules: Two teams fight to keep control of the puck (powerball).")
		chat.SendArenaMessage(arena, "Rules: Each team needs to have a goalie before the game can start.")
		chat.SendArenaMessage(arena, "Rules: Help your team score a goal, but do not fire with in the crease line.")
		return 0
	return set_timer(timer, initial, interval)

def make_faceoff_timer(initial, interval, arena):
	def timer():
		chat.SendArenaSoundMessage (arena, buzz, "GET READY!!!")
		return 0
	return set_timer(timer, initial, interval)

def make_droppuck_timer(initial, interval, p):
	def timer():
		for_each_player(off_sides_check)
		chat.SendArenaSoundMessage (p.arena, drop, "GO! GO! GO!")
		balls.SetBallCount (p.arena, 1)
		return 0
	return set_timer(timer, initial, interval)

def off_sides_check(p):
	if p.ship != SHIP_SPEC:
		if not p.name in p.arena.playerDB:
			p.arena.playerDB[p.name]=player_data()
		if p.freq == 0:
			rgn = mapdata.FindRegionByName(p.arena, "side1")
			if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
				p.arena.playerDB[p.name].penalty+=1
				chat.SendArenaSoundMessage (p.arena, buzz, "[ %s ] - Penalized for being Off-Sides and now has [ %s ] Penalties." % (p.name, p.arena.playerDB[p.name].penalty ))
		if p.freq == 1:
			rgn = mapdata.FindRegionByName(p.arena, "side0")
			if mapdata.Contains(rgn, p.position[0] / 16, p.position[1] / 16):
				p.arena.playerDB[p.name].penalty+=1
				chat.SendArenaSoundMessage (p.arena, buzz, "[ %s ] - Penalized for being Off-Sides and now has [ %s ] Penalties." % (p.name, p.arena.playerDB[p.name].penalty ))

def c_howto(cmd, params, p, targ):
	"""\
Module: <py> hockey
Commands: start, stop, faceoff, apfz, apfo, rpfz, rpfo, howto.
For help on a specific command, use ?c command
"""

def c_faceoff(cmd, params, p, targ):
	"""\
Module: <py> hockey
Min. players: 2
This is an attach module created by resoL
This command will start the faceoff for the hockey game
"""
	if p.arena.game_is_on:
		#set timer for faceoff
		p.arena.droppuck = make_droppuck_timer(random.randrange(1001,1501,100), 100, p)
		p.arena.faceoff = make_faceoff_timer(1000, 100, p.arena)
		chat.SendArenaSoundMessage (p.arena, 103, "NOTICE: The puck will drop between 10-15 seconds! Please stay in faceoff formation.")
		balls.SetBallCount (p.arena, 0)
		p.arena.assistant_tracking = None, None

def c_stop(cmd, params, p, targ):
	"""\
Module: <py> hockey
This command will stop the hockey game if one has started.
"""
	if p.arena.host == p.name and p.arena.game_is_on:
		chat.SendArenaMessage(p.arena, "%s has stopped the current game." % (p.name))
		end_game(p.arena)
		turn_game_off(p.arena)
	if p.arena.host != p.name:
		chat.SendArenaMessage (p, "Only the Event Host can stop the game.")
	else:
		chat.SendArenaMessage (p, "Why would stop a game that hasn't started?")

def c_start(cmd, params, p, targ):
	"""\
Module: <py> hockey
Min. players: 2
This is an attach module created by resoL
This command will active the hockey game
"""
	players = count_playing(p.arena)

	if p.arena.game_is_on:
		chat.SendMessage (p, "There is already a game started. You must first stop the current game before starting a new one silly!")

	elif players >= 1:
		p.arena.start = make_start_timer(1500, 100, p.arena)
		p.arena.rules = make_rules_timer(500, 100, p.arena)
		chat.SendMessage(p, "Commands for Hockey: ?start - Starts the game, ?stop - Stops the game and checks for wins, ?faceoff - Starts the faceoff timer.")
		chat.SendMessage(p, "Commands for Hockey: ?apfz - Adds 1 point to freq zero, ?rpfz - Removes 1 point from freq zero, ?apfo - Adds 1 point to freq one, ?rpvfo - Removes 1 point from freq one.")
		chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: A round of Hockey is starting in 15 seconds!")
		chat.SendArenaMessage(p.arena, "This event is being hosted by: [ %s ]. Please contact this player if you have questions." % p.name)
		p.arena.host=p.name
	else:
		chat.SendMessage(p, "Not enough players, %d more needed." % (2 - players))

def c_apfz(cmd, params, p, targ):
	"""\
Module: <py> hockey
This command will force add a teams point.
"""
	if p.arena.game_is_on:
		objs.Toggle(p.arena, ids[0][p.arena.score[0]], 0)
		p.arena.score[0]+=1
		chat.SendArenaSoundMessage (p.arena, buzz, "NOTICE: The Ref [ %s ] has given Freq 0 a point." % (p.name))
		chat.SendArenaSoundMessage (p.arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (p.arena.score[0], p.arena.score[1]))
		objs.Toggle(p.arena, ids[0][p.arena.score[0]], 1)
		check_score(p.arena)

def c_rpfz(cmd, params, p, targ):
	"""\
Module: <py> hockey
This command will force remove a teams point.
"""
	if p.arena.game_is_on:
		objs.Toggle(p.arena, ids[0][p.arena.score[0]], 0)
		p.arena.score[0]-=1
		chat.SendArenaSoundMessage (p.arena, buzz, "NOTICE: The Ref [ %s ] has removed a point from Freq 0." % (p.name))
		chat.SendArenaSoundMessage (p.arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (p.arena.score[0], p.arena.score[1]))
		objs.Toggle(p.arena, ids[0][p.arena.score[0]], 1)
		check_score(p.arena)

def c_apfo(cmd, params, p, targ):
	"""\
Module: <py> hockey
Args: 0 or 1, for the team. Example '?addpt 1'
This command will force add a teams point.
"""
	if p.arena.game_is_on:
		objs.Toggle(p.arena, ids[1][p.arena.score[1]], 0)
		p.arena.score[1]+=1
		chat.SendArenaSoundMessage (p.arena, buzz, "NOTICE: The Ref [ %s ] has given Freq 1 a point." % (p.name))
		chat.SendArenaSoundMessage (p.arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (p.arena.score[0], p.arena.score[1]))
		objs.Toggle(p.arena, ids[1][p.arena.score[1]], 1)
		check_score(p.arena)

def c_rpfo(cmd, params, p, targ):
	"""\
Module: <py> hockey
This command will force remove a teams point.
"""
	if p.arena.game_is_on:
		objs.Toggle(p.arena, ids[1][p.arena.score[1]], 0)
		p.arena.score[1]-=1
		chat.SendArenaSoundMessage (p.arena, buzz, "NOTICE: The Ref [ %s ] has removed a point from Freq 1." % (p.name))
		chat.SendArenaSoundMessage (p.arena, 103, "NOTICE: Score = <-Freq-0 [ %s  to  %s ] Freq-1->" % (p.arena.score[0], p.arena.score[1]))
		objs.Toggle(p.arena, ids[1][p.arena.score[1]], 1)
		check_score(p.arena)

def make_sb_timer(initial, interval, arena, freq):
	def timer():
		if arena.game_is_on:
			objs.Toggle(arena, ids[freq][arena.score[freq]], 1)
		return 0
	return set_timer(timer, initial, interval)

def paction(p, action, arena):
	if action == PA_ENTERARENA:
		if arena.game_is_on:
			chat.SendMessage(p, "This event is being hosted by: [ %s ]. Contact them if you would like to join the game." % (arena.host))
			chat.SendModMessage ("New Player: %s in Event Arena %s" % (p.name, arena.name))
			try:
				objs.Toggle(p, 1000, 1)
				objs.Toggle(p, 1100, 1)
			except AttributeError:
				objs.Toggle(p.arena, ids[p.freq][arena.score[p.freq]], 1)
	elif count_playing(arena) < 2:
		if arena.game_is_on:
			chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: The game has stopped. Not enough players to continue.")
			end_game(arena)
			turn_game_off(arena)
#	elif action == PA_LEAVEARENA:
#		if count_playing(p.arena) < 2:
#			if p.arena.game_is_on:
#				chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: Game Over. Not enough players to continue.")
#				turn_game_off(p.arena)

def mm_attach(arena):
	arena.game_is_on = False
	arena.hockey_pactioncb = reg_callback(CB_PLAYERACTION, paction, arena)
	arena.host = None
	arena.hockey_cmd1 = add_command("start", c_start, arena)
	arena.hockey_cmd2 = add_command("howto", c_howto, arena)
	arena.hockey_shipcb = reg_callback(CB_SHIPFREQCHANGE, shipfreqchange, arena)
	chat.SendArenaMessage(arena, "Game Module: Hockey attached. Please wait for a staff member to start this event.")
	arena.hockey_killpuck_timer = None
	arena.hockey_droppuck_timer = None
	arena.hockey_faceoff_timer = None
	arena.hockey_start_timer = None
	arena.hockey_rules_timer = None
	arena.hockey_goals = 0
	arena.playerDB={}

def mm_detach(arena):
	arena.game_is_on = False
	arena.hockey_pactioncb = None
	arena.host = None
	arena.hockey_shipcb = None
	arena.hockey_cmd1 = None
	arena.hockey_cmd2 = None
	arena.hockey_ref1 = None
	arena.hockey_ref3 = None
	#arena.hockey_ref5 = None
	arena.hockey_killpuck_timer = None
	arena.hockey_droppuck_timer = None
	arena.hockey_faceoff_timer = None
	arena.hockey_start_timer = None
	arena.hockey_rules_timer = None
	arena.hockey_ref4 = None
	arena.hockey_shipcb = None
	vote_stop(arena)
	arena.vote_ref1 = None
	arena.vote_ref2= None
	arena.vote_ref3 = None
	arena.hockey_ref5 = None
	arena.hockey_ref4 = None
	arena.hockey_goals = 0
	arena.hockey_ref6 = None
	arena.hockey_ref7 = None
	arena.hockey_ref9 = None
	arena.hockey_ref8 = None
	arena.has_puck = None
	arena.hockey_pucktimer = None
	arena.hockey_assisttimer = None
	arena.hockey_not_assisttimer = None
	arena.hockey_icingtimer = None
	reset(arena)
	arena.playerDB={}
	arena.just_got_hat = None
	arena.assist_freq = None
