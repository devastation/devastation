#prodem by resol 7-11
#idea from leader by chambahs and Prodem in SSCU Trench Wars

from asss import *

chat = get_interface(I_CHAT)
game = get_interface(I_GAME)

def count_playing(arena):
	players = [ 0 ]
	def cb_count(p):
		if p.arena == arena and p.ship < SHIP_SPEC:
			players[0] += 1
	for_each_player(cb_count)
	return players[0]

def turn_game_off(arena):
	arena.host = None
	arena.prodem_killcb = None
	arena.game_is_on = False
	game.UnlockArena(arena, 1, 0)

def turn_game_on(arena):
	arena.game_is_on = True
	arena.prodem_killcb = reg_callback(CB_KILL, kill, arena)
	game.LockArena(arena, 1, 0, 0, 0)

def shipfreqchange(p, newship, oldship, newfreq, oldfreq):
	if newship != SHIP_SPEC:
		if p.arena.game_is_on and p.ship == SHIP_WARBIRD:
			chat.SendArenaSoundMessage(p.arena, 2, "NOTICE: %s Has reached Warbird Status, demote them if you can, promote them and they win!" % p.name)
		elif count_playing(p.arena) < 2:
			if p.arena.game_is_on:
				chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: Game Over. Not enough players to continue.")
				turn_game_off(p.arena)

def kill(arena, killer, killed, bounty, flags, pts, green):
    # promote
	if killer.ship != SHIP_WARBIRD and killer.ship >= killed.ship:
		game.SetShip(killer, killer.ship - 1)

    # check for game over
	elif killer.ship == SHIP_WARBIRD and killed.ship == SHIP_WARBIRD:
		chat.SendArenaSoundMessage(arena, 5, "NOTICE: Game over. %s is the winner!" % killer.name)
		game.UnlockArena(arena, 1, 0)
		turn_game_off(arena)

    # demote
	if killed.ship != SHIP_SHARK and killed.ship <= killer.ship:
		game.SetShip(killed, killed.ship + 1)

	return pts, green

def make_start_timer(initial, interval, arena):
	def timer():
		def setships(p):
			if p.ship < SHIP_SPEC:
				game.SetShip(p, SHIP_SHARK)
		for_each_player(setships)

		chat.SendArenaSoundMessage(arena, 104, "GO! GO! GO! GO!")
		turn_game_on(arena)
		return 0
	return set_timer(timer, initial, interval)

def make_rules_timer(initial, interval, arena):
	def timer():
		chat.SendArenaSoundMessage(arena, 4, "Rules: All ships start as shark, kill an equal or lower number ship to get promoted.")
		chat.SendArenaMessage(arena, "Rules: If you are killed by a higher number ship then you, you will be demoted.")
		chat.SendArenaMessage(arena, "Rules: First player to kill another Warbird using a Warbird wins the game.")
		return 0
	return set_timer(timer, initial, interval)

def c_start(cmd, params, p, targ):
	"""\
Module: <py> prodem
Min. players: 2
All ships start as shark, kill an equal or lower number ship to get promoted.
If you are killed by a higher number ship then you, you will be demoted.
First player to kill another Warbird using a Warbird wins the game.
"""
	players = count_playing(p.arena)

	if players >= 2:
		p.arena.start = make_start_timer(1500, 100, p.arena)
		p.arena.rules = make_rules_timer(500, 100, p.arena)
		chat.SendArenaSoundMessage(p.arena, 1, "NOTICE: A round of Prodem is starting in 15 seconds!")
		chat.SendArenaMessage(p.arena, "This event is being hosted by: [ %s ]. Please contact this player if you lagout or have questions." % p.name)
		p.arena.host=p.name
	else:
		chat.SendMessage(p, "Not enough players, %d more needed." % (2 - players))

def paction(p, action, arena):
	if action == PA_ENTERARENA:
		if arena.game_is_on:
			chat.SendMessage(p, "This event is being hosted by: [ %s ]. Contact them if you would like to join the game." % (arena.host))
			chat.SendModMessage ("New Player: %s in Event Arena %s" % (p.name, arena.name))
	elif count_playing(arena) < 2:
		if arena.game_is_on:
			turn_game_off(arena)

def mm_attach(arena):
	arena.game_is_on = False
	arena.prodem_pactioncb = reg_callback(CB_PLAYERACTION, paction, arena)
	arena.host = None
	arena.prodem_cmd1 = add_command("start", c_start, arena)
	arena.prodem_shipcb = reg_callback(CB_SHIPFREQCHANGE, shipfreqchange, arena)
	chat.SendArenaMessage(arena, "Game Module: Prodem attached. Please wait for a staff member to start this event.")


def mm_detach(arena):
	arena.game_is_on = False
	arena.prodem_pactioncb = None
	arena.host = None
	arena.prodem_killcb = None
	arena.prodem_shipcb = None
	arena.prodem_cmd1 = None
