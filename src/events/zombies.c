local void zombieStop(Arena* arena);
local int TimeUp_Zombies(void *p);

/* Announce the Most Valuable Human */
local void zombieMVH(Arena *arena)
{
    Link *link;
    int mvhscore = -1; //in case someone has a score equal to zero
    Player *h, *mvh = NULL;
    pd->Lock();
    FOR_EACH_PLAYER(h)
    {
        Pdata *data = PPDATA(h, playerKey);
        int lives = data->lives;
        if ((h->arena == arena) && (lives > mvhscore) && (h->p_ship != SHIP_SPEC))
        {
            mvhscore = lives;
            mvh = h;
        }
    }
    pd->Unlock();
    chat->SendArenaMessage(arena, "MVH: %s (%i %s)", mvh->name, mvhscore, mvhscore == 1 ? "kill" : "kills");
}

/* Announce the Most Valuable Zombie */
local void zombieMVZ(Arena *arena)
{
    Link *link;
    int mvzscore = -1; //in case someone has a score equal to zero
    Player *g, *mvz = NULL;
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        Pdata *data = PPDATA(g, playerKey);
        int kills = data->kills;
        if ((g->arena == arena) && (kills > mvzscore) && (g->p_ship != SHIP_SPEC))
        {
            mvzscore = kills;
            mvz = g;
        }
    }
    pd->Unlock();
    chat->SendArenaMessage(arena, "MVZ: %s (%i %s)", mvz->name, mvzscore, mvzscore == 1 ? "kill" : "kills");
}

/* Check if there is only 1 human left (or 0 if something went wrong) */
local void zombieCheck(Arena *arena)
{
    Player *p, *winner = NULL;
    Link *link;
    int count = 0;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p->p_freq != ZOMBIE_FREQ))
        {
            count++;
            winner = p;
        }
    }
    pd->Unlock();

    if (count == 1)
    {
        zombieMVH(arena);
        zombieMVZ(arena);
        chat->SendArenaSoundMessage(arena, 5, "Game Over! Last human alive: %s", winner->name);
        zombieStop(arena);
    }
    else if (count == 0)
    {
        zombieMVH(arena);
        zombieMVZ(arena);
        chat->SendArenaSoundMessage(arena, 13, "Game Over! There were no survivors!");
        zombieStop(arena);
    }
}

/* For every kill */
local void zombieKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    /* If a zombie kills a player, put that player on the zombie team. */
    if ((k->p_freq == ZOMBIE_FREQ) && (p->p_freq != ZOMBIE_FREQ))
    {
        game->SetShipAndFreq(p, k->p_ship, ZOMBIE_FREQ);
        chat->SendArenaSoundMessage(p->arena, 13, "%s was zombified!", p->name);
        Pdata *data = PPDATA(k, playerKey);
        /* Record kills as a zombie */
        data->kills++;
        zombieCheck(arena);
    }
    /* If a human kills a zombie */
    else if ((k->p_freq != ZOMBIE_FREQ) && (p->p_freq == ZOMBIE_FREQ))
    {
        /* Record kills as a human */
        Pdata *data = PPDATA(k, playerKey);
        data->lives++;
    }
}

/* When a player spectates */
local void zombieShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        zombieCheck(p->arena);
    }
}

/* When a player leaves the game */
local void zombiePlayerAction(Player *p, int action, Arena *arena)
{
    if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
        zombieCheck(arena);
}

/* Start the game and check parameters */
local void Start_Zombies(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 6; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options*/
	char *next, *string;
	
	//Zombie ship
	int zombieship = 2;
	string = getOption(params, 'g');

	if (string != next)
	{
		zombieship = atoi(string);
		if ((zombieship > 0) && (zombieship < 9))
			adata->gship = zombieship - 1;
	}
	
	if (!adata->gship) //fallback to spider
	{
		adata->gship = 2;
	}
	adata->glockship = 1;
	
	//zombie
	Player *zombie = NULL;
	string = getOption(params, 'z');
	if (string != next)
	{
		zombie = pd->FindPlayer(string);
		
		if (zombie)
			adata->zombie = zombie;
	}

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}

	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "Zombies will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	
	chat->SendArenaMessage(arena, "Zombie ship: %i", adata->gship + 1);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_Zombies, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_Zombies(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset zombie kills
				pdata->lives = 0; //reset human kills

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	//OnePlayerFreqs(host->arena); //put everyone on single teams
	
	Player *zombie = adata->zombie;
	if (!zombie || zombie->arena != arena)
	{
		zombie = RandomPlayer(arena);
		adata->zombie = zombie;
	}
	game->SetShipAndFreq(zombie, adata->gship, ZOMBIE_FREQ);
	
	chat->SendArenaSoundMessage(host->arena, 104, "Zombies started. All zombies will use ship %i.", adata->gship + 1);
	chat->SendArenaMessage(host->arena, "%s has the honor of being the first zombie.", zombie->name);

    mm->RegCallback(CB_KILL, zombieKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, zombiePlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, zombieShipFreqChange, host->arena);
	return 0;
}

/* Abort the current game */
local void zombieStop(Arena* arena)
{
    ml->ClearTimer(TimeUp_Zombies, arena);
    mm->UnregCallback(CB_KILL, zombieKill, arena);
    mm->UnregCallback(CB_PLAYERACTION, zombiePlayerAction, arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, zombieShipFreqChange, arena);
    
	Stop(arena);
}
