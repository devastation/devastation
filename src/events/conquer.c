local void conquerStop(Arena* arena);
local int TimeUp_Conquer(void *p);

/* Retrieve the owner of a particular team frequency */
local char* getOwner(Arena *arena, int freq)
{
    Player *p, *pr = NULL;
    Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        Pdata *data = PPDATA(p, playerKey);
        if ((p->arena == arena) && (p->p_freq == freq) && (data->lives == 1))
        {
            pr = p;
        }
    }
    pd->Unlock();

    if (pr)
        return pr->name;
    else
        return "No Owner";
}

/* Verify whether win conditions have been met (i.e. only one team left standing) */
local void conquerCheck(Arena *arena)
{
    int winfreq = 0, check = 0;
    Player *p, *owner = NULL;
    Link *link;
	
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC))
        {            
            Pdata *data = PPDATA(p, playerKey);
            if (data->lives == 1)
            {
                owner = p;
				winfreq = p->p_freq;
				check++;
            }
        }
    }
    pd->Unlock();

	if (check == 1)
	{
        int mvpscore = -1;
        Player *mvp = NULL;
		
        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
            Pdata *data = PPDATA(p, playerKey);
            int kills = data->kills;
            if ((p->arena == arena) && (kills > mvpscore))
            {
                mvpscore = kills;
                mvp = p;
            }
        }
        pd->Unlock();
		chat->SendArenaMessage(arena, "MVP: %s (%i %s)", mvp->name, mvpscore, mvpscore == 1 ? "kill" : "kills");
		
		chat->SendArenaSoundMessage(arena, 5, "Game Over! Winner: Team %i [%s]", winfreq, owner->name);
		conquerStop(arena);
	}
}

/* When a player kills another player, assimilate them (and their team if captain) */
local void conquerKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    Pdata *data = PPDATA(p, playerKey);

    if (data->lives == 1) //if captain
    {
        data->lives = 0;
        Player *pl;
		Link *link;
		
        chat->SendArenaSoundMessage(arena, 18, "Team %i [%s] has been conquered by team %i [%s]", p->p_freq, p->name, k->p_freq, getOwner(p->arena, k->p_freq));

        pd->Lock();
        FOR_EACH_PLAYER(pl)
        {
            if ((pl->arena == arena) && (pl->p_freq == p->p_freq))
            {
                game->SetFreq(pl, k->p_freq);
            }
        }
        pd->Unlock();
    }
    else
    {
        chat->SendArenaSoundMessage(arena, 18, "Player %s has been conquered by team %i [%s]", p->name, k->p_freq, getOwner(p->arena, k->p_freq));
        game->SetFreq(p, k->p_freq);
    }

    data = PPDATA(k, playerKey);
    data->kills++;
    conquerCheck(arena);
}

/* If a player attempts to change ships, specs or gets disconnected */
local void conquerNewLeader(int freq, Arena *arena, Player *n) //bug: freq should be returning oldfreq, not newfreq
{
/*    Pdata *ndata = PPDATA(n, playerKey);
    if ((n->p_freq == arena->specfreq) && (ndata->lives == 1))
    {
        ndata->lives = 0;
    }

    Player *p; Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_freq == freq) && (p->p_freq != arena->specfreq) && (p != n))
        {
            pd->Unlock();
            Pdata *data = PPDATA(p, playerKey);
            data->lives = 1;
            chat->SendArenaMessage(arena, "%s is now the leader of freq %i", p->name, freq);
            return;
        }
    }
    pd->Unlock();*/
}

/* If a player attempts to change freqs, specs or gets disconnected */
local void conquerNewFLeader(int oldfreq, int newfreq, Arena *arena, Player *n)
{
    Pdata *ndata = PPDATA(n, playerKey);
    if ((newfreq == arena->specfreq) && (ndata->lives == 1))
    {
        ndata->lives = 0;
    }

    Player *p; Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_freq == oldfreq) && (oldfreq != arena->specfreq) && (p != n))
        {
            pd->Unlock();
            Pdata *data = PPDATA(p, playerKey);
            data->lives = 1;
            chat->SendArenaMessage(arena, "%s is now the leader of freq %i", p->name, oldfreq);
            return;
        }
    }
    pd->Unlock();
}

//Callback: ShipFreqChange
local void conquerShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        conquerNewLeader(p->p_freq, p->arena, p);
        conquerCheck(p->arena);
    }
}

//Callback: FreqChange
/*local void conquerFreqChange(Player *p, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        conquerNewFLeader(oldfreq, p->p_freq, p->arena, p);
        conquerCheck(p->arena);
    }
}*/

//Callback PlayerAction
local void conquerPlayerAction(Player *p, int action, Arena *arena)
{
    if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
    {
        conquerNewLeader(p->p_freq, arena, p);
        conquerCheck(arena);
    }
}

/* Start the game and check parameters */
local void Start_Conquer(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 5; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options*/
	char *next, *string;

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}

	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "Conquer will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_Conquer, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_Conquer(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset deaths
				pdata->lives = 1; //reset lives

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	OnePlayerFreqs(host->arena); //put everyone on single teams
	
	chat->SendArenaSoundMessage(host->arena, 104, "Conquer has started. You win after you conquer all enemy teams!");

    mm->RegCallback(CB_KILL, conquerKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, conquerPlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, conquerShipFreqChange, host->arena);
    //mm->RegCallback(CB_FREQCHANGE, conquerFreqChange, host->arena);
	return 0;
}

/* End the current game */
local void conquerStop(Arena* arena)
{
    ml->ClearTimer(TimeUp_Conquer, arena);
    mm->UnregCallback(CB_KILL, conquerKill, arena);
    mm->UnregCallback(CB_PLAYERACTION, conquerPlayerAction, arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, conquerShipFreqChange, arena);
    //mm->UnregCallback(CB_FREQCHANGE, conquerFreqChange, arena);
    
	Stop(arena);
}

