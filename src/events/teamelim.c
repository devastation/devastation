local int TimeUp_TeamElim(void *p);
local void teamelimStop(Arena* arena);

local int TeLlock = 0;

/* Check if there is only one team remaining in the game. */
local void teamelimWinCheck(Arena *arena, int killerfreq)
{
    Player *p;
    int count = 0, killcount = 0;
    Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if (p->arena == arena)
        {
            if (p->p_ship != SHIP_SPEC)
            {
                Pdata *data = PPDATA(p, playerKey);
                    if (p->p_freq != killerfreq)
                    {
                        count++;
                    }
                    else
                    {
                        killcount = killcount + data->kills;
                    }
            }
        }
    }

    pd->Unlock();

    if (count == 0)
    {
        chat->SendArenaSoundMessage(p->arena, 5, "This round's winner is team [%i]. (%i kills)", killerfreq, killcount);
        teamelimStop(p->arena);
        TeLlock = 0;
        return;
    }
    else
        return;
}

/* Count the players left in the game. (FIXME) */
local void teamelimLWinCheck(Arena *arena, Player *pe)
{
    if (TeLlock)
        return;

    Player *p = NULL, *winner = NULL;
    Link *link;
    int i = 0;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p!=pe))
        {
            winner = p;
            i++;
        }
    }
    pd->Unlock();
    if (i == 1)
    {
        Pdata *data = PPDATA(winner, playerKey);
        chat->SendArenaMessage(arena, "This round's winner is %s (%i %s).", winner->name, data->kills, data->kills == 1 ? "kill" : "kills");
        teamelimStop(arena);
    }
}

/* Count the players if someone goes into spec. */
local void teamelimShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        teamelimLWinCheck(p->arena, p);
    }
}

/* Count the players if someone disconnects or leaves arena. */
local void teamelimPlayerAction(Player *p, int action, Arena *arena)
{
    if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
        teamelimLWinCheck(arena, p);
}

local void teamelimCheck(Player *p, int freq, Player *k, int killerfreq)
{
    TeLlock = 1;
    Pdata *data = PPDATA(p, playerKey);
    data->lives--;
    
    int count = 0, killcount = 0, pcount = 0;
    Player *g;
    Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        Pdata *data = PPDATA(g, playerKey);
        if (g->p_freq == freq && g->arena == p->arena)
        {
            count = count + data->lives;
            killcount = killcount + data->kills;
            pcount++;
    //        strcat(buf, g->name); //
        }
    }
    pd->Unlock();
	
	
	Adata *adata = P_ARENA_DATA(p->arena, arenaKey);

    if (count == adata->obj)
    {
        chat->SendArenaSoundMessage(p->arena, 27, "Team [%i] is out. (%i %s)",
                freq, killcount, killcount == 1 ? "kill" : "kills");
        /* Put the entire team in spec. */
        Link *link;
        pd->Lock();
        FOR_EACH_PLAYER(g)
        {
            if (g->p_freq == freq && g->arena == p->arena)
            {
                game->SetShip(g, SHIP_SPEC);
                game->SetFreq(g, p->arena->specfreq);
            }
        }
        pd->Unlock();
    }

    /* Check if there's only one team left. */
    teamelimWinCheck(p->arena, k->p_freq);
    
    TeLlock = 0;
}

/* Add to a player's kills. */
local void teamelimKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    Pdata *data = PPDATA(k, playerKey);
    data->kills++;

    teamelimCheck(p, p->p_freq, k, k->p_freq);
}

local int eteamsof = 2;

/* Start the game and check parameters */
local void Start_TeamElim(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 1; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options*/
	char *next, *string;
	
	//deaths
	int deaths;
	string = getOption(params, 'd');

	if (string != next)
	{
		deaths = atoi(string);
		if ((deaths <= 50) && (deaths > 0))
			adata->obj = deaths;
		else
			adata->obj = 3;
	}
	else
		adata->obj = 3;
	
	string = getOption(params, 't');
	
	if (string != next)
	{
		eteamsof = atoi(string);
		if ((eteamsof > 10) || (eteamsof < 2))
			eteamsof = 2;
	}
	
	if (!eteamsof)
		eteamsof = 2;

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}

	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "Team Elimination will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_TeamElim, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_TeamElim(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset deaths
				pdata->lives = adata->obj; //reset deaths

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	MultiPlayerFreqs(host->arena, eteamsof);
	
	chat->SendArenaSoundMessage(host->arena, 104, "Team Elimination has started, your team will be eliminated after %i %s.", adata->obj, adata->obj == 1 ? "death" : "deaths");

    mm->RegCallback(CB_KILL, teamelimKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, teamelimPlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, teamelimShipFreqChange, host->arena);
	return 0;
}


/* When the game is stopped, unregister callbacks and timers, and unlock arena. */
local void teamelimStop(Arena* arena)
{
    ml->ClearTimer(TimeUp_TeamElim, arena);
    mm->UnregCallback(CB_KILL, teamelimKill, arena);
    mm->UnregCallback(CB_PLAYERACTION, teamelimPlayerAction, arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, teamelimShipFreqChange, arena);
    
    Stop(arena);
}
