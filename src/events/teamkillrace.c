local void teamkillraceStop(Arena* arena);
local int TimeUp_TeamKillRace(void *p);

//FIXME: increment is off (kills = per player rather than per team)

/* Count the players left in the game. (FIXME) */
local void teamkillraceLCheck(Arena *arena, Player *pe)
{
    Player *p = NULL, *winner = NULL;
    Link *link;
    int i = 0;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p!=pe))
        {
            winner = p;
            i++;
        }
    }
    pd->Unlock();
    if (i == 1)
    {
        chat->SendArenaSoundMessage(arena, 5, "Game Over! This round's winner is %s.", winner->name);
        teamkillraceStop(arena);
    }
}

/* Count the players if someone goes into spec. */
local void teamkillraceShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        teamkillraceLCheck(p->arena, p);
    }
}

/* Count the players if someone disconnects or leaves arena. */
local void teamkillracePlayerAction(Player *p, int action, Arena *arena)
{
   if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
        teamkillraceLCheck(arena, p);
}

/* Check win conditions (kills) */
local void teamkillraceCheck(Player *p, int freq)
{
    //data->lives is the number of kills needed to win.
    Pdata *data = PPDATA(p, playerKey);
    
    int count = 0, killcount = 0, pcount = 0;
    Player *g;
    Link *link;
	
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        Pdata *data = PPDATA(g, playerKey);
        if (g->arena == p->arena && g->p_freq == freq)
        {
            count = count + data->lives;
            killcount = killcount + data->kills;
            pcount++;
        }
    }
    pd->Unlock();

    if (killcount == data->lives)
    {
        chat->SendArenaSoundMessage(p->arena, 5, "Game Over! This round's winner is Team [%i].", freq);
        teamkillraceStop(p->arena);
    }
}

/* Add to a player's kills. */
local void teamkillraceKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    Pdata *data = PPDATA(k, playerKey);
    data->kills++;

    teamkillraceCheck(k, k->p_freq);
}

local int kteamsof = 2;

/* Start the game and check parameters */
local void Start_TeamKillRace(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 4; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options */
	char *next, *string;
	
	//kills
	int kills;
	string = getOption(params, 'k');

	if (string != next)
	{
		kills = atoi(string);
		if ((kills <= 50) && (kills > 0))
			adata->obj = kills;
		else
			adata->obj = 10;
	}
	else
	{
		adata->obj = 10;
	}
	
	string = getOption(params, 't');
	
	if (string != next)
	{
		kteamsof = atoi(string);
		if ((kteamsof > 10) || (kteamsof < 2))
			kteamsof = 2;
	}
	
	if (!kteamsof)
		kteamsof = 2;

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}

	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "Team KillRace will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_TeamKillRace, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_TeamKillRace(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;

	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset kills
				pdata->lives = adata->obj; //reset lives

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	MultiPlayerFreqs(host->arena, kteamsof);
	
	chat->SendArenaSoundMessage(host->arena, 104, "Team KillRace started, you'll win after %i %s.", adata->obj, adata->obj == 1 ? "kill" : "kills");

    mm->RegCallback(CB_KILL, teamkillraceKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, teamkillracePlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, teamkillraceShipFreqChange, host->arena);
	return 0;
}

/* When the game is stopped, unregister callbacks and timers, and unlock arena. */
local void teamkillraceStop(Arena* arena)
{
    ml->ClearTimer(TimeUp_TeamKillRace, arena);
    mm->UnregCallback(CB_KILL, teamkillraceKill, arena);
    mm->UnregCallback(CB_PLAYERACTION, teamkillracePlayerAction, arena);
    mm->UnregCallback(CB_SHIPFREQCHANGE, teamkillraceShipFreqChange, arena);

    Stop(arena);
}
