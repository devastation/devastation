local void elimStop(Arena* arena);
local int TimeUp_Elim(void *p);

local int ELlock = 0; // Note: Should be adata

/* Verify whether win conditions have been met (i.e. only one player left) */
local Player* elimWinCheck(Arena *arena)
{
    Player *p, *last = NULL;
    int count = 0;
    Link *link;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if (p->arena == arena)
        {
            if (p->p_ship != SHIP_SPEC)
            {
                Pdata *data = PPDATA(p, playerKey);
                if (data->lives != 0)
                {
                    count++;
                    last = p;
                }
            }
        }
    }

    pd->Unlock();

    if (count == 1)
    {
        Player *g = NULL, *mvp = NULL;
        int mvpscore = -1; //in case all players spec
        Link *link;
        pd->Lock();
        FOR_EACH_PLAYER(g)
        {
            Pdata *data = PPDATA(g, playerKey);
            int kills = data->kills;
            if ((g->arena == arena) && (kills > mvpscore))
            {
                mvpscore = kills;
                mvp = g;
            }
        }
        pd->Unlock();
        chat->SendArenaMessage(arena, "MVP: %s (%i %s)", mvp->name, mvpscore, mvpscore == 1 ? "kill" : "kills");
        
        return last;
    }
    else
        return NULL;
}

/* Count the players left in the game. */
local void elimLWinCheck(Arena *arena, Player *pe)
{
    if (ELlock)
        return;

    Player *p = NULL, *winner = NULL;
    Link *link;
    int i = 0;
    int mvpscore = -1;
    pd->Lock();
    FOR_EACH_PLAYER(p)
    {
        if ((p->arena == arena) && (p->p_ship != SHIP_SPEC) && (p!=pe))
        {
            winner = p;
            i++;
        }
    }
    pd->Unlock();
    Player *g = NULL, *mvp = NULL;
    pd->Lock();
    FOR_EACH_PLAYER(g)
    {
        Pdata *data = PPDATA(g, playerKey);
        int kills = data->kills;
        if ((g->arena == arena) && (kills > mvpscore))
        {
            mvpscore = kills;
            mvp = g;
        }
    }
    pd->Unlock();
    
    if (i == 1)
    {
        Pdata *data = PPDATA(winner, playerKey);
        chat->SendArenaMessage(arena, "MVP: %s (%i %s)", mvp->name, mvpscore, mvpscore == 1 ? "kill" : "kills");
        chat->SendArenaSoundMessage(arena, 5, "This round's winner is %s (%i %s).", winner->name, data->kills, data->kills == 1 ? "kill" : "kills");
        elimStop(arena);
    }
}

/* Count the players if someone goes into spec. */
local void elimShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
    if (p->p_ship == SHIP_SPEC)
    {
        elimLWinCheck(p->arena, p);
    }
}

/* Count the players if someone disconnects or leaves arena. */
local void elimPlayerAction(Player *p, int action, Arena *arena)
{
    if ((action == PA_DISCONNECT) || (action == PA_LEAVEARENA))
        elimLWinCheck(arena, p);
}

local void elimCheck(Player *p)
{
    ELlock = 1;
    Pdata *data = PPDATA(p, playerKey);
    data->lives--;
    char lives = data->lives;

    if (lives < 1)
    {
        chat->SendArenaSoundMessage(p->arena, 27, "%s is out (%i %s).", p->name, data->kills, data->kills == 1 ? "kill" : "kills");
        game->SetShip(p, SHIP_SPEC);
        game->SetFreq(p, p->arena->specfreq);
    }

    Player *winner = elimWinCheck(p->arena);
    if (winner)
    {
        data = PPDATA(winner, playerKey);
        chat->SendArenaSoundMessage(p->arena, 5, "This round's winner is %s (%i %s).", winner->name, data->kills, data->kills == 1 ? "kill" : "kills");
        
        elimStop(p->arena);
    }
    ELlock = 0;
}

/* Add to a player's kills. */
local void elimKill(Arena *arena, Player *k, Player *p, int bounty, int flags, int pts, int green)
{
    Pdata *data = PPDATA(k, playerKey);
    data->kills++;

    elimCheck(p);
}

/* Start the game and check parameters */
local void Start_Elim(Player *host, Arena *arena, const char *params)
{
	/* Set the game as started */
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started >= 1)
	{
		chat->SendMessage(host, "There is already a game running. Please use ?stop to end the current game.");
		return;
	}
	else
	{
		adata->eventid = 1; //Unique Event ID
		adata->started = 1;
	}
	
	/* Get Game Options*/
	//deaths
	int deaths;
	char *next, *string;
	string = getOption(params, 'd');

	if (string != next)
	{
		deaths = atoi(string);
		if ((deaths <= 50) && (deaths > 0))
			adata->obj = deaths;
		else
		{
			Abort(arena, host, 1); //Note: Should we really abort?
			return;
		}
	}
	else
	{
		Abort(arena, host, 2); //Note: Should we really abort?
		return;
	}

	//ships
	string = getOption(params, 's');

	int ii;
	for (ii = 0; ii <= 8; ii++)
	{
		allships[ii] = 0; //reset every ship
	}


	int length = strlen(string);

	if ((string != next) && (string) && (length >= 1))
	{
		if ((length % 2 == 0)  || (length > 15) || (!length))
		{
			Abort(arena, host, 3);
			return;
		}
		else
		{
			int i;
			for (i = 0; i < length; i++)
			{
				if (string[i] != ',');
				{
					char first[64];
					snprintf(first, sizeof(first) - 10, "%s", string + i * 2);
					first[1] = '\0';

					int legal = atoi(first) - 1;
					if (legal == -1)
						break;

					if (legal < 0 || legal > 7)
					{
						Abort(arena, host, 4);
						return;
					}
					else
					{
						LegalShip(legal, arena);
					}
					first[0] = '\0';
				}
			}
			char first[64];
			strncpy(first, string, 4);
			first[1] = '\0';
			adata->defaultship = atoi(first) - 1;
			CheckLegalShip(arena);

			adata->lockships = 1;
		}
	}
	else
	{
		for (ii = 0; ii < 8; ii++)
		{
			allships[ii] = 1; //all ships are legal
		}
		adata->lockships = 0;
	}
	
	chat->SendArenaSoundMessage(arena, 2, "Elim will start in 20 seconds, enter if playing.");
	if (adata->lockships)
		chat->SendArenaMessage(arena, "Allowed ships: %s", string);
	CheckLegalShip(arena);
	
	/* Start the timer */
	ml->SetTimer(TimeUp_Elim, 2000, 2000, host, NULL);
}

/* After 20 seconds have passed, start the game */
local int TimeUp_Elim(void *p)
{
	Player *host = p;
	Arena *arena = host->arena;

	//abort the game if it was somehow cancelled
	Adata *adata = P_ARENA_DATA(arena, arenaKey);
	if (adata->started == 0)
		return 0;

	//set the game to started
	adata->started = 2;
	
	Player *g;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(g)
	{
		if (g->arena == arena)
		{
			if (g->p_ship != SHIP_SPEC)
			{
				game->SetFreq(g, HUMAN_FREQ); //place everyone on team 0
				Pdata *pdata = PPDATA(g, playerKey);
				pdata->kills = 0; //reset deaths
				pdata->lives = adata->obj; //reset deaths

				Target target;
				target.type = T_PLAYER;
				target.u.p = g;
				game->GivePrize(&target, 7, 1); //warp everyone
			}
		}
	}
	pd->Unlock();
	
	CheckLegalShip(arena); //check for valid ships
	
	OnePlayerFreqs(host->arena); //put everyone on single teams
	
	chat->SendArenaSoundMessage(host->arena, 104, "Elim started, you'll be eliminated after %i %s.", adata->obj, adata->obj == 1 ? "death" : "deaths");

	mm->RegCallback(CB_KILL, elimKill, host->arena);
    mm->RegCallback(CB_PLAYERACTION, elimPlayerAction, host->arena);
    mm->RegCallback(CB_SHIPFREQCHANGE, elimShipFreqChange, host->arena);

	return 0;
}

/* When the game is stopped, unregister callbacks and timers, and unlock arena. */
local void elimStop(Arena* arena)
{
	ml->ClearTimer(TimeUp_Elim, arena);
	mm->UnregCallback(CB_KILL, elimKill, arena);
	mm->UnregCallback(CB_PLAYERACTION, elimPlayerAction, arena);
	mm->UnregCallback(CB_SHIPFREQCHANGE, elimShipFreqChange, arena);
	game->UnlockArena(arena, 1, 0);
	
	Stop(arena);
}
