/**
 * Command List
 *   
 */

#include <stdio.h>
#include <stdbool.h>
#include "asss.h"

static Imodman     *mm;
static Icmdman     *cmd;
static Ichat       *chat;

int LLSort_CommandInfo(const void *a_, const void *b_)
{
        const CommandInfo *a = a_;
        const CommandInfo *b = b_;

        return LLSort_StringCompare(a->name, b->name);
}

static helptext_t listcommand_help = "todo";

static void DoList(Arena *arena, Player *p, int filterGlobal, int filterNoAccess)
{
        LinkedList *commands = cmd->GetCommands(arena, p, filterGlobal, filterNoAccess);
        Link *l;
        StringBuffer bufG;
        StringBuffer bufA;
        
        LLSort(commands, LLSort_CommandInfo);
        
        SBInit(&bufG);
        SBInit(&bufA);
        
        SBPrintf(&bufG, "Global:");
        SBPrintf(&bufA, "Arena:");
        
        for (l = LLGetHead(commands); l; l = l->next)
        {
                CommandInfo *info = l->data;
                StringBuffer *buf = info->arena ? &bufA : &bufG;
                
                SBPrintf(buf, " ");
                
                SBPrintf(buf, "%s", info->can_arena || info->can_priv || info->can_rpriv ? "" : "!");
                SBPrintf(buf, "%s", info->can_arena ? "." : "");
                SBPrintf(buf, "%s", info->can_priv ? "/" : "");
                SBPrintf(buf, "%s", info->can_rpriv ? "::" : "");
                SBPrintf(buf, "%s", info->name);
        }
        
        chat->SendWrappedText(p, SBText(&bufG, 0));
        chat->SendWrappedText(p, SBText(&bufA, 0));
        
        SBDestroy(&bufG);
        SBDestroy(&bufA);
        cmd->FreeGetCommands(commands);
}

static void ListCommand(const char *command, const char *params, Player *p, const Target *target)
{
        DoList(p->arena, p, false, true);
}

static void ListAllCommand(const char *command, const char *params, Player *p, const Target *target)
{
        DoList(p->arena, p, false, false);
}

EXPORT const char info_cmdlist[] = "cmdlist (" ASSSVERSION ", " BUILDDATE ")\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(cmd    );
        mm->ReleaseInterface(chat   );
}

EXPORT int MM_cmdlist(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
                mm = mm_;
                
                cmd     = mm->GetInterface(I_CMDMAN    , ALLARENAS);
                chat    = mm->GetInterface(I_CHAT      , ALLARENAS);

                if (!cmd || !chat)
                {
                        printf("<cmdlist> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }
                
                cmd->AddCommand("cmdlist", ListCommand, ALLARENAS, listcommand_help);
                cmd->AddCommand("commands", ListCommand, ALLARENAS, listcommand_help);
                cmd->AddCommand("allcommands", ListAllCommand, ALLARENAS, listcommand_help);
                
                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                cmd->RemoveCommand("cmdlist", ListCommand, ALLARENAS);
                cmd->RemoveCommand("commands", ListCommand, ALLARENAS);
                cmd->RemoveCommand("allcommands", ListAllCommand, ALLARENAS);
        
                ReleaseInterfaces();
                return MM_OK;
        }
        
        return MM_FAIL;
}
