 /**************************************************************
 * Restrict Module
 *
 * The restrict module does not do anything on its own,
 * asides from listing items that are being restricted
 * by other modules such as Buy using ?restrictions .
 *
 * By: Hallowed be thy name
 *
 **************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "asss.h"
#include "restrict.h"

local Imodman *mm;
local Ichat *chat;
local Icmdman *cmd;

typedef struct Rlist
{
    int id;
    const char *name;
    int size;
    int used;
    Restriction *list;
} Rlist;

local Rlist *list;
local int number;

local void cleanUp()
{
    int i = 0;
    for (; i < number; i++)
    {
        free(list[i].list);
    }
    free(list);
}

local void freelist(Rlist *rl)
{
    int id = rl->id;
    const char *name = rl->name;
    free(rl->list);
    free(rl);
    rl = (Rlist*)malloc(sizeof(Rlist));
    rl->id = id;
    rl->name = name;
    rl->size = 0;
    rl->used = 0;

}

local int createList(const char *name)
{
    if (!list)
        list = (Rlist*)malloc(sizeof(Rlist));
    else
        list = (Rlist*)realloc(list, sizeof(Rlist) * (number+1));

    Rlist this = { number, name, 0, 0, NULL };
    list[number] = this;

    return number++;
}

local int restrict_(int listid, const char *name, Target target, unsigned int ttime, void *clos)
{
    Rlist *rl = &list[listid];
    if (!rl->list)
        rl->list = (Restriction*)malloc(sizeof(Restriction));
    else
        rl->list = (Restriction*)realloc(rl->list, sizeof(Restriction) * (rl->size+1));


    Restriction this = { rl->size, name, target, ttime + time(NULL), 1, clos };
    rl->list[rl->size] = this;

    rl->used++;
    return rl->size++;
}

local Restriction* isRes(Player *p, int listid, const char *name)
{
    int i = 0;
    for (; i < list[listid].size; i++)
    {
        Restriction* res = &list[listid].list[i];
        if ((res->time < time(NULL)) && (res->used))
        {
            res->used = 0;
            list[listid].used--;
        }

        if (res->name == name)
        {
            if ((((res->target.type == T_PLAYER) && (res->target.u.p == p)) ||
                ((res->target.type == T_ARENA) && (res->target.u.arena == p->arena)) ||
                ((res->target.type == T_FREQ) && (res->target.u.freq.arena == p->arena) && (res->target.u.freq.freq == p->p_freq)) ||
                (res->target.type == T_ZONE))
                    &&
                (res->used))
            {
                return res;
            }
        }
    }
    if ((i == 0) && (list[listid].size > 0))
        freelist(&list[listid]);

    return NULL;
}

local void cRestrictions(const char *command, const char *params, Player *p, const Target *target)
{
    char *next;
    int new = -1;
    new = strtol(params, &next, 0);
    if ((new > number) || (new < 1))
    {
        //List lists ;)
        int i = 0;
        chat->SendMessage(p, "-------------------------------");
        chat->SendMessage(p, "| ID | List            | Size |");
        chat->SendMessage(p, "|----+-----------------+------|");
        for (; i < number; i++)
        {
            Rlist *rl = &list[i];
            chat->SendMessage(p, "| %2i | %-15s | %4i |", rl->id + 1, rl->name, rl->used);
        }
        chat->SendMessage(p, "-------------------------------");
    }
    else
    {

        char *types[6] = { "", "Player", "Arena", "Freq", "Zone" };
        int rused = 0;
        Rlist *rl = &list[new-1];
        chat->SendMessage(p, "----------------------------------------------------------------------");
        chat->SendMessage(p, "| ID   | Restriction     | Type   | Name                 | Time left |");
        chat->SendMessage(p, "|------+-----------------+--------+----------------------+-----------|");
        int i = 0;
        for (; i < rl->size; i++)
        {
            Restriction *r = &rl->list[i];
            if ((r->time < time(NULL)) && (r->used))
            {
                r->used = 0;
                rl->used--;
            }
            if (r->used)
            {
                rused++;
                char *name = "";
                if (r->target.type == T_PLAYER)
                    name = r->target.u.p->name;
                if (r->target.type == T_FREQ)
                    name = r->target.u.freq.arena->name;
                    //sprintf(name, "%s.%i\n", r->target.u.freq.arena->name, r->target.u.freq.freq);
                if (r->target.type == T_ARENA)
                    name = r->target.u.arena->name;

                int secs = r->time - time(NULL);
                int hours = secs / 3600;
                secs -= (360 * hours);
                int mins = secs / 60;
                secs -= (60 * mins);
                mins -= (60 * hours);

                chat->SendMessage(p, "| %4i | %-15s | %-6s | %-20s | %2ih%2im%2is |", r->id+1, r->name, types[r->target.type], name, hours, mins, secs);
            }
        }
        if ((rused == 0) && (rl->size > 0))
            freelist(rl);
        chat->SendMessage(p, "---------------------------------------------------------------------");
    }
}

local Irestrict interface =
{
    INTERFACE_HEAD_INIT(I_RESTRICT, "Irestrict")
    createList, restrict_, isRes
};

EXPORT int MM_restrict(int action, Imodman *mm_, Arena *arena)
{
    if (action == MM_LOAD)
    {
        mm = mm_;
        chat = mm->GetInterface(I_CHAT, ALLARENAS);
        cmd = mm->GetInterface(I_CMDMAN, ALLARENAS);

        if (!chat || !cmd)
            return MM_FAIL;
        else
        {
            cmd->AddCommand("restrictions", cRestrictions, ALLARENAS, NULL);
            mm->RegInterface(&interface, ALLARENAS);
            return MM_OK;
        }
    }
    else if (action == MM_UNLOAD)
    {
        if (mm->UnregInterface(&interface, ALLARENAS))
		{
			return MM_FAIL;
		}

		cleanUp();

		cmd->RemoveCommand("restrictions", cRestrictions, ALLARENAS);

        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(chat);

        return MM_OK;
    }

    return MM_FAIL;
}
