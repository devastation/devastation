from asss import *
import mysql.connector

cfg = get_interface(I_CONFIG)
host = cfg.GetStr(None, 'Web_DB', 'hostname')
user = cfg.GetStr(None, 'Web_DB', 'user')
password = cfg.GetStr(None, 'Web_DB', 'password')
database = cfg.GetStr(None, 'Web_DB', 'database')

_connection = None

def get_connection():
    global _connection
    if not _connection:
        _connection = mysql.connector.connect(host=host, user=user, password=password, database=database)
    return _connection

# List of stuff accessible to importers of this module. Just in case
__all__ = [ 'get_connection' ]