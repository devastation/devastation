from asss import *
import web_db

ITERATE_WEB_INTERVAL = 100*1*60 #every 60 seconds

#DEBUG:
#ITERATE_WEB_INTERVAL = 100*1*10 #every 10 seconds

def save_counts(counts):
    conn = web_db.get_connection()
    cur = conn.cursor(buffered=True)
    
    query = (
        "INSERT INTO player_count (type, count) VALUES (%(type)s, %(count)s) "
        "ON DUPLICATE KEY UPDATE "
        "  count = VALUES(count);"
    )

    data = {'type':'players', 'count': str(counts['players'])}
    cur.execute(query, data)
    
    data = {'type':'spectators', 'count': str(counts['spectators'])}
    cur.execute(query, data)
    
    conn.commit()

def update_count():
    counts = {'spectators': 0, 'players': 0}
    
    def cb_iter(p):
        
        if p and p.status == S_PLAYING and p.type != T_FAKE and p.arena and p.arena.name and p.arena.name[0] != '#':
            #if player is in a public arena and not in ship 8 (0-7 is OK, 8 is spectator)
            if p.ship >= 0 and p.ship < 8:
                counts['players'] += 1
            else:
                counts['spectators'] += 1

    for_each_player(cb_iter)
    
    save_counts(counts)
    
    #Continue Timer Loop:
    return 1

iter_timer = set_timer(update_count, ITERATE_WEB_INTERVAL)