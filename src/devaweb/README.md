Install Steps:
==============

- Install the .deb from https://dev.mysql.com/downloads/connector/python/
- The pushnotifier directory should be copied into ASSS' src/
- Make
- In <asss>/conf/global.conf set:

```ini
[ Web_DB ]
hostname = 127.0.0.1
user = <username>
password = <password>
database = devaweb
```

- In your database, create the player_count table:

```SQL
CREATE TABLE `player_count` (
  `type` varchar(20) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`type`),
  UNIQUE KEY `item_UNIQUE` (`type`)
);
```

- In <asss>/conf/modules.conf set: <py> web
- Restart ASSS